## Copyright (C) 2005, 2007 G. D. McBain <geordie_mcbain@yahoo.com.au>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

## -*- texinfo -*-
## @deftypefn {Function File} {} ierfc (@var{z})
## Compute the integral of the error function, defined as (Gautschi
## 1965, p. 299, eq. 7.2.1) 
## @example
##
##  ierfc (@var{z}) = quad (@@erfc, @var{z}, Inf),
## @end example
##
## using a recurrence relation (ibid., eq. 7.2.5).
##
## W. Gautschi 1965 Error function and Fresnel integrals.  In M.
## Abramowitz & I. Stegun (eds) @cite{Handbook of Mathematical Functions},
## Dover, New York, ch. 7, pp. 295--329.
## @seealso{erfc, i2erfc}
## @end deftypefn

## ### BACKGROUND THEORY
##
## See Gautschi (1965) for properties of the iterated integrals of the
## error function.  In particular, see p. 305 for a recommended method.

## Author: G. D. McBain <geordie.mcbain@aeromech.usyd.edu.au>
## Created: 2005-12-19
## Keywords: special functions, repeated integrals of the error function

function f = ierfc (z)
  f = exp (-z.*z) / sqrt (pi) - z .* erfc (z);
endfunction

%!demo 
%!
%! ## reproduce table 7.4 of Gautschi (1965, p. 317)
%!
%! f = ierfc (x = (0.0 : 0.1 : 5.0)');
%! printf ("%3.1f\t%- 11.5e\n", [x, 2*gamma(1/2+1)*f]');
%! plot (x, f, "*-;ierfc;");
