## Copyright (C) 2006, 2007 G. D. McBain <geordie_mcbain@yahoo.com.au>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

## -*- texinfo -*-
## @deftypefn {Function File} {} i2erfc (@var{z})
## Compute the repeated integral of the error function, defined as
## (Gautschi 1965, p. 299, eq. 7.2.1)
## @example
##
##  i2erfc (@var{z}) = quad (@@ierfc, @var{z}, Inf),
## @end example
##
## using a recurrence relation (ibid., eq. 7.2.5).
##
## W. Gautschi 1965 Error function and Fresnel integrals.  In M.
## Abramowitz & I. Stegun (eds) @cite{Handbook of Mathematical Functions},
## Dover, New York, ch. 7, pp. 295--329.
## @seealso{erfc}
## @end deftypefn

## ### BACKGROUND THEORY
##
## See Gautschi (1965) for properties of the iterated integrals of the
## error function.  In particular, see p. 305 for a recommended method.

## Author: G. D. McBain <geordie.mcbain@aeromech.usyd.edu.au>
## Created: 2006-12-06
## Keywords: special functions, iterated integrals of the error function

function f = i2erfc (z)
  f = (0.5 * z.*z + 0.25) .* erfc (z) - 0.5 * z .* exp (-z.*z) / sqrt (pi);
endfunction

%!demo
%!
%! ## reproduce table 7.4 of Gautschi (1965, p. 317)
%!
%! f = i2erfc (x = (0.0 : 0.1 : 5.0)');
%! printf ("%3.1f\t%- 11.5e\n", [x, 4*f]');
%! plot (x, f, "*-;ierfc;");
