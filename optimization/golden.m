## Copyright (C) 2007, 2012 G. D. McBain -*- octave -*-
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

## -*- texinfo -*-
## @deftypefn {Function File} {} [@var{x}, @var{y}] = golden (@var{f}, @var{a}, @var{b}, @var{tol})
## Compute the minimum @var{x} of the unimodal function @var{f} on the
## segment from @var{a} to @var{b} using the golden section search
## (Greig 1980, p. 37; Kahaner, Moler, & Nash 1989, p. 360), and
## optionally the value @var{y} of the function there. 
##
## D. M. Greig 1980 @cite{Optimisation}.  London: Longman
##
## D. Kahaner, C. Moler, & S. Nash 1989 @cite{Numerical Methods and
## Software}. Englewood Cliffs, New Jersey: Prentice-Hall
## @end deftypefn

## ### BACKGROUND THEORY
##
## The method is classical (e.g. Greig 1980, p. 37; Kahaner, Moler, &
## Nash 1989, p. 360).

## ### IMPLEMENTATION NOTES
##
## I've tried to write it so that it might later be generalized to
## permit column-vector operands, but haven't tested this at all.  The
## idea is that line-segment searches often arise as steps in
## higher-dimensional optimization problems (Greig 1980, p. 35).

## Author: G. D. McBain <geordie.mcbain@aeromech.usyd.edu.au>
## Created: 2007-01-30
## Keywords: optimization, dissection

function [xmin, fxmin] = golden (f, a, b, tol, verbose)

  if (nargin < 5)
    verbose = false;
  endif

  x = dissect (a, b);
  fx = f (x);

  y = dissect (b, a);
  fy = f (y);

  while (norm (b-a) > tol)
    if (fx < fy)
      b = y;
      y = x;
      fy = fx;
      x = dissect (a, b);
      fx = f (x);
    else
      a = x;
      x = y;
      fx = fy;
      y = dissect (b, a);
      fy = f (y);
    endif
    if (verbose)
      printf ("f(%g) < f(%g)\n", a, b);
    endif
  endwhile

  xmin = 0.5 * (a + b);

  if (nargout > 1)
    fxmin = f (xmin);
  endif

endfunction

function x = dissect (a, b)
  persistent r = 2 / (1 + sqrt (5)); # (/ 2 (1+ (sqrt 5)))
  x = r * a + (1-r) * b;
endfunction

%!test
%!
%!  tol = 1e-3;
%!  x = golden (@ (x) (3-4*x)./(1+x.^2), 1, 6.5, tol);
%!  assert (abs (x - 2) < tol);

%!demo
%!
%! ## f (x) = (3-4*x)/(1+x^2) (Greig 1980, p. 37, example 2.1)
%!
%! [x, fx] = golden (@ (x) (3-4*x)./(1+x.^2), 1, 6.5, 1e-3, true)

%!demo
%!
%! ## f (x) = (3-4*x)/(1+x^2) (Greig 1980, p. 37, example 2.1)
%!
%! printf ("%5s\t%7s\t %s\n\n", "tol", "x", "error");
%! for tol = logspace (-1, -9, 9)
%!   x = golden (@ (x) (3-4*x)./(1+x.^2), 1, 6.5, tol);
%!   printf ("%5.0e\t%7.5f\t% e\n", tol, x, x - 2);
%! endfor

%!demo
%!
%! ## f (x) = (x - 0.65).^2 (Kahaner, Moler, & Nash 1989, p. 360, example 9.3)
%!
%! printf ("%5s\t%7s\t %s\n\n", "tol", "x", "error");
%! for tol = logspace (-1, -9, 9)
%!   x = golden (@ (x) (x-0.65).^2, 0, 1, tol);
%!   printf ("%5.0e\t%7.5f\t% e\n", tol, x, x - 0.65);
%! endfor

%!demo
%!
%! ## f (x) = x.^3 - 2*x - 5 (Kahaner, Moler, & Nash 1989, pp. 362--363)
%!
%! printf ("%5s\t%7s\t %s\n\n", "tol", "x", "error");
%! for tol = logspace (-1, -9, 9)
%!   x = golden (@ (x) x.^3 - 2*x -5, 0.1, 0.9, tol);
%!   printf ("%5.0e\t%7.5f\t% e\n", tol, x, x - sqrt (2/3));
%! endfor
