## Copyright (C) 2006, 2007 G. D. McBain <geordie_mcbain@yahoo.com.au>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or (at
## your option) any later version.
##
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301USA

## -*- texinfo -*-
##
## @deftypefn {Function File} {} thermalcolormap (@var{n})
##
## Return a colormap with @var{n} levels of hue running from blue
## through green to red.
##
## @seealso{colormap}
##
## @end deftypefn

## Author: G. D. McBain <geordie.mcbain@aeromech.usyd.edu.au>
## Created: 2006-12-13
## Keywords: colormap

function c = thermalcolormap (nlevels)

  if nargin < 1, nlevels = rows (colormap); endif

  c = hsv2rgb (postpad (linspace (2/3, 0, nlevels)', 3, 1, 2));

endfunction

%!demo# Norms in the first quadrant: taxicab: abs (x) + abs (y)
%! n = 64;
%! x = y = linspace (0, 1, n);
%! [xx, yy] = meshgrid (x, fliplr (y));
%! zz = abs (xx) + abs (yy);
%! colormap (thermalcolormap (n));
%! imagesc (zz);

%!demo# Euclidean norm: sqrt (x*x + y*y)
%! n = 64;
%! x = y = linspace (0, 1, n);
%! [xx, yy] = meshgrid (x, fliplr (y));
%! zz = sqrt (xx.^2 + yy.^2);
%! colormap (thermalcolormap (n));
%! imagesc (zz);

%!demo# maximum norm: max (x, y)
%! n = 64;
%! x = y = linspace (0, 1, n);
%! [xx, yy] = meshgrid (x, fliplr (y));
%! zz = max (xx, yy);
%! colormap (thermalcolormap (n));
%! imagesc (zz);
