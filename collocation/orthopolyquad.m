## Copyright (C) 2004, 2005, 2006, 2007 G. D. McBain <geordie_mcbain@yahoo.com.au>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

## -*- texinfo -*-
## @deftypefn {Function File} {} [@var{x}, @var{w}] = orthopolyquad (@var{family}, @var{n}[, @var{alpha}[, @var{beta}]])
## Compute the column of abscissae @var{x} and row of weights @var{w} of
## the @var{family} ("Legendre", "Chebyshev", "Gegenbauer", "Jacobi",
## "Laguerre", or "Hermite") of Gaussian quadrature rule (with
## parameters @var{alpha} and @var{beta}, as needed).
## @end deftypefn

## ### BACKGROUND THEORY
##
## Any family of orthogonal polynomials obeys a three-term recurrence
## relation, the coefficients of which can be combined into a
## tridiagonal matrix, called the Jacobi matrix (Wilf 1962, p. 55), the
## eigenvalues of which give the roots.  The matrix can be rendered
## symmetric (Golub & Welsch 1969), and the leading elements of its
## eigenvectors are proportional to the weights (ibid.); see also Laurie
## (2001).

## ### TODO
##
## ### ## Solution
##
## ### ## # Sparse solution
##
## Can the structure of the Jacobi matrix be exploited, or shall we
## continue to rely on a call to the generic Octave eig function.
##
## ### ## # The Golub--Welsch (1969) algorithm
##
## Only the leading elements of the eigenvector are required, whereas
## Octave's eig naturally computes the entire eigenvector.  An efficient
## algorithm for computing just the required elements was developed by
## Golub & Welsch (1969).  Is it worth implementing this?

## ### REFERENCES
##
## P. Concus, D. Cassatt, G. Jaehnig, & E. Melby 1963 Tables for the
## evaluation of  $\int^\infty_0 x^\beta e^{-x} f(x) d x$ by
## Gauss-Laguerre quadrature.  Mathematics of Computation 17:245-256.
##
## P. J. Davis & I. Polonsky 1965 Numerical interpolation,
## differentiation, and integration.  In M. Abramowitz & I. Stegun (eds)
## Handbook of Mathematical Functions, Dover, ch. 25.
##
## G. H. Golub & J. H. Welsch 1969 Calculation of Gauss quadrature
## rules. Mathematics of Computation 23:221-230.
##
## D. P. Laurie 2001 Computation of Gauss-type quadrature formulas.
## Journal of Computational and Applied Mathematics 127:201-217.
##
## D. S. Liepman 1965 Mathematical constants.  In M. Abramowitz & I.
## Stegun (eds) Handbook of Mathematical Functions, Dover, ch. 1.
##
## P. Rabinowitz & G. Weiss 1959 Tables of abscissas and weights for
## numerical evaluation of integrals of the form $\int_0^\infty e^{-x}
## x^n f(x)\;dx$\,. Mathematical Tables and other Aids to Computation
## 13(68):285-294.
##
## T. S. Shao, T. C. Chen, & R. M. Frank (1964) Tables of zeros and
## Gaussian weights of certain associated Laguerre polynomials and the
## related generalized Hermite polynomials.  Mathematics of Computation
## 18(88):598--616.
##
## E. T. Whittaker & G. N. Watson 1927 A Course of Modern Analysis, 4th
## ed., Cambridge University Press.
##
## H. S. Wilf 1962 Mathematics for the Physical Sciences.  Wiley, New
## York.

## Author: G. D. McBain <geordie.mcbain@aeromech.usyd.edu.au>
## Created: 2006-05-03
## Keywords: Gaussian quadrature

function [x, w] = orthopolyquad (family, n, alpha, beta)
  
  switch (family = tolower (family))
    case "legendre"
      alpha = beta = 0.0;
      family = "jacobi";
    case "chebyshev"
      alpha = beta = -0.5;
      family = "jacobi";
    case {"gegenbauer", "ultraspherical"}
      beta = alpha;
      family = "jacobi";
    case "jacobi"
      "do nothing";
    case "laguerre"
      if nargin < 3, alpha = 0.0; endif
      b0 = gamma (1+alpha);
    case "hermite"
      b0 = sqrt (pi);
    otherwise
      error ("orthogonal polynomial %s unimplemented", family);
  endswitch

  if strcmp (family, "jacobi")
     b0 = 2 ^ (1+alpha+beta) * gamma (alpha+1) * gamma (beta+1) \
	 / gamma (alpha+beta+2);
  endif

  switch n
    case 0
      J = zeros (0, 1);
    case 1
      switch family
	case "jacobi"
	  J = (beta-alpha) / (2+alpha+beta);
	case "laguerre"
	  J = 1+alpha;
	case "hermite"
	  J = 0;
      endswitch
    otherwise
      k = 1:n;
      switch family
	case "jacobi"
	  k1 = 2 * k + alpha + beta;
	  a = [(beta-alpha)./(2+alpha+beta), \
	       ((beta^2-alpha^2)./(k1.*(k1-2)))(2:n)];
	  b = sqrt ([4*(1+alpha)*(1+beta)/((3+alpha+beta)*(2+alpha+beta)^2),\
		     ((4*k.*(k + alpha).*(k + beta).*(k + alpha + beta)
		       ./ (k1.^2 .* (k1.^2 - 1)) ) (2:(n-1)) )]);
	case "laguerre"
	  a = 2*k - 1 + alpha;
	  b = sqrt (k .* (k+alpha)) (1:(n-1));
	case "hermite"
	  a = zeros (1, n);
	  b = sqrt ((1:(n-1)) / 2);
      endswitch
      J = spdiags ([[b, 0]; a; [0, b]]', -1:1, n, n);
  endswitch

  switch nargout
    case {0, 1}
      x = eig (J);
    case 2
      [S, X] = eig (J);
      x = diag (X);
      if n==0
	w = zeros (1, 0);
      else
	w = b0 * S(1,:).^2;
      endif
  endswitch

endfunction

%!test
%!
%! ## integrates a constant correctly
%!
%! n = 64;
%! [x, w] = orthopolyquad ("legendre", n);
%! assert (sum (w), 2, 1e-12)

%!test
%!
%! ## integrates a linear function correctly
%!
%! n = 64;
%! [x, w] = orthopolyquad ("legendre", n);
%! assert (w*x, 0, 1e-12)

%!test
%!
%! ## integrates a quadratic function correctly
%!
%! n = 64;
%! [x, w] = orthopolyquad ("legendre", n);
%! assert (w*x.^2, 2/3, 1e-12)

%!test
%!
%! ## known value for single generalized Laguerre point
%!
%! n = 1;
%! for alpha = -0.5:0.5:0.5
%!   [x, w] = orthopolyquad ("laguerre", n, alpha);
%!   xx = alpha+1; wx = gamma (alpha+1);
%!   assert (x, xx);
%!   assert (w, wx);
%! endfor

%!test
%!
%! ## known values for two generalized Laguerre points
%!
%! n = 2;
%! for alpha = -0.5:0.5:0.5
%!   [x, w] = orthopolyquad ("laguerre", n, alpha);
%!   xx = alpha + 2 + [-1; 1] * sqrt (alpha + 2); 
%!   wx = [(alpha+2-sqrt(alpha+2))*gamma(3+alpha)/2/((2+alpha)*(1-sqrt(alpha+2)))^2, (alpha+2+sqrt(alpha+2))*gamma(3+alpha)/2/((2+alpha)*(1+sqrt(alpha+2)))^2];
%!   assert (x, xx, 1e-12);
%!   assert (w, wx, 1e-12);
%! endfor

%!test
%!
%! ## two identities (Rabinowitz & Weiss 1959)
%!
%! n = 64;
%! alpha = -0.5;
%! x = orthopolyquad ("laguerre", n, alpha);
%! assert (sum (x), n * (n + alpha), 1e-12);
%! assert (prod (x' ./ (alpha + (1:n))), 1, 1e-12);

%!demo 
%!
%! ## compute Euler's constant
%! ## quad (@ (x) exp (-x) * (1/(1-exp(-x)) - 1/x), 0, Inf);
%! # (Whittaker & Watson 1927, p. 246)
%! euler = 0.577215664901532860606512; # (Liepman 1965, p. 3)
%! printf ("%3s\t%17.15s\t%- s\n\n", "n", "integral", "error");
%! for n = 2 .^ (0:9)
%!  [x, w] = orthopolyquad ("laguerre", n);
%!  eulern = w * (1 ./ (1 - exp (-x)) - 1 ./ x);
%!  printf ("%3d\t%17.15f\t%- e\n", n, eulern, eulern-euler);
%! endfor


%!demo
%!
%! ## quad (@ (x) exp (-x), 0, Inf) => 1
%!
%! printf ("%3s\t%s\t%- s\n\n", "n", "integral", "error");
%! for n = 2 .^ (0:9)
%!   [x, w] = orthopolyquad ("laguerre", n);
%!   q = w * ones (size (x));
%!   printf ("%3d\t%g\t%- e\n", n, q, q-1);
%! endfor

%!demo
%!
%! ## Gaussian quadrature points, comparison with colloc
%!
%! printf ("%3s\t%11s\n\n", "n", "error");
%! for n = 2 .^ (0:8)
%!   [x, w] = orthopolyquad ("legendre", n);
%!   [xc, A, B, wc] = colloc (n); clear ("A", "B");
%!   err = norm (x - (2*xc-1)) + norm (w' - (2*wc));
%!   printf ("%3d\t%11.5e\n", n, err);
%! endfor

%!demo
%!
%! ## reproduce Jacobi abscissae from Ma & Korsunsky (2004, table 1)
%!
%! B = 0.4727828;
%! n = 25;
%! x = orthopolyquad ("jacobi", n, B-1, -B);
%! ma04 = [0.998152341293455 0.982628164674953 0.951605745485052 0.905576080378733 0.845265233707347 0.771624374840183 0.685814873889498 0.589190001797237 0.483273592282616 0.369736011291179 0.250367814783509 0.127051510791568 1.731871230082485E-003 -0.123614738279451 -0.247011526863765 -0.366512453580906 -0.480232917685294 -0.586379479991279 -0.683278146666021 -0.769400769550092 -0.843389147127064 -0.904076447810500 -0.950505626208274 -0.981944605084091 -0.997899122911981];
%! printf ("%2s   %18s  %18s  %11s\n\n", "i", "present", "Ma & Korunsky (2004)", "error");
%! for i = 1:n
%!   err = x(n-i+1) - ma04(i);
%!   printf ("%2d   %- 18.15f  %- 18.15f  %- 11.5e\n", i, x(n-i+1), ma04(i), err);
%! endfor

%!demo
%!
%! ## n=15 Laguerre points (Davis & Polonsky 1965, p. 923, table 25.9)
%! 
%! [x, w] = orthopolyquad ("laguerre", 15); [x, w']

%!demo
%!
%! ## alpha=5 Laguerre points (Rabinowitz & Weiss 1959, p. 293)
%!
%! alpha = 5;
%! printf ("%23s  %23s\n\n", "abscissae", "weights");
%! for n = [4, 8, 12, 16];
%!   printf ("%23s  alpha = %d, n = %3d\n\n", "", alpha, n);
%!   [x, w] = orthopolyquad ("laguerre", n, alpha);
%!   printf ("%23.17e  %23.17e\n", [x, w']');
%!   printf ("\n");
%! endfor

%!demo
%!
%! ## Concus et al. (1963, p. 248), n=15 table
%! [x, w] = orthopolyquad ("laguerre", 15, -0.25); 
%! [x, w', w'.*exp(x)]

%!demo
%!
%! ## Shao, Chen, & Frank (1964, p. 599)
%!
%! [x, w] = orthopolyquad ("laguerre", 32, -0.5); 
%! [x, w']

%!demo
%!
%!  ## Hermite quadrature (Davis & Polonsky 1965, p. 924, table 25.10)
%!
%!  n = 20
%!  printf ("%15s  %16s\n\n", "x", "w");
%!  [x, w] = orthopolyquad ("hermite", 20);
%!  for i = 11:20
%!    printf ("%15.13f  %16.12e\n", x(i), w(i))
%!  endfor
