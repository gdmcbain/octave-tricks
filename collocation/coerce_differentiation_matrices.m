## Copyright (C) 2006, 2007, 2010 G. D. McBain -*- octave -*-
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or (at
## your option) any later version.
##
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301USA

## -*- texinfo -*-
## @deftypefn{Function File} {@var{Dc} =} coerce_differentiation_matrices (@var{D}, @var{alpha}, @var{beta})
## Modify the differentiation matrices in the cell array @var{D} by
## multiplying each sampling function by the coercing factor with
## ordinates are @var{alpha} and normalized derivatives @var{beta}; i.e.
## @code{@var{beta}(i,j)} is the j-th derivative of the factor, divided
## by the factor, all evaluated at the i-th abscissa.
## @seealso{differentiation_matrices}
## @end deftypefn

## ### IMPLEMENTATION NOTES
##
## Although we use a double-for loop, note that these are only over
## order K, which is typically a small number like two or four.

## ### TODO
##
## Try out the third-order examples on the segment in section 4 of
##
## W. Huang & D. M. Sloan 1992 The pseudospectral method for third-order
## differential equations.  SIAM Journal on Numerical Analysis
## 29(6):1626--1647.

## ### REFERENCES
##
## D. M. Sloan 2004 On the norms of inverses of pseudospectral
## differentiation matrices.  SIAM Journal on Numerical Analysis
## 42(1):30--48.

## Author: G. D. McBain <gdmcbain@freeshell.org>
## Created: 2006-03-22
## Keywords: collocation, differentiation

function Dc = coerce_differentiation_matrices (D, alpha, beta)
  Dc = D;			# initialize, as well as allocate
  for k = 1:(length (D))
    Dc{k} += diag (beta(:,k));
    for m = 1:(k-1)
      Dc{k} += bincoeff (k, m) * diag (beta(:,k-m)) * D{m};
    endfor
    Dc{k} .*= alpha(:) * (1 ./ alpha(:))';
  endfor
endfunction

%!demo
%!
%! ## 2ptbvp: u'' = -2, u(0) = u(1) = 0; => u (x) = x .* (1-x)
%!
%! order = 2;
%! printf ("%3s\t%- 14.6s\n\n", "n", "error");
%! for n = 2 .^ (0:9)
%!   x = (orthopolyquad ("Legendre", n) + 1)/2;
%!   D0 = differentiation_matrices (x, order);
%!   alpha = x .* (1-x);
%!   beta = [1-2*x, -2*ones(n,1)] ./ repmat (alpha, [1, order]);
%!   D = coerce_differentiation_matrices (D0, alpha, beta);
%!   u = D{2} \ (-2 * (ones (n, 1)));
%!   ux = x .* (1-x);
%!   printf ("%3d\t%- 14.6e\n", n, norm (u - ux));
%! endfor

%!demo
%!
%! ## 2ptbvp: u'' = -((4*m+1)*pi)^2*sin((4*m+1)*pi*x), u(0) = u(1) = 0 
%! ## which has the exact solution u = sin ((4*m+1)*pi*x) with u(1/2)=1
%! ## Resolution precedes asymptotic convergence.
%! printf ("%3s\t%- 14s\t%- 14s\n\n", "n", "m=1", "m=5");
%! alpha_f = @ (x) x .* (1-x);
%! for n = 2 .^ (0:8)
%!   x = (orthopolyquad ("Legendre", n) + 1)/2;
%!   [D, w] = differentiation_matrices (x, 2);
%!   alpha = alpha_f (x);
%!   beta = [1-2*x, -2*ones(size(x))] ./ repmat (alpha, 1, 2);
%!   D = coerce_differentiation_matrices (D, alpha, beta);
%!   bary =  barycentric_init (x, w, 0.5, alpha_f);
%!   m=1; u1 = D{2} \ (-((4*m+1)*pi)^2*sin((4*m+1)*pi*x));
%!   m=5; u5 = D{2} \ (-((4*m+1)*pi)^2*sin((4*m+1)*pi*x));
%!   printf ("%3d\t%- 14.6e\t%- 14.6e\n", n, barycentric_interpolate(bary,u1)-1,barycentric_interpolate(bary,u5)-1);
%! endfor

%!demo# eigenproblem: u'' = -l^2 u, u(0) = u(1) = 0 
%! ## => u = sin (l*x), l(k) = k*pi, k = 1,2,3,...
%! ## frequencies of a string (Christie 1952, p. 405, e.g.)
%! printf ("%3s\t%16.11s\t%14.6s\n\n", "n", "l1", "error");
%! Q = 2;
%! alpha_f = inline ("x .* (1-x)", 0);
%! for n = 2 .^ (0:8)
%!   x = colloc (n);
%!   D = differentiation_matrices (x, Q);
%!   alpha = alpha_f (x);
%!   beta = [1-2*x, -2*ones(size(x))] ./ repmat (alpha, 1, Q);
%!   D = coerce_differentiation_matrices (D, alpha, beta);
%!   l1 = max (eig (D{2})); l1x = -pi^2;
%!   printf ("%3d\t%16.11f\t%- 14.6e\n", n, l1, l1-l1x);
%! endfor

%!demo# eigenproblem: u'' = l u, u(+/- 1) = 0 
%! ## => l(k) = - (k*pi/2)^2
%! ## frequencies of a string (Christie 1952, p. 405, e.g.)
%! printf ("%3s\t%- 14.11s\t%- 14.6s\n\n", "n", "l1", "error");
%! Q = 2;
%! alpha_f = inline ("1-x.^2");
%! for n = 2 .^ (0:8)
%!   x = 2 * colloc (n) - 1;
%!   D = differentiation_matrices (x, Q);
%!   alpha = alpha_f (x);
%!   beta = -2*[x, (ones (size (x)))] ./ repmat (alpha, 1, Q);
%!   D = coerce_differentiation_matrices (D, alpha, beta);
%!   l1 = max (eig (D{2})); l1x = -pi^2/4;
%!   printf ("%3d\t%- 14.11f\t%- 14.6e\n", n, l1, l1-l1x);
%! endfor

%!demo
%!
%! ## Chebyshev 2nd order Dirichlet differential operator
%! ## (Sloan 2004, table 1)
%!
%! n = 5; 
%! Q = 2;
%! x = orthopolyquad ("Gegenbauer", n, 0.5);
%! D = differentiation_matrices (x, Q);
%! alpha = 1 - x.^2;
%! beta = -2 * [x, (ones (n, Q-1))] ./ repmat (alpha, [1, Q]);
%! D = coerce_differentiation_matrices (D, alpha, beta);
%! printf ("%- 8.3f%- 8.3f%- 8.3f%- 8.3f%- 8.3f\n", -inv (D{2})'/1e-1);

%!demo
%!
%! ## 2nd order Dirichlet differential operator for evenly spaced points
%! ## (Sloan 2004, table 2).
%!
%! n = 5; 
%! Q = 2;
%! x = linspace (-1, 1, n+2) ' (2:end-1);
%! D = differentiation_matrices (x, Q);
%! alpha = 1 - x.^2;
%! beta = -2 * [x, (ones (n, Q-1))] ./ repmat (alpha, [1, Q]);
%! D = coerce_differentiation_matrices (D, alpha, beta);
%! printf ("%- 8.3f%- 8.3f%- 8.3f%- 8.3f%- 8.3f\n", -inv (D{2})'/1e-1);

%!demo
%!
%! ## norms of Chebyshev extrema differential operators
%! ## Theorem 2.1 of Sloan (2004): the Inf-norm is
%! ## 0.5 for even n and 0.5*cos(pi*2/n).^2 for odd n.
%!
%! Q = 2;
%! printf ("%3s\t%6s\t%6s\n\n", "n", "norm", "Sloan (2004)");
%! for n = 1:32
%! x = orthopolyquad ("Gegenbauer", n, 0.5);
%!  D = differentiation_matrices (x, Q);
%!  alpha = 1 - x.^2;
%!  beta = -2 * [x, (ones (n, Q-1))] ./ repmat (alpha, [1, Q]);
%!  D = coerce_differentiation_matrices (D, alpha, beta);
%!  printf ("%3d\t%6.4f", n, norm (inv (D{2}), Inf));
%!  nx = 0.5;
%!  if (floor (n/2) == n/2)
%!    nx = 0.5 * cos (0.5*pi/(n+1)) .^2;
%!  endif
%!  printf ("\t%6.4f\n", nx);
%! endfor

%!demo
%! 
%! ## norms of Chebyshev roots differential operators
%! ## (Sloan 2004, thm 2.1)
%!
%! Q = 2;
%! printf ("%3s\t%6s\t%6s\n\n", "n", "norm", "Sloan (2004, thm 2.1)");
%! for n = 1:32
%!  x = orthopolyquad ("Chebyshev", n);
%!  D = differentiation_matrices (x, Q);
%!  alpha = 1 - x.^2;
%!  beta = -2 * [x, (ones (n, Q-1))] ./ repmat (alpha, [1, Q]);
%!  D = coerce_differentiation_matrices (D, alpha, beta);
%!  printf ("%3d\t%6.4f", n, norm (inv (D{2}), Inf));
%!  nx = 0.5;
%!  if (floor (n/2) == n/2)
%!    nx *= cos (0.5*pi/n) .^2; 
%!  endif
%!  printf ("\t%6.4f\n", nx);
%! endfor

%!demo
%!
%! ## norms of evenly-space differential operators (Sloan 2004, tbl. 3)
%!
%! Q = 2;
%! for n = [4:8, 15, 16, 32]-1
%!   x = linspace (-1, 1, n+2) ' (2:end-1);
%!   D = differentiation_matrices (x, Q);
%!   alpha = 1 - x.^2;
%!   beta = -2 * [x, (ones (n, Q-1))] ./ repmat (alpha, [1, Q]);
%!   D = coerce_differentiation_matrices (D, alpha, beta);
%!   printf ("%3d\t%11.4f\n", n+1, norm (inv (D{2}), Inf));
%! endfor

%!demo
%!
%! ## norms of Legendre differential operators (Sloan 2004, p. 38)
%!
%! Q = 2;
%! printf ("%3s\t%6s\t%6s\n\n", "n", "norm", "Sloan (2004, p. 38)");
%! for n = 1:32
%!   x = orthopolyquad ("Legendre", n);
%!   D = differentiation_matrices (x, Q);
%!   alpha = 1 - x.^2;
%!   beta = -2 * [x, (ones (n, Q-1))] ./ repmat (alpha, [1, Q]);
%!   D = coerce_differentiation_matrices (D, alpha, beta);
%!   printf ("%3d\t%11.4f", n+1, norm (inv (D{2}), Inf));
%!   nx = 0.5;
%!   if (floor (n/2) == n/2)
%!     nx = 0.5 * (1 - x (n/2).^2);
%!   endif
%!   printf ("\t%11.4f\n", nx);
%! endfor

%!demo
%!
%! ## norms of Chebyshev roots 1st order differential operators
%! ## reproduce table 4 of Sloan (2004); in fact it doesn't, quite
%!
%! Q = 1;
%! for n = 2 .^ (0:8)
%!  x = orthopolyquad ("Chebyshev", n);
%!  D = differentiation_matrices (x, Q);
%!  alpha = 1 + x;
%!  beta = ones (n, 1) ./ repmat (alpha, [1, Q]);
%!  D = coerce_differentiation_matrices (D, alpha, beta);
%!  printf ("%3d\t%6.4f\n", n, norm (inv (D{1}), Inf));
%! endfor


%!demo
%!
%! ## Laguerre weighting; e.g. differentiating exp (-x) * cos (x) twice.
%! ## This works O.K. unless the function decays too slowly, or n >~ 100.
%!
%! printf ("%3s\t%14s\t%14s\t%14s\n\n", "n", "err", "fast err", "slow err");
%! for n = 2 .^ (0:7)
%!   x = orthopolyquad ("laguerre", n);
%!   D = differentiation_matrices (x, 2);
%!   alpha = exp (-x);
%!   beta = repmat ((-1).^(1:2), n, 1);
%!   D = coerce_differentiation_matrices (D, alpha, beta);
%!   ## f = exp (-x) * cos (x)
%!   fpp = 2 * exp (-x) .* sin (x);
%!   fppn = D{2} * (exp(-x).*cos(x));
%!   err = norm (fpp - fppn);
%!   ## f = exp (-2*x) * cos (2*x)
%!   fpp = 8 * exp (-2*x) .* sin (2*x);
%!   fppn = D{2} * (exp(-2*x).*cos(2*x));
%!   fast_err = norm (fpp - fppn);
%!   ## f = exp (-x/2) * cos (x/2)
%!   fpp = exp (-x/2) .* sin (x/2) / 2;
%!   fppn = D{2} * (exp(-x/2).*cos(x/2));
%!   slow_err = norm (fpp - fppn);
%!   printf ("%3d\t%- 14.6e\t%- 14.6e\t%- 14.6e\n", n, err, fast_err, slow_err);
%! endfor

%!demo
%!
%! ## 2ptbvp on ray: u'' + u' = -x * exp (-x/2) / 4, u(0)=u(Inf)=0
%! ##
%! ## The exact solution is u (x) = x .* exp (-x/2).
%!
%! ## The Laguerre differentation matrices become singular for n>124.
%!
%! printf ("%3s\t%s\n\n", "n", "error");
%! for n = [2.^(0:6), 124]
%!   x = orthopolyquad ("laguerre", n);
%!   D = differentiation_matrices (x, 2);
%!   alpha = x .* exp (-x/2); # also the exact answer
%!   q = 1:2; 
%!   beta = (1 - (2.0 ./ x)*q) .* repmat ((-0.5).^q, n, 1);
%!   D = coerce_differentiation_matrices (D, alpha, beta);
%!   f = (D{2} + D{1}) \ (-alpha/4);
%!   printf ("%3d\t%e\n", n, norm (f - alpha));
%! endfor

%!demo
%!
%! ## 2pt BVP on the ray
%! 
%! ## u'' + 2 x u' = (2x-1) exp (-x), u (0) = u (Inf) = 0.
%!
%! ## exact solution: u (x) = erfc (x) - exp (-x)
%!
%! printf ("%3s\t%s\n\n", "n", "error");
%! for n = 8:8:112
%!   x = orthopolyquad ("laguerre", n, -0.5);
%!   D = differentiation_matrices (x, 2);
%!   alpha = x .* exp (-x/2); 
%!   q = 1:2; 
%!   beta = (1 - (2.0 ./ x)*q) .* repmat ((-0.5).^q, n, 1);
%!   D = coerce_differentiation_matrices (D, alpha, beta);
%!   f = (D{2} + diag (2*x) * D{1}) \ ((2*x-1).*exp(-x)) + exp (-x);
%!   printf ("%3d\t%e\n", n, norm (f - erfc (x)));
%! endfor

%!demo
%!
%! ## Schroedinger's equation (Weideman & Reddy 2000, Table XIII)
%! ## Apparently the exact answer is 1.424333, but we don't do too well
%! ## here, though I'm not sure why yet.
%! r = 5.08685476; epsi = 0.929852862;
%! exact = 1.424333;
%! printf ("%3s\t%- 14s\t%- 14s\n\n", "n", "lambda", "error");
%! for n = 16:2:64
%!   x = orthopolyquad ("laguerre", n, -0.5);
%!   D = differentiation_matrices (x, 2);
%!   alpha = x .* exp (-x/2);
%!   q = 1:2; 
%!   beta = (1 - (2.0 ./ x)*q) .* repmat ((-0.5).^q, n, 1);
%!   D = coerce_differentiation_matrices (D, alpha, beta);
%!   lambda = min (eig (diag (1+exp((x-r)/epsi)) * (eye (n) - D{2})));
%!   printf ("%3d\t%- 14.6e\t%- 14.6e\n", n, lambda, lambda-exact);
%! endfor
