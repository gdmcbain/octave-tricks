## Copyright (C) 2006, 2007 G. D. McBain <geordie_mcbain@yahoo.com.au>
##
## This program is free software.

## -*- texinfo -*-
## @deftypefn {Function File} {} map_differentiation_matrices (@var{D}, @var{t})
## Modify the differentiation matrices in the cell array @var{D} using
## the mapping whose j-th derivative at the i-th abscissa is
## @code{@var{t}(i,j)}.
##
## Basically this just involves the higher order chain rule.  As the
## formula for arbitrary order is too complicated (Johnson 2002), we use
## explicit expressions.
##
## For an example, see @code{map_ray_algebraic}.
##
## W. P. Johnson 2002 The curious history of Fa@`a di Bruno's formula.
## The American Mathematical Monthly 109:217--234.
## @end deftypefn

## Author: G. D. McBain <geordie.mcbain@aeromech.usyd.edu.au>
## Created: 2006-03-22
## Keywords: collocation, differentiation

function Dm = map_differentiation_matrices (D, t)
  
  k = length (D);

  Dm = D;

  Dm{1} = dmult (t(:,1), D{1});

  if (k > 1)

    Dm{2} = dmult (t(:,1).^2, D{2}) \
	+ dmult (t(:,2), D{1});
  
    if (k > 2)

      Dm{3} = dmult (t(:,1).^3, D{3}) \
	  + dmult (3 * t(:,1) .* t(:,2), D{2}) \
	  + dmult (t(:,3), D{1});

      if (k > 3)

	Dm{4} = dmult (t(:,1).^4, D{4}) \
	    + dmult (6 * t(:,1).^2 .* t(:,2), D{3}) \
	    + dmult (4 * t(:,1) .* t(:,3) + 3 * t(:,2).^2, D{2}) \
	    + dmult (t(:,4), D{1});

	if (k > 4)

	  warning ("order %d > 4 not implemented", k);
	  
	endif
      endif
    endif
  endif
  
endfunction

%!demo# do nothing, but advertise another demo
%! map_differentiation_matrices ({}, []);
%! "Try demo map_ray_algebraic."
