2007-05-28  G. D. McBain <gdmcbain@freeshell.org>

	* coerce_differentiation_matrices.m: Doc fix.

        * map_ray_algebraic.m: Copied here from map_halfline_algebraic.m.
	Removed the redundant modification factors for the quadrature
	weights.
	
	* map_differentiation_matrices.m: Copied here.

	* barycentric_interpolate.m: Rewrote help string.  Implemented
	demos from section 5.4 of Henrici (1982).  Added unit test.
	
	* barycentric_init.m: Allowed x to be a column rather than
	necessarily a row.  Completely rewrote help string.

2007-05-25  G. D. McBain <geordie_mcbain@yahoo.com.au>

	* barycentric_interpolate.m: Copied here from baryint.m, but
	separated all the arguments.

	* barycentric_init.m: Copied here from baryint_init2.m

	* differentiation_matrices.m, coerce_differentiation_matrices.m:
	Copied here.  Added a unit test and a demo on the barycentric
	weights.

	* orthopolyquad.m: Copied here to this new directory.  Moved
	HISTORY to this file.  Added six unit tests.  Corrected spelling
	error in error message.

2007-05-24  G. D. McBain <geordie_mcbain@yahoo.com.au>

	* orthopolyquad.m: Altered white space.  Added full GNU copyright
	notice.

2007-04-18  Geordie McBain  <geordie.mcbain@aeromech.usyd.edu.au>

	* differentiation_matrices.m: Documented that the first returned
	value is a cell array of differentiation matrices.

	* differentiation_matrices.m: Moved HISTORY here.

2007-04-02  Geordie McBain  <geordie.mcbain@aeromech.usyd.edu.au>

	* differentiation_matrices.m: realized that the mathematics behind
	the `original' method for diagonals had already been discovered by
	Huang & Sloan (1994) and so correctly added attribution to
	comments

2006-11-29  Geordie McBain  <geordie.mcbain@aeromech.usyd.edu.au>

	* differentiation_matrices.m: reverted to the original method for
	diagonals, since the row-sum trick performs so badly for
	generalized Laguerre points and the associated exponential
	coercion on the ray

2006-11-02  G. D. McBain <geordie_mcbain@yahoo.com.au>

	* map_differentiation_matrices.m: Added a null demo advertising
	demo map_halfline_algebraic.

2006-09-05  G. D. McBain <geordie_mcbain@yahoo.com.au>

	* differentiation_matrices.m, coerce_differentiation_matrices.m,
	map_differentiation_matrices.m: Changed the return value from a
	three-dimensional array to a cell array of square matrices.

2006-09-04  Geordie McBain  <geordie.mcbain@aeromech.usyd.edu.au>

	* differentiation_matrices.m: Readopted the row-sum trick for
	computing diagonal elements.

2006-06-30  G. D. McBain <geordie_mcbain@yahoo.com.au>

	* orthopolyquad.m: Simplified call to spdiags.

2006-05-29  G. D. McBain <geordie_mcbain@yahoo.com.au>

	* map_differentiation_matrices.m: Handle trivial k = 0 case
	gracefully.

2006-05-18  G. D. McBain <geordie_mcbain@yahoo.com.au>

	* orthopolyquad.m: Assembled Jacobi matrix with one call to
	spdiags instead of summing multiple calls to diag.  Added data
	from Ma & Korunsky (2004) to demo.

2006-05-04  Geordie McBain  <geordie.mcbain@aeromech.usyd.edu.au>

	* differentiation_matrices.m: Added some comments.  Tidied code.

2006-05-03  G. D. McBain <geordie_mcbain@yahoo.com.au>

	* orthopolyquad.m: New file, formed from the merger of
	{glag,jacobi}quad, with the addition of Hermite quadrature.

2006-04-28  Geordie McBain  <geordie.mcbain@aeromech.usyd.edu.au>

        * map_halfline_algebraic.m.: Changed factorial to gamma, since
	factorial isn't standard Octave.
	
2006-04-18  Geordie McBain  <geordie.mcbain@aeromech.usyd.edu.au>

	* differentiation_matrices.m: Changed @code{factorial (q-1)} to
	@code{gamma (q)}, as @code{gamma} is standard Octave.

2006-04-05  Geordie McBain  <geordie.mcbain@aeromech.usyd.edu.au>

	* differentiation_matrices.m: Dumped coercion altogether.

2006-04-04  Geordie McBain  <geordie.mcbain@aeromech.usyd.edu.au>

	* differentiation_matrices.m: Adapted from
	differentiation_matrices, delegating to
	coerce_differentiation_matrices in case of weighting, and using
	explicit expressions for diagonal elements.

2006-03-24   Geordie McBain  <geordie.mcbain@aeromech.usyd.edu.au>

	* map_halfline_algebraic.m: New file.
	
2006-03-22   Geordie McBain  <geordie.mcbain@aeromech.usyd.edu.au>

	* differentiation_matrices.m: Test n>1 before treating offdiagonal
	elements.

	* coerce_differentiation_matrices.m,
	map_differentiation_matrices.m: New files.

2006-03-02  G. D. McBain <geordie_mcbain@yahoo.com.au>

	* baryint_init2.m: Doc fix.

2006-02-06  G. D. McBain <geordie_mcbain@yahoo.com.au>

	* differentiation_matrices.m: New file.

2006-02-01  G. D. McBain <geordie_mcbain@yahoo.com.au>

	* baryint_init2.m, baryint.m: New files.
