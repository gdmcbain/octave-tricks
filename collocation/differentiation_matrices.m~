## Copyright (C) 2006, 2007 G. D. McBain <geordie_mcbain@yahoo.com.au>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or (at
## your option) any later version.
##
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301USA

## -*- texinfo -*-
## @deftypefn {Function File} {} [@var{D}, @var{w}] = differentiation_matrices (@var{x}, @var{q})
## Return the @var{q} matrices in a cell array @var{D} that turn columns
## of ordinates into columns of derivatives, as approximated by
## differentiating the polynomial interpolating on the abscissae
## @var{x}. Also return the weights @var{w} of the barycentric
## polynomial interpolation formula.
##
## For a general discussion of differentiation matrices, see Welfert
## (1997), Weideman & Reddy (2000), or Berrut & Trefethen (2004, Section
## 9.3).
##
## Here we compute the offdiagonal elements using Theorem 2.1 of Welfert
## (1997) and the diagonal elements an original method which actually is
## probably equivalent to that in equation 3.11 of Huang & Sloan (1994).
## Other methods for the diagonals include the `row-sum trick' and one
## by Welfert (1997) which was used by Weideman & Reddy (2000). I find
## that the row-sum trick is very poor for generalized Laguerre points;
## this is consistent with Weideman & Reddy (2000), whereas the present
## method performs acceptably.
##
## A. Bayliss, A. Class, & B. Matkowsky 1994 Roundoff error in computing
## derivatives using the Chebyshev differentiation matrix. @cite{Journal
## of Computational Physics} @strong{116}:380--383.
##
## J.-P. Berrut & L. N. Trefethen 2004 Barycentric Lagrange
## interpolation. @cite{SIAM Review} @strong{46}:501--517.
##
## D. E. Christie 1952 @cite{Intermediate College Mechanics}.
## McGraw-Hill, New York.
##
## B. Costa & W. S. Don 2000 On the computation of high order
## pseudospectral derivatives.  @cite{Applied Numerical Mathematics}
## @strong{33}:151--159.
##
## E. M. E. Elbarbary & S. M. El-Sayed 2005 Higher order pseudospectral
## differentiation matrices.  @cite{Applied Numerical Mathematics}
## @strong{55}(4):425--438.
##
## B. Fornberg 1988 Generation of finite difference formulas on
## arbitrarily spaced grids.  @cite{Mathematics of Computation}
## @strong{51}(184):699--706.
##
## W. Huang & D. M. Sloan 1994 The pseudospectral method for solving
## differential eigenvalue problems.  @cite{Journal of Computational
## Physics} @strong{111}:399--409.
##
## J. A. C. Weideman & S. C. Reddy 2000 A MATLAB differentiation suite.
## @cite{ACM Transactions on Mathematical Software}
## @strong{26}(4):465--519.
##
## B. Welfert 1997 Generation of pseudospectral differentiation matrices
## I.  @cite{SIAM Journal on Numerical Analysis} @strong{34}:1640--1657.
## @seealso{coerce_differentiation_matrices, map_differentiation_matrices} 
## @end deftypefn

## ### TODO
##
## The method of Huang & Sloan (1994) for off-diagonal elements might be
## investigated.

## ### REFERENCES

## Author: G. D. McBain <geordie.mcbain@aeromech.usyd.edu.au>
## Created: 2006-02-06
## Keywords: collocation, differentiation, interpolation

function [DM, w] = differentiation_matrices (x, order)

  x = x(:);
  n = length (x);

  d = repmat (x, [1, n]);	# d(i,j) = x(i)
  d = d - d';			# d(i,j) = x(i) - x(j)
  D = eye (n);			# zeroth derivative is identity
  diagonal = logical (D);	# a mask for the diagonal
  d(diagonal) = 1;		# so diagonals don't contribute to product
  w = 1 ./ prod (d, 2);		# barycentric weights
  h = (1 ./ w) * w' ;		# h(i,j) = w(j) / w(i)
  d(diagonal) = Inf;		# so reciprocals of diagonals are 0

  DM = cell (1, order);

  for q = 1:order
    
    if n>1			# i.e. offdiagonals exist
      D = q * (dmult (diag (D), h) - D) ./ d; 
    endif

    D(diagonal) = (-1)^(q-1) * gamma (q) * sum (d.^-q, 2);
    for m = 1:(q-1)
      D(diagonal) -= (-1)^m * prod (q - (1:m-1)) * \
	  (diag (DM{q-m}) .* sum (d.^-m, 2));
    endfor

    DM{q} = D;
  endfor

endfunction

%!demo
%!
%! ## 1st-3rd derivatives of monomials on Gauss's abscissae
%!
%! n = 128; 
%! Q = 4;
%! x = orthopolyquad ("legendre", n);
%! D = differentiation_matrices (x, Q);
%! printf ("%3s\t%14s\t%14s\t%14s\t%14s\n\n", "k", "error 1", "error 2", "error 3", "error 4");
%! for k = 2 .^ (0:8)
%!   fp = D{1} * (x .^ k);
%!   fpp = D{2} * (x .^ k);
%!   fppp = D{3} * (x .^ k);
%!   fp4 = D{4} * (x .^ k);
%!   printf ("%3d\t%- 14.6e\t%- 14.6e\t%- 14.6e\t%- 14.6e\n", k, norm (fp - k*x.^(k-1)), norm (fpp - k*(k-1)*x.^(k-2)), norm (fppp - k*(k-1)*(k-2)*x.^(k-3)), norm (fppp - k*(k-1)*(k-2)*(k-3)*x.^(k-4)));
%! endfor
