## Copyright (C) 2006, 2007 G. D. McBain <geordie_mcbain@yahoo.com.au>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or (at
## your option) any later version.
##
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301USA

## -*- texinfo -*-
## @deftypefn {Function File} {} barycentric_init (@var{x}, @var{w}, @var{s})
## @deftypefnx {Function File} {} barycentric_init (@var{x}, @var{w}, @var{s}, @var{alpha})
## Return an object for barycentric interpolation from abscissae @var{x}
## with weights @var{w} computed by @code{[@var{D}, @var{w}] =
## differentiation_matrices (@var{x}, q)} (with q irrelevant, say 0) to
## abscissae @var{s}, and optional weighting function @var{alpha}.  The
## object is of the type required as the first argument to
## @code{barycentric_interpolate}.
## @end deftypefn

## ### IMPLEMENTATION NOTES
##
## Are there more efficient ways of doing things than using repmat?

## ### REFERENCES
##
## J.-P. Berrut & L. N. Trefethen 2004 Barycentric Lagrange
## interpolation. SIAM Review 46:501--517.
##
## P. Henrici 1982 Essentials of Numerical Analysis.  Wiley.  New York.
##
## J. A. C. Weideman & S. C. Reddy 2000 A MATLAB differentiation suite.
## ACM Transactions on Mathematical Software 26(4):465--519.

## Author: G. D. McBain <geordie.mcbain@aeromech.usyd.edu.au>
## Created: 2006-02-01
## Keywords: weighted polynomial barycentric interpolation

function bary = barycentric_init (x, w, s, alpha)

  s = s(:);
  d = repmat (s, 1, length (x)) - repmat (x(:) .', length (s), 1);

  [olds, oldx] = find (~d); # s(olds)' == x(oldx), s actually old
  news = complement (olds, 1:length (s)) '; # s indeed new

  num = repmat (w', length (news), 1) ./ d(news,:);
  den = sum (num, 2);

  if (nargin == 4)		# weighted polynomial interpolation
    num .*= repmat (alpha (s(news)), 1, length (x)) \
	./ repmat (alpha (x) (:) .', length (news), 1);
  endif
  
  bary = struct ("olds", olds, "news", news, "oldx", oldx, 
		 "num", num, "den", den);
endfunction
