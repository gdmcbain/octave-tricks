## Copyright (C) 2006, 2007 G. D. McBain <gdmcbain@freeshell.org>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or (at
## your option) any later version.
##
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301USA

## -*- texinfo -*-
## @deftypefn {Function File} {} [@var{x}, @var{tau}] = map_ray_algebraic (@var{t}, @var{L}, @var{k})
## Map points on the segment @code{-1 < @var{t} < 1} to the ray @code{0
## < @var{x} < Inf} using the algebraic mapping
##
## @example
## @group
## @var{t} = (@var{x} - @var{l}) / (@var{x} + @var{l})
## @var{x} = @var{l} * (1 + @var{t}) / (1 - @var{t}) 
## @end group 
## @end example
##
## Also return the first @var{k} derivatives @var{tau} of the mapping at
## the abscissae; i.e.
##
## @example
##   tau(i,j) = t^(j) (@var{x}(i)) = 2 @var{l} (-1)^(j+1) j! / (@var{x}(i) + @var{l})^(j+1)
## @end example
##
## These derivatives are required by
## @code{map_differentiation_matrices}. The transposed reciprocal of the
## first column can also be used to modify the weights from a quadrature
## rule for the segment; i.e. if
##
## @example
## q * @command{f} ~= quad (@@@command{f}, -1, 1) 
## @end example
##
## then
##
## @example
## (q ./ tau(:,1)') * @command{f} ~= quad (@@@command{f}, 0, Inf) 
## @end example
##
## The mapping has been used and discussed by Grosch & Orszag (1977),
## Delves & Mohamed (1985, p. 42), Herron, von Kerczek, & Tozzi (1985)
## and Boyd (1987) for various problems in applied analysis.
##
## M. Abramowitz & I. Stegun (eds) 1965 Handbook of Mathematical
## Functions, Dover, New York.
##
## J. P. Boyd 1987 Orthogonal rational functions on a semi-infinite
## interval. Journal of Computational Physics 70:63--88.
##
## L. M. Delves & J. L. Mohamed 1985 Computational Methods for Integral
## Equations. Cambridge University Press.
##
## W. Gautschi 1965 Error function and Fresnel integrals.  In Abramowitz
## & Stegun, ch. 7, pp. 295--329.
##
## A. E. Gill & A. Davey 1969 Instabilities of a buoyancy driven system.
## Journal of Fluid Mechanics 35:775--798.
##
## C. E. Grosch & S. A. Orszag 1977 Numerical solution of problems in
## unbounded regions: coordinate transforms.  Journal of Computational
## Physics 25:273--296.
##
## I. H. Herron, C. H. von Kerczek, & J. Tozzi 1985 Instability
## characteristics of the asymptotic suction profile.  Journal of
## Applied Mechanics 52:487.
##
## D. S. Liepman 1965 Mathematical constants.  In Abramowitz & Stegun,
## ch. 1.
##
## E. T. Whittaker & G. N. Watson 1927 A Course of Modern Analysis, 4th
## ed., Cambridge University Press.
## @seealso{map_differentiation_matrices, mhalegdif} 
## @end deftypefn

## Author: G. D. McBain <geordie.mcbain@aeromech.usyd.edu.au>
## Created: 2006-03-24
## Keywords: collocation, differentiation

function [x, tau, sigma] = map_ray_algebraic (t, L, order)

  n = length (t);
  j = 1:order;

  x = L * (1 + t) ./ (1 - t);
  tau = repmat (-2*L*(-1).^j .* gamma (j+1), [n, 1]) \
      ./ repmat (x+L, [1, order]) .^ repmat (j+1, [n, 1]);
  sigma = 2 * L ./ (1 - t') .^ 2;

endfunction

%!demo
%!
%! ## differentiatiation: 1/(x+1) twice.
%!
%! printf ("%3s\t%14s\n\n", "n", "error");
%! for n = 16:16:256
%!   order = 2;
%!   t = orthopolyquad ("legendre", n);
%!   Dt = differentiation_matrices (t, order);
%!   [x, tau] = map_ray_algebraic (t, 1, order);
%!   D = map_differentiation_matrices (Dt, tau);
%!   y = 1 ./ (x+1);
%!   ypp = 2 ./ (x+1).^3;
%!   yppn = D{2} * y;
%!   printf ("%3d\t%- 14.6e\n", n, norm (ypp-yppn));
%! endfor

%!demo
%!
%! ## 2pt BVP: u'' + u' = -x*exp(-x/2)/4, u (0) = u (Inf) = 0
%! ## => u (x) = x .* exp (-x/2);
%! ## Using algebraically mapped ultraspherical polynomials.
%! ## Here we need to enforce the far-field condition, because the
%! ## complementary function 1 - exp (-x) satisfies the 
%! ## Dirichlet condition at the origin as well as the u'(Inf)=0 
%! ## condition implicit in the algebraic mapping.
%! order = 2;
%! printf ("%3s\t%- 14.6s\n\n", "n", "error");
%! for n = 16:16:256
%!   t = orthopolyquad ("ultraspherical", n, 2);
%!   Dt = differentiation_matrices (t, order);
%!   alpha = 1 - t.^2;
%!   beta = -2*[t, ones(n,order-1)] ./ repmat (alpha, [1, order]);
%!   Dt = coerce_differentiation_matrices (Dt, alpha, beta);
%!   [x, tau] = map_ray_algebraic (t, 1, order);
%!   D = map_differentiation_matrices (Dt, tau);
%!   ux = x .* exp (-x/2);
%!   u = (D{2} + D{1})  \ (-ux/4);
%!   printf ("%3d\t%- 14.6e\n", n, norm (u - ux));
%! endfor

%!demo
%!
%! ## 2pt BVP: u'' + u = -(1-5x/4)*exp(-x/2), u(0) = u'(Inf) = 0
%! ## => u (x) = x .* exp (-x/2);
%! ## Using algebraically mapped Jacobi polynomials.
%! ## Here we don't need to enforce the far-field condition, because
%! ## it's implicit in the algebraic mapping, and no nonzero complementary 
%! ## function satisfies both boundary conditions
%! order = 2;
%! printf ("%3s\t%14s\n\n", "n", "error");
%! for n = 2 .^ (0:8)
%!   t = orthopolyquad ("jacobi", n, 0, 2);
%!   Dt = differentiation_matrices (t, order);
%!   alpha = 1 + t;
%!   beta = [ones(n,1), zeros(n,order-1)] ./ repmat (alpha, [1, order]);
%!   Dt = coerce_differentiation_matrices (Dt, alpha, beta);
%!   [x, tau] = map_ray_algebraic (t, 1, order);
%!   D = map_differentiation_matrices (Dt, tau);
%!   u = (D{2} + eye (n))  \ (-(1-5*x/4).*exp(-x/2));
%!   printf ("%3d\t%- 14.6e\n", n, norm (u - (x .* exp (-x/2))));
%! endfor

%!demo
%!
%! ## 2pt BVP for erfc (Gautschi 1965, p. 299)
%! ## actually for u (x) = erfc (x) - exp (-x), viz.
%! ## u'' + 2 x u' = (2x-1) exp (-x), u (0) = u (Inf) = 0.
%! ## This problem does need the far-field condition enforced.
%! order = 2;
%! printf ("%3s\t%14s\n\n", "n", "error");
%! for n = 2 .^ (0:8)
%!   t = orthopolyquad ("legendre", n);
%!   Dt = differentiation_matrices (t, order);
%!   alpha = 1 - t.^2;
%!   beta = -2*[t, ones(n,order-1)] ./ repmat (alpha, [1, order]);
%!   Dt = coerce_differentiation_matrices (Dt, alpha, beta);
%!   [x, tau] = map_ray_algebraic (t, L = 1, order);
%!   D = map_differentiation_matrices (Dt, tau);
%!   u = (D{2} + dmult (2*x, D{1})) \ ((2*x-1) .* exp (-x)) + exp (-x);
%!   printf ("%3d\t%- 14.6e\n", n, norm (u - erfc (x)));
%! endfor

%!demo
%!
%! ## vector 2pt BVP: base solution of Gill & Davey (1969)
%! ## The problem is V'' + 2 T = T'' - 2 V = 0, 
%! ## with V (0) = 0, T (0) = 1 and decay for large x.  
%! ## Render the thermal boundary condition homogeneous 
%! ## by putting T = U + exp (-x), then the problem is
%! ## V'' + 2 U = - 2 exp (-x), U'' - 2 V = - exp (-x) 
%! ## with homogeneous Dirichlet conditions at the wall.
%! order = 2;
%! L = 1.0;
%! printf ("%3s\t%11s\n\n", "n", "error");
%! for n = 2 .^ (0:7)
%!   t = orthopolyquad ("jacobi", n, 0, 2);
%!   Dt = differentiation_matrices (t, order);
%!   alpha = 1 + t;
%!   beta = [ones(n,1), zeros(n,1)] ./ repmat (alpha, [1, order]);
%!   Dt = coerce_differentiation_matrices (Dt, alpha, beta);
%!   [x, tau] = map_ray_algebraic (t, 1, order);
%!   D = map_differentiation_matrices (Dt, tau);
%!   A = [D{2}, 2*eye(n); -2*eye(n), D{2}];
%!   u = A \ kron ([2; 1], -exp (-x));
%!   V = u(1:n);
%!   Theta = u(n+1:2*n) + exp (-x);
%!   Vx = exp (-x) .* sin (x);
%!   Thetax = exp (-x) .* cos (x);
%!   err = norm (V - Vx) + norm (Theta - Thetax);
%!   printf ("%3d\t%11.5e\n", n, err);
%! endfor
%! plot (x, V, ";V;", x, Theta, ";Theta;")
%! axis ([0, 5, -0.2, 1])
%! title ("base solution of Gill & Davey (1969)")

### What we haven't done yet: the same again but specifying the gradient
### at the wall and vanishing in the far field.  This will require a new
### way of imposing the Neumann condition...

%!demo
%!
%! ## quadrature: Euler's constant 0.577215664901532860606512... 
%! ## gamma = quad (@ (x) exp(-x)*(1/(1-exp(-x)) - 1/x), 0, Inf);
%! ## (Whittaker & Watson 1927, p. 246)
%! gamma = 0.577215664901532860606512; # (Liepman 1965, p. 3)
%! printf ("%3s\t%14s\t%14s\n\n", "n", "integral", "error");
%! for n = 2 .^ (0:9)
%!   [t, q] = orthopolyquad ("Legendre", n);
%!   [x, tau] = map_ray_algebraic (t, 1.0, 1);
%!   I = (q ./ tau(:,1)') * (exp (-x) .* (1.0 ./ (1 - exp (-x)) - 1.0 ./ x));
%!   printf ("%3d\t%- 14.6e\t%- 14.6e\n", n, I, I-gamma);
%! endfor
