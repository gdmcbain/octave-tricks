## Copyright (C) 2010 G. D. McBain -*- octave -*-

## -*- texinfo -*- 
## @deftypefn {Function File} {[@var{x}, @var{y}]} = isolines (@var{c}) 
## Convert the output @var{c} of @code{contourc} to something which can
## be plotted in the plane with @code{plot (@var{x}, @var{y})}.
##
## This is particularly useful if one wishes to superimpose other plots
## on the contours; see the demo.
## @seealso{contourc, plot}
## @end deftypefn

## ### Implementation notes
##
## The function contourc returns `a 2 by N matrix containing the contour
## lines in the following format C = [lev1, x1, x2, ..., LEVN, x1, x2,
## ...; len1, y1, y2, ..., LENN, y1, y2, ...] in which contour line N
## has a level (height) of LEVN and length of LENN.' (from its help
## string). Here we simply replace the level values and lengths with
## NaNs and separate the rows so that plot can plot the level sets as
## polygonal lines.

## Author: G. D. McBain <gdmcbain@freeshell.org>
## Created: 2010-12-11
## Keywords: two-dimensional plotting, contouring

function [x, y] = isolines (c)
  j = 1;
  do
    l = c(2,j);
    c(:,j) = nan (2, 1);
    j += l + 1;
  until (j >= columns (c))
  x = c(1,2:end);
  y = c(2,2:end);
endfunction

%!demo
%! [x, y, z] = peaks (50); # the example from contourf
%! c = contourc (x, y, z);
%! [X, Y] = isolines (c);
%! plot (X, Y, "k-", [0, 0], [0, 1], "r-")
%! axis ("image", "off")
