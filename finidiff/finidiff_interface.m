## Copyright (C) 2007 G. D. McBain <gdmcbain@protonmail.com>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or (at
## your option) any later version.
##
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301USA

## -*- texinfo -*-
## @deftypefn {Function File} {} finidiff_interface  (@var{D}, @var{x}, @var{s}, @var{order}, @var{kl}, @var{kr})
## Return a differentiation matrix, modified from @var{D}, of
## @var{order} 1 or 2 for a grid @var{x} with an interface at @var{s}
## such that the value of the unknown is continuous and is so is k times
## the gradient where k is @var{kl} for @code{@var{x} < @var{s}} and
## @var{kr} for @code{@var{s} < @var{x}}.
##
## The sparseness of @var{D} is preserved.
## 
## @strong{Theory} If on the left and right we wish to impose
##
## @example
## @group
## wl(-2:0) * y(-2:0)
##  wr(0:2) * y(0:2)
## @end group
## @end example
##
## but we don't have access to y(0), although we know it's
## single-valued, corresponding to the continuity of value across the
## interface, then we form the continuity of flux condition:
##
## @example
## kl * hl(-2:0) * y(-2:0) = kr * hr(0:2) * y(0:2)
## @end example
##
## from which we extract the expression for the interfacial temperature, 
##
## @example
## @group
## y(0) = kl * hl(-2:-1)/h * y(-2:-1) - kr * hr(1:2)/h * y(1:2) 
##      
##      = [kl * hl(-2:-1), kr * hr(1:2)]/h * y([-2:-1, 1:2])
## @end group
## @end example
##
## where 
##
## @example
## h = kr * hr(0) - kl * hl(0)
## @end example
##
## Thus we express the first or second derivative at the points adjacent
## the interface with stencils including two points either side.
## @seealso{finidiff}
## @end deftypefn

## Author: G. D. McBain <gdmcbain@protonmail.com>
## Created: 2007-05-31
## Keywords: finite difference

function D = finidiff_interface (D, x, s, order, p, q)

  if (nargin < 6)
    print_usage ();
  endif

  n = length (x);
  i = sum (x<s);		# x(i:i+1) crosses interface
  
  assert (any (order == [1, 2])); # only for 1st & 2nd at present
  
  oldstencill = [x((i-1):i); s]; # left peripheral
  oldstencilr = [s; x(i+(1:2))]; # right peripheral
  hl = p * finidiff (oldstencill, s, 1); # left flux
  hr = q * finidiff (oldstencilr, s, 1); # righxot flux
  h = hr(1) - hl(3);
  newstencil = i+(-1:2);	# two points either side
  kl = finidiff (oldstencill, x(i), order);
  kr = finidiff (oldstencilr, x(i+1), order);
  h0 = horzcat (hl(1:2), -hr(2:3)) / h;
  D(i,newstencil) = postpad (kl(1:2), 4) + kl(3)*h0;
  D(i+1,newstencil) = prepad (kr(2:3), 4) + kr(1)*h0;

endfunction

%!test
%!
%! ## steady conduction of heat in a composite slab with uniform 
%! ## volumetric generation and cold sides
%!
%! ## (k u')' = -1, u (+/- 1) = 0,
%!
%! ## k = 0.1 (x<0); 10 (x>0).
%!
%! kl = 0.1;
%! kr = 10;
%!
%! n = 32;
%! x = sort (2 * rand (n, 1) - 1);
%!
%! D = finidiff_matrix_dirichlet (x, 2, -1, 1);
%! D = finidiff_interface (D, x, 0, 2, kl, kr);	# interfacial condition
%!
%! k = kl * ones (size (x));	     # thermal conductivity...
%! k(x>0) = kr;			     # ...has a jump
%! u = D \ (-1./k);		     # solve linear system
%!
%! u_exact = 1/2/kl * ((1-x.^2) + (kl-kr)/(kl+kr)*(1+x));
%! u_exact(x>0) = 1/2/kr * ((1-x.^2) + (kr-kl)/(kr+kl)*(1-x)) (x>0);
%! assert (u, u_exact, 1e-12);

%!demo
%!
%! ## steady conduction of heat in a composite slab with uniform 
%! ## volumetric generation and cold sides
%!
%! ## (k u')' = -1, u (+/- 1) = 0,
%!
%! ## k = 0.1 (x<0); 10 (x>0).
%!
%! kl = 0.1;
%! kr = 10;
%!
%! n = 32;
%! x = sort (2 * rand (n, 1) - 1);
%!
%! D = finidiff_matrix_dirichlet (x, 2, -1, 1);
%! D = finidiff_interface (D, x, 0, 2, kl, kr);	# interfacial condition
%!
%! k = kl * ones (size (x));	     # thermal conductivity...
%! k(x>0) = kr;			     # ...has a jump
%! u = D \ (-1./k);		     # solve linear system
%!
%! x1 = linspace (-1, 1, 10*n); # for exact solution
%! y1 = 1/2/kl * ((1-x1.^2) + (kl-kr)/(kl+kr)*(1+x1));
%! y1(x1>0) = 1/2/kr * ((1-x1.^2) + (kr-kl)/(kr+kl)*(1-x1)) (x1>0);
%! plot (x, u, "*;numerical;", x1, y1, "-;exact;")
