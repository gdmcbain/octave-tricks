## Copyright (C) 2007 G. D. McBain <gdmcbain@protonmail.com>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or (at
## your option) any later version.
##
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301USA

## -*- texinfo -*-
## @deftypefn {Function File} {} richardson_extrapolate (@var{a}, @var{h}, @var{n})
## Estimate the limit of the sequence @var{a}, supposed to depend
## termwise on the sequence @var{h} as the limit plus a constant times
## @var{h}^@var{n}; i.e. the @var{h}^@var{n} extrapolation of Richardson
## (1927).
##
## L. F. Richardson 1927 The deferred approach to the limit.  Part
## I.---Single lattice.  @cite{Philosophical Transactions of the Royal
## Society of London.  Series A, containing Papers of a Mathematical or
## Physical Character}.  @strong{226}:299--349.
##
## @end deftypefn

## Author: G. D. McBain <geordie.mcbain@aeromech.usyd.edu.au>
## Create: 2006-02-16
## Keywords: extrapolation, deferred approach to the limit

function r = richardson_extrapolate (a, h, n)
  r = a(2:end) + diff (a) ./ ((h(1:end-1)./h(2:end)).^n - 1);
endfunction

%!demo
%!
%! ## `An ancient problem retouched' Richardson (1927, Section 3.1)
%!
%! p = @ (n) 2*n .* sin (pi./n); # length of inscribed regular n-gon
%! n = [4, 6]; # square, hexagon
%! pr = richardson_extrapolate (p (n), n, -2)
%! # `The error in the extrapolated value is thus only 1/33 of the error
%! # `in the better of the two values from which it was derived.'
%! min (abs (p (n) - 2*pi)) / abs (pr - 2*pi)
%! # `To get as good a result from a single inscribed regular polygon it 
%! # `would need to have 35 sides'
%! p (34:35)

%!demo
%!
%! ## `Napier's exponential base' (Richardson 1927, Section 3.2)
%!
%! p = @ (n) ((2*n+1)./(2*n-1)).^n;
%! n = 4:5;
%! er = richardson_extrapolate (p (n), n, -2)
%! ererr = er - e
%! p5err = p(5) - e
%! abs (p5err / ererr)
%! p (44:45) - e
