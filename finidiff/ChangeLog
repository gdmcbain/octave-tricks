2007-05-31    G. D. McBain  <gdmcbain@freeshell.org>

	* finidiff_interface.m: New file.

2007-05-30  G. D. McBain  <gdmcbain@freeshell.org>

	* grid_cellcentred_n.m: Added code to handle special case of equal
	spacing (r=1) which previously caused a 0/0 error.  Added a demo
	for the continuity of the special case with the general case.

	* (all files): Changed e-mail address to gdmcbain@freeshell.org.

	* finidiff_matrix2_mod.m: Added unit test based on first demo.

2007-05-29  G. D. McBain  <gdmcbain@freeshell.org>

	* finidiff_matrix2_mod.m: Simplified the expression for modified
	operators near boundaries and added their derivations to help
	string.

2007-05-25  G. D. McBain  <gdmcbain@freeshell.org>

	* finidiff_matrix2_mod.m: Added demo for interfacial condition
	(steady conduction of heat in a composite slab).

	* finidiff_quad_trapezoidal.m: Added unit tests.

2007-05-24  G. D. McBain  <gdmcbain@freeshell.org>

	* finidiff_quad_trapezoidal.m: Copied here from
	fd_quad_trapezoidal.m.

	* finidiff_matrix_dirichlet.m: Added check that the order wasn't
	so large that the special boundary stencils for points near the
	left boundary didn't exceed the right boundary.

	* finidiff_matrix.m: Made some notes on the extension from the
	line to the plane or space.  Added white space to the demo (in the
	source code and in the output).

2007-05-23  G. D. McBain  <gdmcbain@freeshell.org>

	* richardson_extrapolate.m: Moved to this directory.

	* grid_cellcentred.m, grid_cellcentred_n.m: Changed demos to match
	the grids described in the draft paper for J. C. Patterson &
	S. W. Armfield; i.e. (1) h = 0.08, r = 1.1, and n = 47, and (2) h
	= 0.16, r = 1+5*h/4.

2007-05-22  G. D. McBain  <gdmcbain@freeshell.org>

	* finidiff_matrix_dirichlet.m: Removed reference in a demo to
	obsolete dense version.

2007-05-22  G. D. McBain  <gdmcbain@freeshell.org>

	* finidiff.m, finidiff_matrix.m, finidiff_matrix_dirichlet.m,
	finidiff_matrix2_mod.m: Deleted excessive white space in help
	string.

	* grid_cellcentred.m, grid_cellcentred_n.m: New files.

	* finidiff_matrix.m: Added much white space.  Reverted to in-loop
	computation of the row indices; moving the Kronecker product
	vectorized version to comments under the heading `Implementation
	notes'.  Checked for exactly two input arguments.

2007-05-21  G. D. McBain  <gdmcbain@freeshell.org>

	* finidiff_matrix2_mod.m: Renamed from fd_matrix2_mod.m.

	* finidiff_matrix.m: Renamed from sp_fd_matrix.m.  Doc fix. 

	* finidiff_matrix_dirichlet.m: Renamed from sp_fd_matrix_dirichlet.m.

	* finidiff.m: Renamed from fd_stencil.m.

2007-05-08  G. D. McBain  <gdmcbain@freeshell.org>

	* sp_fd_matrix.m: Doc fix.

	* sp_fd_matrix_dirichlet.m: Wrote help string and added copyright
	notice.

2007-04-20  G. D. McBain  <gdmcbain@freeshell.org>

	* fd_stencil.m: Added more working to theoretical description in
	comments.

2007-04-04  G. D. McBain  <gdmcbain@freeshell.org>

	* fd_stencil.m: Checked for enough arguments.

	* fd_stencil.m: Moved HISTORY to ChangeLog.

2007-03-16  G. D. McBain  <gdmcbain@freeshell.org>

	* sp_fd_matrix.m: Adapted from dense fd_matrix.m.

	* fd_matrix2_mod.m: Added note to help that sparseness is
	preserved.

2007-03-05  G. D. McBain  <gdmcbain@freeshell.org>

	* fd_matrix2_mod.m: Added full GNU copyright statement.

2007-01-23  G. D. McBain  <gdmcbain@freeshell.org>

	* fd_matrix2_mod.m: Corrected a bug in the internal interface code
	where it had been assumed in some places that the interface was at
	zero instead of the input parameter s.2006-11-29 Geordie McBain
	<geordie@geordie3.aeromech.usyd.edu.au>

	* fd_stencil.m: Changed the default value of the monomial's offset
	z from zero to the evaluation point s.

