## Copyright (C) 2007 G. D. McBain <gdmcbain@protonmail.com>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

## -*- texinfo -*-
## @deftypefn {Function File} {} grid_cellcentred (@var{h}, @var{r}, @var{n})
## Compute the @var{n} centres of cells of a grid in which the first cell 
## begins at zero and in which the cell widths increase geometrically with 
## common ratio @var{r} from @var{h}.
## @seealso{grid_cellcentred_n}
## @end deftypefn

## Author: G. D. McBain <geordie.mcbain@aeromech.usyd.edu.au>
## Created: 2007-02-12
## Keywords: grid

function x = grid_cellcentred (h, r, n)
    ## x = h / (r-1) * (r.^((1:n)'-1) * (r+1)/2 - 1);
    w = h * r .^ (0:(n-1))';
    x = cumsum (w) - w / 2;
endfunction

%!demo
%!
%! ## `with h=0.08, r=1.1, and n=47, which puts xi_{+/- n} .= 66.551'
%!
%! grid_cellcentred (0.08, 1.1, 47) (end)

%!demo 
%!
%! ## `required n > [17] to extend to xi_n > 17 
%! ## or n > 20 to extend to xi_n > 32'
%!
%! h = 0.16
%! r = 1 + 5*h/4
%! printf ("\n%2s\t%s\n\n", "n", "xi_n");
%! for n = 17:21
%!  xin = grid_cellcentred (h, r, n) (end);
%!  printf ("%2d\t%f\n", n, xin);
%! endfor
