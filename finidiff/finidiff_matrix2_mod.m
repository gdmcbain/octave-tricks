## Copyright (C) 2006, 2007 G. D. McBain <gdmcbain@protonmail.com>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or (at
## your option) any later version.
##
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301USA

## -*- texinfo -*-
## @deftypefn {Function File} {} finidiff_matrix2_mod (@var{D}, @var{x}, @var{s}, @var{p}, @var{q})
## Modify the finite difference differentiation matrix @var{D} for the
## grid @var{x} to impose a boundary condition at @var{s}.  For a
## homogeneous Dirichlet or Neumann condition at an exterior point,
## @var{p} is 0 or 1 and @var{q} isn't needed.  For an interfacial
## condition, @var{p} and @var{q} are the thermal conductivities on the
## left and right sides of @var{s}.
##
## Sparseness is preserved.
##
## Say a homogeneous condition were to be imposed at the left, so at the
## nonexistent point x(0).  Say the linear operator involved was [b0,
## b1, b2] and it was to operate on f(x(0:2)): b*f=0.  From this we
## get an expression for y(0): y(0) = -b(1:2)*y(1:2)/b(0)
##
## Say the field operation we wanted to use was
##
## @example
## @group
## [k0, k1, k2] * [y0;
##                 y1;
##                 y2]
## @end group
## @end example
##
## but the point x0 for y0 didn't exist.  If the homogeneous boundary
## condition had the form
##
## @example
## @group
## [b0, b1, b2] * [y0;
##                 y1;
##                 y2] = 0 
## @end group
## @end example
##
## then y0 could be expressed as
##
## @example
## @group
## y0 = -[b1, b2] * [y1;
##                   y2] / b0
## @end group
## @end example
##
## so the originally intended field operation can be applied as
##
## @example
## @group
## ([k1, k2] - k0*[b1, b2]/b0)* [y1;
##                               y2]
## @end group
## @end example
##
## which is what we end up using.
##
## Similarly for the interfacial condition, if on the left and right
## we wish to impose
##
## @example
## @group
## kl(-2:0) * y(-2:0)
## kr(0:2) * y(0:2)
## @end group
## @end example
##
## but we don't have access to y(0), although we know it's
## single-valued, corresponding to the continuity of value across the
## interface, then we form the continuity of flux condition:
##
## @example
## p * hl(-2:0) * y(-2:0) = q * hr(0:2) * y(0:2)
## @end example
##
## from which we extract the expression for the interfacial temperature, 
##
## @example
## @group
## y(0) = p * hl(-2:-1)/h * y(-2:-1) - q * hr(1:2)/h * y(1:2) 
##      
##      = [p * hl(-2:-1), q * hr(1:2)]/h * y([-2:-1, 1:2])
## @end group
## @end example
##
## where 
##
## @example
## h = q * hr(0) - p * hl(0)
## @end example
##
## Thus we express the second-derivative at the points adjacent the
## interface with stencils including two points either side.
## @seealso{finidiff}
## @end deftypefn

## Author: G. D. McBain <geordie.mcbain@aeromech.usyd.edu.au>
## Created: 2006-11-29
## Keywords: finite difference

function D = finidiff_matrix2_mod (D, x, s, p, q)
  
  n = length (x);

  switch (i = sum (x<s))		# x(i:i+1) crosses interface

    case 0			# left boundary
      
      newstencil = 1:2;		
      oldstencil = [s; x(newstencil)(:)]; 
      b = finidiff (oldstencil, s, p); # p=0/1, Dirichlet/Neumann
      k = finidiff (oldstencil, x(1), 2);
      D(1,newstencil) = k(2:3) - k(1) * b(2:3) / b(1);

    case num2cell (1:n-1)	# internal interface

      oldstencill = [x((i-1):i); s]; # left peripheral
      oldstencilr = [s; x(i+(1:2))]; # right peripheral
      hl = p * finidiff (oldstencill, s, 1); # left flux
      hr = q * finidiff (oldstencilr, s, 1); # right flux
      h = hr(1) - hl(3);
      newstencil = i+(-1:2);	# two points either side
      kl = finidiff (oldstencill, x(i), 2);
      kr = finidiff (oldstencilr, x(i+1), 2);
      h0 = horzcat (hl(1:2), -hr(2:3)) / h;
      D(i,newstencil) = postpad (kl(1:2), 4) + kl(3)*h0;
      D(i+1,newstencil) = prepad (kr(2:3), 4) + kr(1)*h0;

    case n			# right boundary

      newstencil = (n-1):n;
      oldstencil = [x(newstencil); s];	
      b = finidiff (oldstencil, s, p); # p=0/1, Dirichlet/Neumann
      k = finidiff (oldstencil, x(n), 2);
      D(n,newstencil) = k(1:2) - k(3) * b(1:2)/b(3);

  endswitch

endfunction

%!test
%!
%! ## steady conduction of heat in a composite slab with uniform 
%! ## volumetric generation and cold sides
%!
%! ## (k u')' = -1, u (+/- 1) = 0,
%!
%! ## k = 0.1 (x<0); 10 (x>0).
%!
%! kl = 0.1;
%! kr = 10;
%!
%! n = 32;
%! x = sort (2 * rand (n, 1) - 1);
%!
%! D = finidiff_matrix (x, 2);
%! D = finidiff_matrix2_mod (D, x, -1, 0); # left boundary condition
%! D = finidiff_matrix2_mod (D, x, 1, 0); # right boundary condition
%! ## equiv.: D = finidiff_matrix_dirichlet (x, 2, -1, 1)
%! D = finidiff_matrix2_mod (D, x, 0, kl, kr);	# interfacial condition
%!
%! k = kl * ones (size (x));	     # thermal conductivity...
%! k(x>0) = kr;			     # ...has a jump
%! u = D \ (-1./k);		     # solve linear system
%!
%! u_exact = 1/2/kl * ((1-x.^2) + (kl-kr)/(kl+kr)*(1+x));
%! u_exact(x>0) = 1/2/kr * ((1-x.^2) + (kr-kl)/(kr+kl)*(1-x)) (x>0);
%! assert (u, u_exact, 1e-12);

%!demo
%!
%! ## steady conduction of heat in a composite slab with uniform 
%! ## volumetric generation and cold sides
%!
%! ## (k u')' = -1, u (+/- 1) = 0,
%!
%! ## k = 0.1 (x<0); 10 (x>0).
%!
%! kl = 0.1;
%! kr = 10;
%!
%! n = 32;
%! x = sort (2 * rand (n, 1) - 1);
%!
%! D = finidiff_matrix (x, 2);
%! D = finidiff_matrix2_mod (D, x, -1, 0); # left boundary condition
%! D = finidiff_matrix2_mod (D, x, 1, 0); # right boundary condition
%! ## equiv.: D = finidiff_matrix_dirichlet (x, 2, -1, 1)
%! D = finidiff_matrix2_mod (D, x, 0, kl, kr);	# interfacial condition
%!
%! k = kl * ones (size (x));	     # thermal conductivity...
%! k(x>0) = kr;			     # ...has a jump
%! u = D \ (-1./k);		     # solve linear system
%!
%! x1 = linspace (-1, 1, 10*n); # for exact solution
%! y1 = 1/2/kl * ((1-x1.^2) + (kl-kr)/(kl+kr)*(1+x1));
%! y1(x1>0) = 1/2/kr * ((1-x1.^2) + (kr-kl)/(kr+kl)*(1-x1)) (x1>0);
%! plot (x, u, "*;numerical;", x1, y1, "-;exact;")
