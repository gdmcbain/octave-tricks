## Copyright (C) 2007 G. D. McBain <gdmcbain@protonmail.com>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or (at
## your option) any later version.
##
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301USA

## -*- texinfo -*-
## @deftypefn {Function File} {} finidiff_matrix (@var{x}, @var{q})
## Construct a sparse centred finite difference matrix for the @var{q}th
## derivative on the abscissae @var{x}, omitting the rows for the
## derivatives at peripheral points---i.e. those with stencils requiring
## points outside the @var{x}. The stencils reach @code{ceil
## (@var{q}/2)} on either side of the evaluation point.
## @seealso{finidiff}
## @end deftypefn

## ### IMPLEMENTATION NOTES
##
## ### ## Vectorization?
##
## Is the for loop really necessary?  For example, ri is a Kronecker
## product and could be computed with
##
##   ri = kron (((1 + r) : (n - r))', ones (nzr, 1));
##
## outside the for loop.  We probably do need to loop for the calls to
## finidiff though unless that can be vectorized, which I doubt.
##
## ### ## Extension to plane and spatial problems
##
## The current implementation (2007-05-22 and as 2007-05-24) is only for
## problems on the line.  For problems in the plane (or in space, with
## obvious modifications), does it make sense, conceptually, for
## functions to be represented by two-dimensional arrays rather than
## column-vectors and for linear operators to be represented by
## four-dimensional arrays; should the (i,j,k,l)-th coefficient
## represent the influence of the (k,l)-th point on the (i,j)-th point,
## just as for problems on the line the (i,j)-th coefficient represents
## the influence of the j-th point on the i-th?
##
## Of course, these higher-dimensional constructed would normally be
## reshaped into columns and squares for computation, but does this
## suggest an approach to assembly?  Perhaps using repmat, reshape,
## etc.?

## Author: G. D. McBain <geordie.mcbain@aeromech.usyd.edu.au>
## Created: 2007-03-16
## Keywords: finite difference

function [D, r] = finidiff_matrix (x, q)

  if (nargin != 2)
    print_usage (); 
  endif

  n = length (x);
  r = ceil (q/2);		# the stencil's reach

  nzr = 2 * r + 1;		# entries per row
  nz = (n - 2 * r) * nzr;	# total number of entries
  ri = ci = d = zeros (nz, 1);

  for i = (1 + r) : (n - r)	# interior points
    entries = (i - (1 + r)) * nzr + (1:nzr);
    ri(entries) = i * ones (nzr, 1);
    ci(entries) = (i - r) : (i + r);
    d(entries) = finidiff (x(ci(entries)), x(i), q);
  endfor

  D = sparse (ri, ci, d, n, n);

endfunction

%!demo
%!
%! ## first to fourth-order on an even grid of seven points
%!
%! for q = 1:6
%!   printf ("\nq = %d\n\n", q);
%!   D = finidiff_matrix (-3:3, q);
%!   if (q/2 == ceil (q/2))
%!     printf (" % -5.0f% -5.0f% -5.0f% -5.0f% -5.0f% -5.0f% -5.0f\n", D.');
%!   else
%!     printf ("% 5.1f% 5.1f% 5.1f% 5.1f% 5.1f% 5.1f% 5.1f\n", D.');
%!   endif
%! endfor
