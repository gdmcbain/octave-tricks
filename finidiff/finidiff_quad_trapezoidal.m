## Copyright (C) 2007 G. D. McBain <gdmcbain@protonmail.com>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or (at
## your option) any later version.
##
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301USA

## -*- texinfo -*-
## @deftypefn {Function File} {} finidiff_quad_trapezoidal (@var{x}, @var{xl}, @var{xr})
## Compute the trapezoidal rule quadrature weights for the abscissae
## @var{x} over the domain from @var{xl} to @var{xr} for functions
## supposed to vanish at the ends.  The quadrature error is of order
## @code{length (x)^2}.
## @end deftypefn

## ### BACKGROUND THEORY
##
## Erect a tent on each abscissa; its area is the sum of two triangles:
## 0.5*y(i)*(x(i)-x(i-1)) + 0.5*y(i)*(x(i+1)-x(i)); hence the quadrature
## weight is 0.5*(x(i+1)-x(i-1)).
##
## This formula fails for the two outermost points.  Here we add
## supplementary exterior points at which the function is supposed to
## vanish. Any contribution to the integral from nonvanishing terminal
## values must be added separately.

## ### REFERENCES
##
## P. J. Davis & P. Rabinowitz 1967 Numerical Integration.  Blaisdell,
## Waltham, Massachusetts.

## Author: G. D. McBain <geordie.mcbain@aeromech.usyd.edu.au>
## Created: 2006-03-13
## Keywords: quadrature (functions of one variable)

function w = finidiff_quad_trapezoidal (x, xL, xR)
  X = [xL; x(:); xR];		# augment with termini
  w = 0.5 * (X(3:end)-X(1:end-2))';
endfunction

%!test
%!
%! ## integrate constants
%!
%! n = 99;
%! x = sort (rand (n, 1));
%! w = finidiff_quad_trapezoidal (x, 0, 1);
%! assert (sum (w), 1, 5e-2)

%!test
%!
%! ## integrate linear functions
%!
%! n = 99;
%! x = sort (rand (n, 1));
%! w = finidiff_quad_trapezoidal (x, 0, 1);
%! assert (w * x, 1/2, 5e-2)

%!test
%!
%! ## integrate quadratic functions
%!
%! n = 99;
%! x = sort (rand (n, 1));
%! w = finidiff_quad_trapezoidal (x, 0, 1);
%! assert (w * x.^2, 1/3, 5e-2)

%!test
%!
%! ## sin (pi*x) over (0,1) (Davis & Rabinowitz 1967, p. 17)
%!
%! x = linspace (0, 1, 99)';
%! w = finidiff_quad_trapezoidal (x, 0, 1);
%! assert (w * sin (pi*x), 2/pi, 1e-4)

%!demo
%!
%! ## sin (pi*x) over (0,1) (Davis & Rabinowitz 1967, p. 17)
%!
%! printf ("%4s\t%10s\t%s\n\n", "n", "integral", "error");
%! for n = 2.^(1:12)
%!   x = linspace (0, 1, n+1) (2:n);
%!   q = finidiff_quad_trapezoidal (x, 0, 1) * sin (pi*x');
%!   printf ("%4d\t%10.8f\t%e\n", n, q, 2/pi-q);
%! endfor

%!demo
%!
%! ## x.*(1-x) over (0,1)
%!
%! printf ("%4s\t%10s\t%s\n\n", "n", "integral", "error");
%! for n = 2.^(1:12)
%!   x = (linspace (0, 1, n+1) (2:n))';
%!   y = x .* (1-x);
%!   w = finidiff_quad_trapezoidal (x, 0, 1);
%!   q = w*y;
%!   printf ("%4d\t%10.8f\t%e\n", n, q, 1/6-q);
%! endfor
