## Copyright (C) 2007 G. D. McBain <gdmcbain@protonmail.com>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

## -*- texinfo -*-
## @deftypefn {Function File} {} grid_cellcentred_n (@var{h}, @var{r}, @var{x})
## Compute how many cells are required if the last is to be centred on
## @var{x}, if the first cell begins at zero and has width @var{h} and
## the widths have common ratio @var{r}.  The answer is not an integer.
## @seealso{grid_cellcentred}
## @end deftypefn

## Author: G. D. McBain <geordie.mcbain@aeromech.usyd.edu.au>
## Created: 2007-02-12
## Keywords: grid

function n = grid_cellcentred_n (h, r, x)
  if (r == 1)
    n = 0.5 + x / h;
  else
    n = 1 + log (2/(1+r)*(1-(1-r)*x/h)) / log (r);
  endif
endfunction

%!demo
%!
%! ## For h = 0.08 and r = 1+5h/4, need 47 to get to 66.551.
%! 
%! h = 0.08
%! r = 1 + 5*h/4
%! grid_cellcentred_n (h, r, 66.551)

%!demo
%!
%! ## For h = 0.16 and r = 1+5h/4, how many to get to x = 17? 32?
%!
%! h = 0.16;
%! r = 1 + 1.25 * h
%! grid_cellcentred_n (h, r, [17, 32])

%!demo
%!
%! ## For h = 0.16 and r = 1+p h, how many to get to x = 32?
%!
%! xinf = 32;
%! h = 0.16;
%! n = grid_cellcentred_n (h, 1-1e-4, xinf)
%! n = grid_cellcentred_n (h, 1, xinf)
%! n = grid_cellcentred_n (h, 1+1e-4, xinf)
