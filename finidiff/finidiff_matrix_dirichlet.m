## Copyright (C) 2007 G. D. McBain <gdmcbain@protonmail.com>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or (at
## your option) any later version.
##
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301USA

## -*- texinfo -*-
## @deftypefn {Function File} {} finidiff_matrix_dirichlet (@var{x}, @var{q}, @var{xl}, @var{xr})
## Construct a sparse centred finite difference matrix for the @var{q}th
## derivative on the abscissae @var{x} interior to the segment from
## @var{xl} to @var{xr}, assuming zeros of sufficiently high order at
## the boundaries; e.g. simple zeros if @code{@var{q}=2} and double
## zeros if @code{@var{q}=4}.  The reach of each stencil is the least
## integer at least half the order @var{q}, and the length of @var{x}
## must be at least twice this reach.
##
## The matrix acts on a column vector of ordinates to produce a column
## vector approximating the derivative of order @var{q} at the
## abscissae. For peripheral points (those with stencils reaching
## outside @var{x}), the derivative is computed using the available
## points supplemented with one order of zero at the relevant terminal
## point for each missing point.
## @seealso{finidiff, finidiff_matrix}
## @end deftypefn

## ### BACKGROUND THEORY
##
## (As in finidiff, r is the `reach' of the stencil from the
## evaluation point; i.e. an interior stencil reaches r points on
## either side.)
##
## On the left, we affect the first r rows, adding i+r entries to the
## i-th. Thus on left and right we add 2 * sum ((1:r)+r) = r*(3*r+1).
##
## ### ## The row indices
##
## Of these, the first r+1 is in row 1, the next r+2 in row 2, the
## next r+3 in row 3, and so on up to r.  To obtain these row indices,
## form the matrix with r identical rows of [1, 2, ..., r] and stack it
## above its upper triangular part, e.g. for r = 3

##   1   2   3
##   1   2   3
##   1   2   3
##   1   2   3
##   0   2   3
##   0   0   3

## then take the nonzero elements columnwise.  Then append the same
## thing for the right end.
##
## ### ## The column indices
##
## On the left, the entries are added in columns 1:r+1, then columns
## 1:r+2, then columns 1:r+3, up to columns 1:2*r.  This is formed by
## taking columnwise (for r=3, for example)

##   1   1   1
##   2   2   2
##   3   3   3
##   4   4   4
##   .   5   5
##   .   .   6

## ### ## The entries
##
## The entries are generated row-wise with calls to finidiff.  Where
## to add them in the data vector for sparse is governed by a counter,
## ii.

## Author: G. D. McBain <geordie.mcbain@aeromech.usyd.edu.au>
## Created: 2007-03-16
## Keywords: finite difference

function D = finidiff_matrix_dirichlet (x, q, xl, xr)

  if (nargin ~= 4)
    print_usage ();
  endif

  n = length (x);
  [D, r] = finidiff_matrix (x, q);

  if (2*r > n)
    error (strcat ("Can't compute order %d derivative ", 
		   "with a boundary condition on %d points."),
	   q, n );
  endif

  ri = ci = d = zeros (nz = r*(3*r+1), 1);

  ril = ones (r, 1) * (1:r);
  ril = [ril; (triu (ril))];
  mask = ril > 0;
  ril = ril(mask);
  ri = [ril; ((n+1) - ril)];

  cil = (1:2*r)' * ones (1, r);
  cil = cil(mask);
  ci = [cil; ((n+1) - cil)];

  ii = 0;
  for i = 1:r			# rows for left-peripheral points
    star = ii+1:(ii += i+r);
    d(star) = finidiff (x(1:i+r), x(i), q, b=1+r-i, xl);

    j = n+1-i;			# mirror right-peripheral row
    d(nz/2 + star) = fliplr (finidiff (x(j-r:n), x(j), q, b, xr));
  endfor

  D += sparse (ri, ci, d);
  
endfunction

%!demo
%!
%! ## first to fourth-order on an even grid of seven points
%!
%! for q = 1:4
%!   printf ("\nq = %d\n\n", q);
%!   D = finidiff_matrix_dirichlet (-3:3, q, -4, 4);
%!   if (q/2 == ceil (q/2))
%!     printf (" % -5.0f% -5.0f% -5.0f% -5.0f% -5.0f% -5.0f% -5.0f\n", D.');
%!   else
%!     printf ("% 5.1f% 5.1f% 5.1f% 5.1f% 5.1f% 5.1f% 5.1f\n", D.');
%!   endif
%!   cond (D)
%! endfor

%!demo
%!
%! ## the odd orders are singular
%!
%! printf ("%5s\t%s\n\n", "order", "condition");
%! for q = 1:6
%!   D = finidiff_matrix_dirichlet (-3:3, q, -4, 4);
%!   printf ("%1d\t%e\n", q, cond (D));
%! endfor

%!demo 
%! 
%! ## u'' = -2, u (0) = u (1) = 0, which has the solution x*(1-x).
%!
%! x0 = linspace (0, 1, 100);
%! y0 = x0 .* (1-x0);
%! n = 64;
%! x = sort (rand (n, 1));
%! D2 = finidiff_matrix_dirichlet (x, 2, 0, 1);
%! y = D2 \ (-2 * ones (size (x)));
%! plot (x, y, "*;numerical;", x0, y0, "-;exact;");

%!demo
%!
%! ##  u'''' = 24, u(0)=u'(0)=u(1)=u'(1)=0, for which u=(x*(1-x))^2.
%!
%! x1 = linspace (0, 1, 100);
%! y1 = (x1 .* (1-x1)).^2;
%! n = 64;
%! x = sort (rand (n, 1));
%! D4 = finidiff_matrix_dirichlet (x, 4, 0, 1);
%! y = D4 \ (24 * ones (size (x)));
%! plot (x, y, "*;numerical;", x1, y1, "-;exact;");

%!demo
%!
%! ## A vector 2pt BVP, the base solution of Gill & Davey (1969).
%! 
%! ##   V'' + 2 T = T'' - 2 V = 0, 
%!
%! ##   V (0) = 0, T (0) = 1
%! 
%! ##   V (Inf) = T (Inf) ~ 0
%!
%! ## Render the thermal boundary condition homogeneous 
%! ## by putting T = U + exp (-x), then the problem is
%! ## V'' + 2 U = - 2 exp (-x), U'' - 2 V = - exp (-x) 
%! ## with homogeneous Dirichlet conditions at the wall.
%! x = (linspace (0, xinf = 6, (n=16)+2) ((1:n)+1)) ';
%! D2 = finidiff_matrix_dirichlet (x, 2, 0, xinf);
%! soln = [D2, (2 * eye (n)); -(2 * eye (n)), D2] \ kron (-[2; 1], exp (-x));
%! V = soln(1:n);
%! Theta = soln(n+1:2*n) + exp (-x);
%! xx = linspace (0, xinf, 10*n);
%! Thetax = exp (-xx) .* cos (xx);
%! Vx = exp (-xx) .* sin (xx);
%! plot (x, Theta, "*;Theta, numerical;r", xx, Thetax, "-;Theta, exact;r", x, V, "+;V, numerical;g", xx, Vx, "-;V, exact;g");

%!demo
%! 
%! ## cpu time for demo #1
%!
%! printf ("%4s\t%s\n\n", "n", "cpu time (s)");
%! repeats = 1e2;
%! for n = 2.^(9:13)
%!   x = (linspace (0, 1, n+2) (2:n+1))';
%!   b = -2 * ones (n, 1);
%!   D2 = finidiff_matrix_dirichlet (x, 2, 0, 1);
%!   t0 = cputime ();
%!   for i=1:repeats, y = D2 \ b; endfor
%!   ts = (cputime () - t0) / repeats;
%!   printf ("%4d\t%e\n", n, ts);
%! endfor
