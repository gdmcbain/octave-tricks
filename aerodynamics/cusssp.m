## Copyright (C) 2006, 2007, 2018 G. D. McBain -*- octave -*-
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

## -*- texinfo -*-
## @deftypefn {Function File} {} [@var{z}, @var{omega}, @var{Q}] = cusssp (@var{zp}, @var{alpha})
## Find the plane ideal flow over a wing section with profile, defined
## anticlockwise by the complex points @var{zp}, at incidence
## @var{alpha} (radians) to a uniform stream.  For their midpoints
## @var{z}, return the complex influence matrix @var{omega} for the
## complex uniform source panels and their strengths @var{Q}.  The
## method is essentially that of Hess & Smith (1967): the imaginary
## parts of the strengths are taken to be the same and determined by the
## Kutta--Joukowsky condition (that the tangential components of
## velocity agree either side of the trailing edge), while the different
## constant real parts are determined by enforcing impermeability of the
## panels; see also Anderson (2001, pp. 319--325) and Moran (2003, pp.
## 103--112).
##
## The complex velocity at the midpoints @var{z} can be computed by
## @code{exp (-1i*alpha) + omega * Q}. @seealso{cusssp_influence} 
## @end deftypefn

## ### REFERENCES
##
## J. D. Anderson, Jr. 2001 Fundamentals of Aerodynamics, 3rd edition.
## McGraw-Hill, Boston.
##
## J. L. Hess & A. M. O. Smith 1967 Calculation of potential flow about
## arbitrary bodies.  Progress in Aerospace Science 8:1--138.
##
## J. Moran 2003 An Introduction to Theoretical and Computational
## Aerodynamics. Dover, New York.

## Author: G. D. McBain <geordie.mcbain@aeromech.usyd.edu.au>
## Created: 2006-08-21
## Keywords: aerodynamics, panel method

function [z, omega, Q] = cusssp (zp, alpha)

  dz = diff (zp);		# panel segments, 1*n
  n = length (dz);		# no. panels
  lambda = arg (dz)';		# panel angles, 1*n
  z = (zp(1:n) + 0.5*dz).';	# panel midpoints, n*1

  omega = cusssp_influence (z, zp(1:n), dz); # n*n

  diagonal = logical (eye (n)); # correct diagonals
  omega(diagonal) = abs (omega(diagonal)) .* exp (-1i*(lambda-pi/2));

  A = diag (exp (1i*lambda)) * omega; # rotate to tangential--normal
  k = A(1,:) + A(n,:);		# for Kutta condition

  B = [(-imag (A)), (-sum (imag (1i*A), 2));
       (real (k)), (sum (real (1i*k)))];

  rhs = [(sin (lambda - alpha));
	 -(cos (lambda(1) - alpha) + cos (lambda(n) - alpha))];

  Q = B \ rhs;  
  Q = Q(1:n) + 1i*Q(n+1);

endfunction

%!demo# NACA 0006 Abbott & von Doenhoff (1959, p. 311)
%! zp = naca4z1 (64, 0, 0.4, 0.06);
%! [z, omega, Q] = cusssp (zp, 0);
%! w = 1 + omega * Q;
%! x = [0, 0.5, 1.25, 2.5:2.5:10, 15:5:30, 40:10:90, 95, 100]/100;
%! vV2 = [0, 0.880, 1.117, 1.186, 1.217, 1.225, 1.212, 1.206, 1.190, 1.179, 1.162, 1.136, 1.109, 1.086, 1.057, 1.026, 0.980, 0.949, 0];
%! title ("NACA 0006 (Abbott & von Doenhoff 1959, p. 311)")
%! grid ("on"), axis ([0, 1, 0, 2], "normal"); 
%! xlabel ("x/c"); ylabel ("(v/V)^2")
%! plot (real (z), 0.2+imag (z), "-;;k", x, vV2, "+;Abbott & von Doenhoff (1959);b", real (z), abs (w).^2, "*;present method;r")

%!demo# NACA 2412 at 8 deg.: draw the velocity at the midpoints
%! zp = naca4z1 (32, 0.02, 0.4, 0.12);
%! [z, omega, Q] = cusssp (zp, alpha = (alphad = 8) * pi/180);
%! w = exp (-1i*alpha) + omega * Q;
%! [ax,ay] = qquiver (x=real (z), y=imag (z), real (w), -imag (w), 0.2);
%! ylabel ("y/c"); axis ("image");
%! title (sprintf ("NACA 2412 at alpha = %3.1f deg.; surface velocity", alphad))
%! plot (x, y, "*;;g", ax, ay, "-;;r", real (zp), imag (zp), "-;;k");

%!demo# NACA 2412, at 8 deg.: draw the velocity field
%! zp = naca4z1 (256, 0.02, 0.4, 0.12);
%! [z, omega, Q] = cusssp (zp, alpha=(alphad=8)*pi/180);
%! X = linspace (-0.5, 1.5, 40); Y = linspace (-0.3, 0.3, 20);
%! [X, Y] = meshgrid (X, Y); ZP=(X+1i*Y)(:);
%! W = exp(-1i*alpha) + cusssp_influence (ZP, zp(1:(end-1)), diff (zp)) * Q;
%! [ax, ay] = qquiver (real (ZP), imag (ZP), real (W), -imag (W), 0.03);
%! title (sprintf ("NACA 2412 at alpha = %g deg.\n", alpha*180/pi));
%! xlabel (""); ylabel (""); axis ("image")
%! plot (X, Y, "*;;g", ax, ay, "-;;r", real (zp), imag (zp), "-;;k");

%!demo# NACA 2412: lift v. incidence
%! zp = naca4z1 (256, 0.02, 0.4, 0.12);
%! alpha0d = -2.077;
%! alphad = -10:10; Cl = Cl_thin = zeros (size (alphad));
%! for i = 1:(length (alphad));
%!   [z, omega, Q] = cusssp (zp, alpha=alphad(i)*pi/180);
%!   Cl(i) = 2 * sum (imag (Q));
%!   printf ("%- 4d  %- 7.4f\n", alphad(i), Cl(i));
%! endfor
%! axis ([-32, 32, -2, 3.6], "normal");
%! grid ("on"), grid ("minor", "on")
%! title ("NACA 2412 wing section (Abbott & von Doenhoff 1959, p. 478)");
%! xlabel ("Section angle of attack, alpha (deg.)");
%! ylabel ("Section lift coefficient, Cl");
%! Cl_thin = 2*pi*(alphad - alpha0d)*pi/180;
%! plot (alphad, Cl, "*-;;", alphad, Cl_thin, "-;thin;");
