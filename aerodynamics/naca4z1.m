## Copyright (C) 2006, 2007 G. D. McBain <geordie_mcbain@yahoo.com.au>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

## -*- texinfo -*-
##
## @deftypefn {Function File} {} @var{z} = naca4z1 (@var{n}, @var{m}, @var{p}, @var{t})
##
## Return a vector of complex points describing a NACA four-digit wing
## section, anticlockwise from the trailing edge.
##
## @seealso{naca4meanline, naca4thickness}
##
## @end deftypefn

## ### BACKGROUND
##
## ### ## Complexifying the formula
##
## We have (Jacobs, Ward, & Pinkerton 1933; Abbott & von Doenhoff 1959,
## p. 113)
##
##   xu = x - yt * sin (lambda)
##
##   yu = yc + yt * cos (lambda)
##
##   xl = x + yt * sin (lambda)
##
##   yl = yc - yt * cos (lambda)
##
## so if zc=x+1i*yc then zu = xu+1i*yu = zc + 1i * yt * exp (1i*lambda)
##
## and zl = xl+1i*yl = zc - 1i * yt * exp (1i*lambda).  Generally, z =
## zc + upper * 1i * yt * exp (1i*lambda), where upper is +1 on the
## upper surface and -1 on the lower and can be computed as upper = 2 *
## sign (chi) - 1 where chi = acos (2*x/c-1) is the eccentric angle.

## ### REFERENCES
##
## I. H. Abbott & A. E. von Doenhoff 1959 Theory of Wing Sections,
## Dover.
##
## E. N. Jacobs, K. E. Ward, & R. M. Pinkerton 1933 The characteristics
## of 78 related airfoil sections from tests in the variable-density
## wind tunnel.  NACA Report No. 460.

## Author: G. D. McBain <geordie.mcbain@aeromech.usyd.edu.au>
## Created: 2006-08-25
## Keywords: aerodynamics, wing sections

function z = naca4z1 (n, m, p, t)

  chi = linspace (0, 2*pi, n); # eccentric angle
  x = 0.5 * (1 + cos (chi));	# abscissae on chord
  xte = fsolve (@naca4thickness, 1.0); # trailing edge
  nte = max (2, ceil ((xte-x(1))/(x(1)-x(2)))); # no. additional points needed
  xx = linspace (xte, x(1), nte);
  x = [xx, x(2:(end-1)), (fliplr (xx))];
  chi = [(zeros (1, nte-1)), chi, (2*pi*ones (1, nte-1))];

  [yc, tanlambda] = naca4mean (x, m, p);
  zc = x + 1i*yc;
  z = zc + (2 * (chi < pi) - 1) * 1i * t .* naca4thickness (x) .* exp (1i * atan (tanlambda));

endfunction

%!demo# NACA 0012
%! z = naca4z1 (32, 0, 0.0, 0.12);
%! title ("NACA 0012"); xlabel ("x/c"); ylabel ("y/c");
%! plot (real (z), imag (z), "*-;;b")

%!demo# NACA 2412
%! z = naca4z1 (360, 0.02, 0.4, 0.12);
%! title ("NACA 2412"); xlabel ("x/c"); ylabel ("y/c");
%! plot (real (z), imag (z), "*-;;b");

%!demo# NACA 4412
%! z = naca4z1 (360, 0.04, 0.4, 0.12);
%! title ("NACA 4412"); xlabel ("x/c"); ylabel ("y/c");
%! plot (real (z), imag (z), "*-;;b");
