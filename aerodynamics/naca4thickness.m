## Copyright (C) 2006, 2007 G. D. McBain <geordie_mcbain@yahoo.com.au>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA


## -*- texinfo -*-
##
## @deftypefn {Function File} {} naca4thickness (@var{x})
##
## Compute the thickness at abscissae @var{x} of the NACA 4-series wing
## section. All lengths expressed in fractions of the chord.
##
## @end deftypefn

## ### BACKGROUND
##
## The formula is taken from Jacobs, Ward, & Pinkerton (1933, p. 300);
## see also Abbott & von Doenhoff (1959, p. 113).

## ### REFERENCES
##
## I. H. Abbott & A. E. von Doenhoff 1959 Theory of Wing Sections,
## Dover.
##
## E. N. Jacobs, K. E. Ward, & R. M. Pinkerton 1933 The characteristics
## of 78 related airfoil sections from tests in the variable-density
## wind tunnel.  NACA Report No. 460.

## Author: G. D. McBain <geordie.mcbain@aeromech.usyd.edu.au>
## Created: 2006-08-14
## Keywords: aerodynamics, wing sections

function y = naca4thickness (x)
  y = 1/0.20 * (0.29690 * sqrt (x) - 0.12600*x - 0.35160*x.^2
		+ 0.28430*x.^3 - 0.10150*x.^4 );
endfunction

%!demo# Jacobs et al. (1933, figure 1, p. 300)
%! x = [0, 1.25, 2.5:2.5:10, 15:5:30, 40:10:90, 95, 100]/100;
%! y = 0.20 * naca4thickness (x); 
%! [x; y]*100
