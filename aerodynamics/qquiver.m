## Copyright (C) 2006, 2007 G. D. McBain <geordie_mcbain@yahoo.com.au>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

## -*- texinfo -*- 
##
## @deftypefn {Function File} {} [@var{arrowx}, @var{arrowy}] = qquiver (@var{x}, @var{y}, @var{u}, @var{v}, @var{dt})
##
## Plot the vector field with components @code{@var{u}*@var{dt}} and
## @code{@var{v}*@var{dt}} at @var{x} and @var{y}.  Also return the data
## for the arrows.
##
## @end deftypefn

## Author: G. D. McBain <geordie.mcbain@aeromech.usyd.edu.au>
## Created: 2006-08-10
## Keywords: plotting, vector fields, direction, arrow, quiver

function [arrowx, arrowy] = qquiver (x, y, u, v, dt)
  n = length (x(:));
  x1 = x + u*dt;
  y1 = y + v*dt;
  arrowx = reshape ([x(:), x1(:), NaN(n,1)]', [3*n, 1]);
  arrowy = reshape ([y(:), y1(:), NaN(n,1)]', [3*n, 1]);
  plot (x, y, "*;;g", arrowx, arrowy, "-;;r");
endfunction

%!demo
%! x = y = linspace (-2, 2, 40);
%! [X, Y] = meshgrid (x, y);
%!  u = Y./(X.^2+Y.^2);
%! v = -X./(X.^2+Y.^2);
%! qquiver (X, Y, u, v, 0.2)
