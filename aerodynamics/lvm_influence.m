## Copyright (C) 2006, 2007 G. D. McBain <geordie_mcbain@yahoo.com.au>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

## -*- texinfo -*-
## @deftypefn {Function File} {} @var{w} = lvm_influence (@var{z}, @var{zeta})
##
## Compute the matrix @var{w} of complex velocities induced at the
## column of complex points @var{z} by unit point sources at the row of
## complex points @var{zeta}.  The matrix @var{w} acts on a column of
## complex source strengths to produce the complex velocities at the
## points.
## @end deftypefn

## Author: G. D. McBain <geordie.mcbain@aeromech.usyd.edu.au>
## Created: 2006-08-30
## Keywords: aerodynamics

function w = lvm_influence (z, zeta)
  w = 1.0 ./ (2*pi*(repmat (z(:), [1, (length (zeta))])
		    - repmat (zeta, [(length (z)), 1]) ));
endfunction

%!demo#: source at origin
%! wf = inline ("reshape (lvm_influence (z(:), 0), size (z))");
%! title ("Plane ideal unit source at origin"); axis ("image")
%! qquiver_w (-2-2i, 2+2i, 40, wf, 0.2);

%!demo#: vortex at origin
%! wf = inline ("1i * reshape (lvm_influence (z(:), 0), size (z))");
%! title ("Plane ideal unit vortex at origin"); axis ("image")
%! qquiver_w (-2-2i, 2+2i, 40, wf, 0.2);
