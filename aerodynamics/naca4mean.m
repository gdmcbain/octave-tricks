## Copyright (C) 2006, 2007 G. D. McBain <geordie_mcbain@yahoo.com.au>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

## -*- texinfo -*-
##
## @deftypefn {Function file} {} [@var{y}, @var{theta}] naca4mean (@var{x}, @var{m}, @var{p})
##
## Compute the ordinate and slope at chord-fractional abscissae @var{x}
## of the NACA 4-series wing section of maximum camber @var{m} at
## chord-fraction @var{p}.
##
## @end deftypefn

## Author: G. D. McBain <geordie.mcbain@aeromech.usyd.edu.au>
## Created: 2006-08-23
## Keywords: aerodynamics, wing sections

function [y, tantheta] = naca4mean (x, m, p)
  if m==0			# symmetric
    tantheta = y = zeros (size (x));
  else				# cambered
    aft = x>p;			# mask for aft portion
    y = m/p^2 * (2*p*x - x .^2);	# camber line
    tantheta = 2*m/p^2 * (p-x);	# camber line slope
    y(aft) = m/(1-p)^2 * (1-2*p + 2*p*x(aft) - x(aft).^2);
    tantheta(aft) *= (p/(1-p))^2;	# correct aft portion
  endif
endfunction
