## Copyright (C) 2006, 2007 G. D. McBain <gdmcbain@protonmail.com>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

## -*- texinfo -*-
## @deftypefn {Function File} {} [@var{L}, @var{M}] = gilldavey (@var{Re}, @var{Pr}, @var{k}, @var{D}, @var{D2}, @var{D4}, @var{D2T}, @var{V}, @var{Vpp}, @var{Tp})
## Assemble the stiffness @var{L} and mass @var{M} matrices for the
## equations governing the linear stability of two-dimensional
## disturbances to a buoyancy layer in a stratified fluid, with vertical
## velocity profile @var{V} and second derivative @var{Vpp} and
## horizontal temperature gradient @var{Tp}; parameterized by the
## Reynolds @var{Re} and Prandtl @var{Pr} numbers, and the vertical
## wavenumber @var{k}; see Gill & Davey (1969).  The caller provides the
## first @var{D} and second @var{D2} and fourth @var{D4} differentiation
## matrices for the velocity and second differentiation matrix @var{D2T}
## for the temperature.  
##
## The equations are (Gill & Davey 1969; McBain, Armfield, & Desrayaud,
## in press)
##
## @example
## @group
## (D^2-k^2)^2 f - i k Re ((V-c)(D^2-k^2) f - V'' f) + 2 D q = 0
##
## (D^2-k^2) q - i k Re Pr ((V-c) q - T' f) - 2 D f = 0
## @end group
## @end example
##
## A. E. Gill & A. Davey 1969 Instabilities of a buoyancy driven
## system. @cite{Journal of Fluid Mechanics} @strong{35}:775--798.
##
## G. D. McBain, S. W. Armfield, & G. Desrayaud (2007) Instability
## of the buoyancy layer on an evenly heated vertical wall.
## @cite{Journal of Fluid Mechanics}.
## @end deftypefn

## Author: G. D. McBain <gdmcbain@protonmail.com>
## Created: 2006-04-10
## Keywords: hydrodynamic stability

function [L, M] = gilldavey (Re, Pr, k, D1, D2, D4, D2T, V, Vpp, Tp)

  I = eye (size (D2));
  Z = zeros (size (D2));

  Lv = [k^4*I - 2*k^2*D2 + D4, 2*D1;
	2*D1,                  k^2*I-D2T];
  
  Li = [(diag (V) * (k^2*I-D2) + diag (Vpp)), Z;
	(diag (-Pr*Tp)),                    (diag (Pr*V))];

  L = Lv / (1i*k*Re) + Li;

  M = [k^2*I-D2, Z; 
       Z,        Pr*I];

endfunction
