## Copyright (C) 2018 G. D. McBain -*- octave -*-

## -*- texinfo -*-
## @deftypefn {Function File} {} [@var{V}, @var{Vpp}] = rayleigh (@var{x})
## Compute the velocity @var{V} and its second derivative @var{Vpp}
## for the Rayleigh flow on a grid @var{x} normalized by the diffusion
## timescale.
##
## Reference: Otto (1993)
##
## Otto, S. R. (1993). On the stability of a time dependent boundary
## layer. ICASE Report No. 93-73.  Institute for Computer Applications
## in Science and Engineering, Hampton, Virginia.
## @end deftypefn

function [V, Vpp] = otto (x)
  x = x(:);			# ensure x is a column
  
  V = erf(x);
  Vpp = -4 * x .* exp(-x.^2) / sqrt(pi);

endfunction
