## Copyright (C) 2006, 2007, 2012 G. D. McBain -*- octave -*-
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

## -*- texinfo -*-
## @deftypefn {Function File} {} [@var{L}, @var{M}] = gershuniplapp (@var{Re}, @var{Pr}, @var{k}, @var{D1T}, @var{D2}, @var{D4}, @var{D2T}, @var{V}, @var{Vpp}, @var{Tp})
## Assemble the stiffness @var{L} and mass @var{M} matrices for the
## equations governing the temporal linear stability of two-dimensional
## disturbances to a buoyancy layer, with vertical velocity profile
## @var{V} and second derivative @var{Vpp} and horizontal temperature
## gradient @var{Tp}; parameterized by the Reynolds @var{Re} and Prandtl
## @var{Pr} numbers, and the vertical wavenumber @var{k}.
##
## The caller provides the second- and fourth-order differentiation
## matrices, @var{D2} and @var{D4} for the velocity and the first- and
## second-order differentiation matrix @var{D2T} for the temperature.
##
## The wave speeds are the eigenvalues of @code{M\L}.
##
## The governing equations were derived independently by Gershuni (1953)
## and Plapp (1957). Gershuni's Orr--Sommerfeld equation is
##
## @example
##   f'''' - f'' (i w + 2 k^2 + i k v)
##
##         + f (i w k^2 + k^4 + i k^3 v + i k v'') - Gr q' = 0 .
## @end example
##
## If we put F = Gr f, V = Gr v, w/k = - Gr c and divide by i k Gr^2 we
## get
##
## @example
##   (F'''' - 2k^2 F'' + k^4 F - q') / i k Gr + (V(k^2-D^2)+V'') F =
##
##   c (k^2 - D^2) F ,
## @end example
##
## which is what we use here.  Gershuni's temperature equation is
##
## @example
##   i k T' f - q''/Pr + (i w + i k v + k^2/Pr) q = 0
## @end example
##
## which with the above and on dividing by i k Gr becomes
##
## @example
##   (k^2-D^2) q /i k Gr Pr + T' F + V q = c q ,
## @end example
##
## which is what we use here.
##
## If the input matrices are sparse, the outputs are too.
##
## G. Z. Gershuni 1953 Ob ustoichivosti ploskogo konvectivnogo
## dvizheniia zhidkosti (On the stability of plane convection motion of
## a liquid).  @cite{Zhurnal Tekhnischeskoi Fiziki}
## @strong{23}(10):1838--1844.
##
## J. E. Plapp 1957 The analytic study of laminar boundary-layer
## stability in free convection.  @cite{Journal of the Aeronautical
## Sciences} @strong{24}:318--319.
## @end deftypefn

## Author: G. D. McBain <gdmcbain@protonmail.com>
## Created: 2006-12-01
## Keywords: hydrodynamic stability

function [L, M] = gershuniplapp (Gr, Pr, k, D1T, D2, D4, D2T, V, Vpp, Tp)

  n = length (V);
  I = speye (n);
  Z = spalloc (n, n);

  MOS = k^2*I - D2;

  Lv = [k^4*I - 2*k^2*D2 + D4, -D1T;
	Z,                     (k^2*I-D2T)/Pr];
  
  Li = [(diag (V) * MOS + sparse (diag (Vpp))), Z;
	(sparse (diag (Tp))),                   (sparse (diag (V)))];

  L = Lv / (1i * k * Gr) + Li;

  M = [MOS, Z; 
       Z,   I];

endfunction
