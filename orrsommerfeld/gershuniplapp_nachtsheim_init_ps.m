function [V, Vpp, Tp, D, x] = gershuniplapp_nachtsheim_init_ps (n, Pr, fpp00, Tp00, l)

  t = orthopolyquad ("legendre", n);
  Dt = differentiation_matrices (t, 4);
  [x, tau] = map_ray_algebraic (t, l, 4);

  [F, H] = psb_shoot ([0; x], fpp00, Tp00, Pr);
  V = F(2:end,2); Vpp = F(2:end,4); Tp = H(2:end,2);

  D = map_differentiation_matrices (Dt, tau);
  weight = 1 + t;		# for u(0)=0, since t(0)=-1
  weight1 = [(ones (n, 1)), (zeros (n, 4-1))] ./ repmat (weight, [1, 4]);
  DtD = coerce_differentiation_matrices (Dt, weight, weight1);
  DD = map_differentiation_matrices (DtD, tau);	# u(0)=0
  DtDD = coerce_differentiation_matrices (DtD, weight, weight1);
  DDD = map_differentiation_matrices (DtDD,tau); # u(0)=u'(0)=0

  D{2} = DD{2};
  D{4} = DDD{4};

endfunction

%!demo
%!
%! ## finding a transverse segment near the critical point (Pr = 0.733)
%!
%! Pr = 0.733; 
%! alpha = 0.15;
%! [V, Vpp, Tp, D] = gershuniplapp_nachtsheim_init_ps (64, Pr, 0.675, -0.508, 1.0);
%! printf ("%4s\t%15s\n\n", "Re", "c1");
%! for Re = [63.5, 64.5]
%!   [L, M] = gershuniplapp (Re, Pr, alpha, D{1}, D{2}, D{4}, D{2}, V, Vpp, Tp);
%!   c = eig (M\L);
%!   [rc1, ic1] = max (imag (c));
%!   c1 = c(ic1);
%!   printf ("%4.1f\t% 6.4f %+6.4fi\n", Re, real (c1), imag (c1));
%! endfor

%!demo
%!
%! ## ... Finding a transverse segment near the bottom-right
%!
%! Pr = 0.733; 
%! Re = 200;
%! [V, Vpp, Tp, D, x] = gershuniplapp_nachtsheim_init_ps (64, Pr, 0.675, -0.508, 0.5); max (x)
%! printf ("%5s\t%15s\n\n", "alpha", "c1");
%! for alpha = [0.026, 0.04]
%!   [L, M] = gershuniplapp (Re, Pr, alpha, D{1}, D{2}, D{4}, D{2}, V, Vpp, Tp);
%!   c = eig (M\L); 
%!   [rc1, ic1] = max (imag (c));
%!   c1 = c(ic1);
%!   printf ("%5.3f\t% 6.4f %+6.4fi\n", alpha, real (c1), imag (c1));
%! endfor
