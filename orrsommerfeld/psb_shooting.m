## Copyright (C) 2006, 2007 G. D. McBain <gdmcbain@protonmail.com>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

## -*- texinfo -*-
## @deftypefn {Function File} {} [@var{fpp0}, @var{q0}] = psb_shooting (@var{l}, @var{fpp0}, @var{q0}, @var{Pr})
## Compute the missing initial conditions for natural convection
## boundary layer on an isothermal hot vertical wall in an isothermal
## cold fluid of Prandtl number @var{Pr} using the shooting method from
## the guessed (dimensionless) skin friction @var{fpp0} and heat flux
## @var{q0}.  See @code{psb_shoot} for the details of the equations.
## @seealso{psb_shoot}
## @end deftypefn

## ### REFERENCES
##
## B. Gebhart, Y. Jaluria, R. L. Mahajan, & B. Sammakia 1988
## @cite{Buoyancy-Induced Flows and Transport}, reference edition,
## Hemisphere, New York.  (See especially table 3.4.1, p. 53.)
##
## E. Schmidt & W. Beckmann 1930 Das Temperatur- und
## Geschwindigkeitsfeld vor einer W@"arme abgebenden senkrechten Platte
## bei nat@"urlicher Konvektion.  II Die Versuche und ihre Ergebnisse.
## @cite{Techn. Mechan. u. Thermodynamik} @strong{1}(11):391--406.
 
## Author: G. D. McBain <geordie.mcbain@aeromech.usyd.edu.au>
## Created: 2006-12-05
## Keywords: natural convection, boundary layer, similarity, shooting method

function [fpp0, q0] = psb_shooting (l, fpp00, q00, Pr)
  h = fsolve (@ (h) psb_shooting_r (h, [0; l], Pr), [fpp00, q00]);
  fpp0 = h(1); q0 = h(2);
endfunction

function r = psb_shooting_r (h, x, Pr)

  [f, theta] = psb_shoot (x, h(1), h(2), Pr);
  r = [f(end,2), theta(end,1)];

endfunction

%!test
%! Pr = 2.2/3;
%! fpp00 = 0.675; q00 = -0.508; l = 6; # Schmidt & Beckmann (1930)
%! [fpp0, q0] = psb_shooting (l, fpp00, q00, Pr);
%! assert ([fpp0, q0], [fpp00, q00], 1e-2)

%!test
%! Pr = 6.7;
%! fpp00 = 0.4547; q00 = -1.0408; l = 8.5; # Nachtshiem (1963)
%! [fpp0, q0] = psb_shooting (l, fpp00, q00, Pr);
%! assert ([fpp0, q0], [fpp00, q00], 1e-4)

%!demo
%!
%! ## Pr = 2.2/3 (Schmidt & Beckmann 1930, table 4, at p. 402)
%!
%! [fpp0, q0] = psb_shooting (6, 0.675, -0.508, 2.2/3)

%!demo
%!
%! ## ditto, with dependence on domain truncation
%!
%! printf ("%2s\t%6s  %6s\n\n", "l", "-q", "f''(0)");
%! for l = [2:10, 20]
%!   [fpp0, q0] = psb_shooting (l, 0.675, -0.508, 2.2/3);
%! printf ("%2d\t%6.4f  %6.4f\n", l, -q0, fpp0);
%! endfor


%!demo
%!
%! ## Pr = 6.7 (Nachtsheim 1963, table 1, at p. 28)
%!
%! printf ("%2s\t%6s  %6s\n\n", "l", "-q", "f''(0)");
%! for l = [2:15, 30, 40]
%!   [fpp0, q0] = psb_shooting (l, 0.4547, -1.0408, 6.7);
%! printf ("%2d\t%6.4f  %6.4f\n", l, -q0, fpp0);
%! endfor

%!demo
%!
%! ## Gebhart et al. (1988, table 3.4.1, p. 53)
%!
%! data = [0.01, 0.9873, -0.0807; 0.1, 0.8591, -0.2301; 0.72, 0.6760, -0.5046; 1.0, 0.6422, -0.5671; 2.0, 0.5713, -0.7165; 5.0, 0.4818, -0.9540; 6.7, 0.4548, -1.0408; 10.0, 0.4192, -1.1693; 100.0, 0.2517, -2.1913];
%! printf ("%6s  %6s  %6s\n\n", "Pr", "-q", "f''(0)");
%! for i = 1:(rows (data))
%!   [fpp0, q0] = psb_shooting (33, data(i,2), data(i,3), data(i,1));
%!   printf ("%6.2f  %6.4f  %6.4f\n", data(i,1), -q0, fpp0);
%! endfor
