## Copyright (C) 2006, 2007, 2018 G. D. McBain -*- octave -*-
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

## -*- texinfo -*- 
## @deftypefn {Function File} {} [@var{V}, @var{Vpp}, @var{Tp}, @var{D}, @var{D2T}] = gilldavey_anabatic_init_fd (@var{x}, @var{Pr}, @var{thetabc}, @var{xinf}, @var{ell}) 
## Set up on a grid @var{x} the linear stability of the natural
## convection boundary layer on an heated vertical wall in a stratified
## fluid of Prandtl number @var{Pr}; either for fixed heat flux
## "Neumann" or excess temperature "Dirichlet" thermal boundary
## condition @var{thetabc}.  Only the first character @var{thetabc} is
## inspected and that without regard to case.
##
## Obtain the base solution profiles @var{V}, @var{Vpp}, and @var{Tp}
## from @command{anabatic} (see which for their interpretation).
##
## Return the first-, second-, and fourth-order finite difference
## differentiation matrices with Dirichlet boundary conditions in the
## cell array @var{D}, and a second-order differentiation matrix
## @var{D2T} with appropriate thermal boundary conditions.
##
## The boundary conditions at infinity are treated by simply truncating
## the domain at @var{xinf}.
##
## Accurate solutions of the linear stability equations have been
## computed by Gill & Davey (1969) for the Dirichlet case and McBain &
## Armfield (2004) and McBain, Armfield, & Desrayaud (in press) for the
## Neumann case.
##
## A. E. Gill & A. Davey 1969 Instabilities of a buoyancy driven system.
## @cite{Journal of Fluid Mechanics} @strong{35}:775--798.
##
## G. D. McBain & S. W. Armfield 2004 Linear stability of natural
## convection on an evenly heated vertical wall. In M. Behnia, W. Lin, &
## G. D. McBain (eds) @cite{Proceedings of the Fifteenth Australasian
## Fluid Mechanics Conference}, The University of Sydney, Paper
## AFMC00196.
##
## G. D. McBain, S. W. Armfield, &  G. Desrayaud (in press) Instability
## of the buoyancy layer on an evenly heated vertical wall.
## @cite{Journal of Fluid Mechanics}.
##
## @seealso{finidiff_matrix_dirichlet, finidiff_matrix2_mod} 
## @end deftypefn

## Author: G. D. McBain <gdmcbain@protonmail.com>
## Created: 2006-11-29
## Keywords: natural convection, hydrodynamic stability

function [V, Vpp, Tp, D, D2T] = gilldavey_anabatic_init_fd (x, Pr, thetabc, xinf)

  [V, Vpp, T, Tp] = anabatic (x);

  for q = [1, 2, 4]
    D{q} = finidiff_matrix_dirichlet (x, q, 0, xinf);
  endfor  
  D{1}(1,1:3) = finidiff (x(1:3), x(1), 1); # remove b.c.
  
  switch (toupper (thetabc(1)))	## Fix temperature or its gradient?
    case ("D")			# Dirichlet
      D2T = D{2};
    case ("N")			# Neumann
      D2T = finidiff_matrix2_mod (D{2}, x, 0, 1);
    otherwise
      print_usage ();		
  endswitch

endfunction

%!test
%!
%! ## a transverse segment for Pr = 2 and Dirichlet thermal b.c.
%!
%!
%! Pr = 2;
%! Re = 400;
%!
%! xinf = 12;
%! h = 0.1;
%! r = 1 + 1.25 * h;
%! n = ceil (grid_cellcentred_n (h, r, xinf));
%! x = grid_cellcentred (h, r, n);
%! xinf = x(end) + x(end)-x(end-1);
%!
%! [V, Vpp, Tp, D, D2T] = gilldavey_anabatic_init_fd (x, Pr, "D", xinf);
%!
%! ps = gilldavey_unstable_p ([Re; 0.01], Pr, D{1}, D{2}, D{4}, D2T, V, Vpp, Tp);
%! pu = gilldavey_unstable_p ([Re; 0.5], Pr, D{1}, D{2}, D{4}, D2T, V, Vpp, Tp);
%! assert (~ps, pu);


%!demo
%!
%! ## the critical mode at Pr = 0.72, Dirichlet thermal b.c.
%! ## (takes about three seconds)
%!
%! Pr = 0.72; 
%! Re = 109; 
%! kappa = 0.502; 
%!
%! c1GD = 0.200; # (Gill & Davey 1969, table 1)
%! xinf = 12;
%! printf ("%s\t%3s\t%13s    %s\n\n", "h", "n", "uppermost c", "error"); 
%! h = 1;
%! do
%!   h /= 2;
%!   r = 1 + 1.25 * h;
%!   x = grid_cellcentred (h, r, max (ceil (grid_cellcentred_n (h, r, xinf)), 4));
%!   xinf = x(end) + x(end)-x(end-1);
%!   [V, Vpp, Tp, D, D2T] = gilldavey_anabatic_init_fd (x, Pr, "D", xinf);
%!   [L, M] = gilldavey (Re, Pr, kappa, D{1}, D{2}, D{4}, D2T, V, Vpp, Tp);
%!   c = eig (M\L);
%!   [rc1, ic1] = max (imag (c));
%!   c1 = c(ic1);
%!   err = c1 - c1GD;
%!   printf ("%e\t%3d\t%- 6.3f %+6.3fi % .1e\n", h, length (x), real (c1), imag (c1), err); 
%! until (length (x) > 128)

%!demo
%!
%! ## the critical mode at Pr = 0.7, Neumann thermal b.c.
%! ## (takes about three seconds)
%!
%! Pr = 0.7; 
%! Re = 44.48526; 
%! kappa = 0.3858; 
%!
%! c1MA = 0.3200; # (McBain & Armfield 2004, table 1)
%! xinf = 12;
%! printf ("%3s\t%13s    %s\n\n", "n", "uppermost c", "error"); 
%! h = 1;
%! do 
%!   xinf = 12;
%!   h /= 2;
%!   r = 1 + 1.25 * h;
%!   n = grid_cellcentred_n (h, r, xinf);
%!   n = max (ceil (n), 4);
%!   x = grid_cellcentred (h, r, n);
%!   xinf = x(end) + x(end)-x(end-1);
%!   [V, Vpp, Tp, D, D2T] = gilldavey_anabatic_init_fd (x, Pr, "N", xinf);
%!   [L, M] = gilldavey (Re, Pr, kappa, D{1}, D{2}, D{4}, D2T, V, Vpp, Tp);
%!   c = eig (M\L);
%!   [rc1, ic1] = max (imag (c));
%!   c1 = c(ic1);
%!   err(n) = c1 - c1MA;
%!   printf ("%3d\t%- 6.3f %+6.3fi % .1e\n", n, real (c1), imag (c1), err(n)); 
%! until (n > 128)

%!demo
%!
%! ## stability margin for Pr = 2 (Gill & Davey 1969, figure 2)
%! ## (takes about 16 s)
%!
%! Pr = 2;
%! Re = 400;
%!
%! xinf = 12;
%! h = 0.1;
%! r = 1 + 1.25 * h;
%! n = ceil (grid_cellcentred_n (h, r, xinf));
%! x = grid_cellcentred (h, r, n);
%! xinf = x(end) + x(end)-x(end-1);
%!
%! [V, Vpp, Tp, D, D2T] = gilldavey_anabatic_init_fd (x, Pr, "D", xinf);
%!
%! printf (" %9s\t%9s\n\n", "R", "alpha");
%! m = askirt ([Re; 0.01], [Re; 0.5], @ (R) gilldavey_unstable_p (R, Pr, D{1}, D{2}, D{4}, D2T, V, Vpp, Tp), [1, 1e-3], 4, 1.2, -[Inf; Inf], [Re; 1], 1e4, true);
%! plot (m(1,:), m(2,:)) 
%! title ("Neutral stabilty curve for Pr = 2 (after Gill & Davey 1969, figure 2)")
%! xlabel ("Reynolds number, R")
%! ylabel ("reduced wave number, alpha")
%! axis ([0, 400, 0, 1])
