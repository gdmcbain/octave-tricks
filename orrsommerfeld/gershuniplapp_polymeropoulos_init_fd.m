## Copyright (C) 2006, 2007 G. D. McBain -*- octave -*-
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or (at
## your option) any later version.
##
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301USA

## -*- texinfo -*-
## @deftypefn {Function File} {} [@var{V}, @var{Vpp}, @var{Tp}, @var{D}, @var{D2T}] = gershuniplapp_polymeropoulos_init_fd (@var{x}, @var{Pr}, @var{fpp00}, @var{Tp00})
## Set up the linear stability of the natural convection boundary
## layer on an evenly heated vertical wall in an isothermal fluid; i.e.
## for the abscissae @var{x} and Prandtl number @var{Pr}, compute the
## vertical velocity @var{V}, its curvature @var{Vpp}, the horizontal
## temperature gradient @var{Tp}, and a cell array @var{D} of first,
## second, and fourth-order differentiation matrices with Dirichlet
## conditions and another (identical in this case) second-order
## differentiation matrix for the temperature.  The caller must provide
## the missing initial conditions @var{fpp00} and @var{Tp00} of the
## similarity equations for the base solution.
## @seealso{sparrowgregg}
## @seealso{finidiff, finidiff_matrix_dirichlet, finidiff_matrix2_mod}
## @end deftypefn

## Author: G. D. McBain <gdmcbain@protonmail.com>
## Created: 2007-03-02
## Keywords: natural convection, linear stability

function [V, Vpp, Tp, D, D2T] = gershuniplapp_polymeropoulos_init_fd (x, Pr, fpp00, Tp00)

  n = length (x);
  xinf = x(n) + (x(n)-x(n-1));
  [F, H] = sparrowgregg ([0; x; xinf], Pr, fpp00, Tp00);
  V = F((1:n)+1,2); Vpp = F((1:n)+1,4); Tp = H((1:n)+1,2);
  
  ## TODO: replace the far-field Dirichlet approximation with correct
  ## asymptotic boundary conditions
  
  for q = [1, 2, 4]
    D{q} = finidiff_matrix_dirichlet (x, q, 0, xinf);
  endfor  

  D{1}(1,1:3) = finidiff (x(1:3), x(1), 1); # no b.c.
  D2T = finidiff_matrix2_mod (D{2}, x, 0, 1); # Neumann
  
endfunction

%!test
%!
%! ## finding a segment transverse across the lower leg of the 
%! ## stability margin for Pr = 0.733
%!
%! Pr = 0.733; 
%! G = 200;
%!
%! xinf = 15; 
%! n = xinf/0.1; 
%! x = (linspace (0, xinf, n+2) ((1:n)+1))';
%!
%! fpp0 = 0.80893; h0 = -1.47981; # values for Pr = 0.733
%! [V, Vpp, Tp, D, D2T] = gershuniplapp_polymeropoulos_init_fd (x, Pr, fpp0, h0);
%! pstable = gershuniplapp_unstable_p ([G; 1e-3], Pr, D{1}, D{2}, D{4}, D2T, V, Vpp, -Tp);
%! punstable = gershuniplapp_unstable_p ([G; 1e-2], Pr, D{1}, D{2}, D{4}, D2T, V, Vpp, -Tp);
%! assert (pstable ~= punstable);

%!demo
%!
%! ## finding a segment transverse across the lower leg of the 
%! ## stability margin for Pr = 6.7
%!
%! Pr = 6.7; 
%! G = 200;
%!
%! xinf = 15; 
%! n = xinf/0.1; 
%! x = (linspace (0, xinf, n+2) ((1:n)+1))';
%!
%! fpp0 = 0.35633; h0 = -0.84170; # values for Pr = 6.7
%! [V, Vpp, Tp, D, D2T] = gershuniplapp_polymeropoulos_init_fd (x, Pr, fpp0, h0);
%! for alpha = [0.005, 0.020]
%!   [L, M] = gershuniplapp (G, Pr, alpha, D{1}, D{2}, D{4}, D2T, V, Vpp, -Tp);
%!   c = eig (M\L); 
%!   [rc1, ic1] = max (imag (c)); 
%!   c1 = c(ic1)
%! endfor

%!demo
%!
%! ## energetics of the two local minima for Pr = 0.733: the long wave 
%! ## is buoyancy-driven and the short wave is mechanically driven
%!
%! Pr = 0.733; 
%! alpha = [0.13, 0.58]; 
%! G = [12.3, 170.3];
%!
%! xinf = 15; 
%! n = xinf/0.1; 
%! x = (linspace (0, xinf, n+2) ((1:n)+1))';
%! w = finidiff_quad_trapezoidal (x, 0, xinf);
%!
%! fpp0 = 0.80893; h0 = -1.47981; # values for Pr = 0.733
%! [V, Vpp, Tp, D, D2T] = gershuniplapp_polymeropoulos_init_fd (x, Pr, fpp0, h0);
%! printf ("%5s  %14s  %9s  %9s  %s\n\n", "G*", "omega", "G* M", "B", "type of instability");
%! for i = 1:2
%!   [L, M] = gershuniplapp (G(i), Pr, alpha(i), D{1}, D{2}, D{4}, D2T, V, Vpp, -Tp);
%!   [q, c] = eig (M\L); 
%!   [rc1, ic1] = max (imag (diag (c))); 
%!   omega = alpha(i) * c(ic1,ic1);
%!
%!   psi = q(1:n,ic1); 
%!   theta = q(n+1:2*n,ic1);
%!
%!   [EM, DM, B, M] = gershuniplapp_energetics (alpha(i), psi, theta, w, D{1}, D{2}, D{4}, V);
%!
%!   printf ("%5.1f  %5.3f %+ 8.1ei %+ 9.2e  %+ 9.2e  ", G(i), real (omega), imag (omega), G(i)*M, B);
%!
%!   if (B<G(i)*M)
%!     printf ("M"); # M for mechanically
%!   else 
%!     printf ("B"); # B for buoyancy-
%!   endif; 
%!   disp (".D.");
%!
%! endfor

%!demo
%!
%! ## stability margin for Pr = 0.733 (cf. Hieber & Gebhart (1971, figure 2)
%! ## (takes about three minutes)
%!
%! Pr = 0.733;
%!
%! xinf = 15;
%! h = 0.1;
%! r = 1 + 1.25 * h;
%! n = ceil (grid_cellcentred_n (h, r, xinf));
%! x = grid_cellcentred (h, r, n);
%!
%! fpp0 = 0.80893; h0 = -1.47981;
%! [V, Vpp, Tp, D, D2T] = gershuniplapp_polymeropoulos_init_fd (x, Pr, fpp0, h0);
%!
%! amp = [0, -0.02, -0.04, -0.06, -0.075];
%! Gs = alpha = omega = NaN (0, length (amp));
%! for j = 1:(length (amp))
%!   printf ("\nimag (alpha) = %g\n\n", amp(j));
%!   G = 600;
%!   alphai = amp(j);
%!   m = askirt ([1e3; 1i*amp(j)], [600; 0.4+1i*amp(j)], @ (Gk) gershuniplapp_unstable_p (Gk, Pr, D{1}, D{2}, D{4}, D2T, V, Vpp, -Tp), [1, 0.001], 4, 1.2, -[Inf; Inf], [1e3; Inf], 300, true);
%!   nm(j) = columns (m);
%!   Gs(1:nm(j),j) = m(1,:);
%!   alpha(1:nm(j),j) = m(2,:);
%!
%!   for i = 1:(nm(j))
%!     [L, M] = gershuniplapp (Gs(i,j), Pr, alpha(i,j), D{1}, D{2}, D{4}, D2T, V, Vpp, -Tp);
%!     c = eig (M\L);
%!     [ic1, ic1i] = max (imag (c));
%!     omega(i,j) = alpha(i,j) * c(ic1i);
%!     printf ("%6.1f\t%6.4f\t%+ 6.3fi\t%6.4f\t%+ 8.1ei\n", Gs(i,j), real (alpha(i,j)), imag (alpha(i,j)), real (omega(i,j)), imag (omega(i,j)));
%!   endfor
%! endfor
%!
%! plot (Gs(1:nm(1),1), real (omega(1:nm(1),1)), ";imag (alpha) = 0;", Gs(1:nm(2),2), real (omega(1:nm(2),2)), ";-0.02;", Gs(1:nm(3),3), real (omega(1:nm(3),3)), ";-0.04;", Gs(1:nm(4),4), real (omega(1:nm(4),4)), ";-0.06;", Gs(1:nm(5),5), real (omega(1:nm(5),5)), ";-0.075;")
%! axis ([0, 1000, 0, 0.14])
%! xlabel ("G*")
%! ylabel ("reduced frequency")
%! title ("amplification level curves, Pr = 0.733 (Hieber & Gebhart 1971, figure 2)")
