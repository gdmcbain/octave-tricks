## Copyright (C) 2006, 2007, 2018 G. D. McBain -*- octave -*-
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or (at
## your option) any later version.
##
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301USA

## -*- texinfo -*- @deftypefn {Function File} {} [@var{L}, @var{M}] =
## orrsommerfeld_poiseuille_init_ps (@var{x}, @var{Re}, @var{k}) 
## Compute the stiffness @var{L} and mass @var{M} matrices for the
## Orr--Sommerfeld equation for plane Poiseuille flow at Reynolds number
## @var{Re} and stream-wise wave number @var{k}, discretized using
## polynomials on grid @var{x}.
##
## Good choices for the grid include @code{orthopolyquad ("type", n)}
## for "type" = "Legendre" or "Chebyshev" and n > 32.
##
## See Orszag (1971, eq. 2) or Drazin (2002, eq. 8.49) for the
## Orr--Sommerfeld equation; the equation solved by Weideman & Reddy
## (2000) is a special case, involving some abuse of notation.
## 
## P. G. Drazin 2002 Introduction to Hydrodynamic Stability, Cambridge
## University Press.
##
## S. A. Orszag 1971 Accurate solution of the Orr--Sommerfeld stability
## equation. Journal of Fluid Mechanics 50:689--703.
##
## J. A. C. Weideman & S. C. Reddy 2000 A MATLAB differentiation matrix
## suite, ACM Transactions on Mathematical Software 26(4):465--519.
## @seealso{differentiation_matrices, differentiation_matrices}
## @seealso{orthopolyquad}
## @end deftypefn

## Author: G. D. McBain <geordie.mcbain@aeromech.usyd.edu.au>
## Created: 2006-03-21
## Keywords: collocation, differentiation, hydrodynamic stability

function [V, Vpp, D] = orrsommerfeld_poiseuille_init_ps (x)

  n = length (x);
  V = 1 - x.^2;
  Vpp = -2 * ones (n, 1);

  D0 = differentiation_matrices (x, Q=4);

  alpha = 1 - x.^2;		# v (+/- 1) = 0
  beta = -2*[x, (ones (n, 1)), (zeros (n, 2))] ./ repmat (alpha, 1, Q);
  D = coerce_differentiation_matrices (D0, alpha, beta);
  Dv = coerce_differentiation_matrices (D, alpha, beta);
  D{4} = Dv{4};

endfunction

%!demo
%!
%! ## The standard benchmark problem is Re = 1e4, k = 1
%! 
%! ## First, the spectrum (Drazin 2002, figure 8.9, p. 171)
%!
%! x = orthopolyquad ("legendre", 128);
%! [V, Vpp, D] = orrsommerfeld_poiseuille_init_ps (x);
%! [L, M] = orrsommerfeld (1e4, 1.0, D{2}, D{4}, V, Vpp);
%! c = eig (M\L);
%! axis ([0, 1, -0.8, 0.005]);
%! plot (real (c), imag (c), "*;;");

%!demo
%!
%! ## most unstable mode for Poiseuille flow at Re = 1e4, k = 1
%! 
%! ## The dimensionless wave speed for this benchmark is
%! ## c1 = 0.23752649 +0.00373967i (Orszag 1971, at p. 695)
%! ## c1C = 0.237526488821 + 0.003739670623i; # (Chubb 2006, eq. 7.1)
%!
%! c1C = 0.237526488821 + 0.003739670623i; # (Chubb 2006, eq. 7.1)
%! Re = 1e4; k = 1;
%! printf ("%3s\t%11s %11s \t%s\n\n", "n", "c1", "", "error");
%! for n = 2 .^ (1:7)
%!   x = orthopolyquad ("Legendre", n);
%!   [V, Vpp, D] = orrsommerfeld_poiseuille_init_ps (x);
%!   [L, M] = orrsommerfeld (Re, k, D{2}, D{4}, V, Vpp);
%!   c = eig (M\L);
%!   [rc1, ic1] = max (imag (c));
%!   c1 = c(ic1);
%!   err = abs (c1 - c1C);
%!   printf ("%3d\t%- 11.8f %+11.8fi\t%e\n", n, real (c1), imag (c1), err); 
%! endfor

%!demo
%!
%! ## ditto, using Chebyshev points
%! 
%! ## The dimensionless wave speed for this benchmark is
%! ## c1 = 0.23752649 +0.00373967i (Orszag 1971, at p. 695)
%! ## c1C = 0.237526488821 + 0.003739670623i; # (Chubb 2006, eq. 7.1)
%!
%! c1C = 0.237526488821 + 0.003739670623i; # (Chubb 2006, eq. 7.1)
%! Re = 1e4; k = 1;
%! printf ("%3s\t%11s %11s \t%s\n\n", "n", "c1", "", "error");
%! for n = 2 .^ (1:7)
%!   x = orthopolyquad ("Chebyshev", n);
%!   [V, Vpp, D] = orrsommerfeld_poiseuille_init_ps (x);
%!   [L, M] = orrsommerfeld (Re, k, D{2}, D{4}, V, Vpp);
%!   c = eig (M\L);
%!   [rc1, ic1] = max (imag (c));
%!   c1 = c(ic1);
%!   err = abs (c1 - c1C);
%!   printf ("%3d\t%- 11.8f %+11.8fi\t%e\n", n, real (c1), imag (c1), err); 
%! endfor
