## Copyright (C) 2007, 2012, 2014 G. D. McBain -*- octave -*-
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

## -*- texinfo -*-
## @deftypefn {Function File} {} gershuniplapp_marginal (@var{alpha}, @var{GrL}, @var{GrH}, @var{Grtol}, @var{Pr}, @var{D1}, @var{D2M}, @var{D4}, @var{D2T}, @var{V}, @var{Vpp}, @var{Tp})
## Compute the marginal Grashof number at wavenumber @var{alpha} and
## Prandtl number @var{Pr}, using bisection between @var{GrL} and
## @var{GrH} with tolerance @var{Grtol}.
## @seealso{gershuniplapp_unstable_p, bisect_expanding}
## @end deftypefn

## Author: G. D. McBain <gdmcbain@protonmail.com>
## Created: 2007-01-30
## Keywords: hydrodynamic stability, natural convection

function Gr = gershuniplapp_marginal (alpha, GrL, GrH, Grtol, Pr, D, D2, D4,
				      D2T, V, V2, Tp)

  Gr = bisect_expanding (@ (Gr) gershuniplapp_unstable_p ([Gr; alpha],
 							  Pr, D, D2, D4, D2T, 
 							  V, V2, Tp), 
			 GrL, GrH, Grtol );
			
endfunction
