## Copyright (C) 2007 G. D. McBain <gdmcbain@protonmail.com>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

## -*- texinfo -*-
## @deftypefn {Function File} {} orrsommerfeld_unstable_p (@var{Rk}, @var{D2}, @var{D4}, @var{V}, @var{Vpp})
## Return 1 if the flow is linearly unstable and zero otherwise.  The
## Reynolds number and stream-wise wave number are packed in the
## two-element column @var{Rk}.  The columns @var{V} and @var{Vpp} are
## the velocity profile and its second derivative.  @var{D2} and
## @var{D4} are second- and fourth-order differentiation matrices.
## @var{D4} should include impermeability and no-slip boundary
## conditions, but @var{D2} should only include the former.
## @seealso{orrsommerfeld} 
## @end deftypefn

## Author: G. D. McBain <gdmcbain@protonmail.com>
## Created: 2007-05-22
## Keywords: hydrodynamic stability

function p = orrsommerfeld_unstable_p (Rk, D2, D4, V, Vpp)
  
  if ((p = all (Rk>0)))		# first quadrant
    [L, M] = orrsommerfeld (Rk(1), Rk(2), D2, D4, V, Vpp);
    p = any (imag (Rk(2) * eig (M\L)) > 0);
  endif

endfunction
