## Copyright (C) 2006, 2007 G. D. McBain <gdmcbain@protonmail.com>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

## -*- texinfo -*-
## @deftypefn {Function File} {} [@var{f}, @var{theta}] = sparrowgregg (@var{x}, @var{fpp0}, @var{theta0}, @var{Pr})
## Compute the natural convection boundary layer on an evenly heated
## vertical wall in an isothermal fluid of Prandtl number @var{Pr},
## reporting the results at @var{x}, a vector of abscissae beginning
## from zero, using a shooting method from the guessed (dimensionless)
## skin friction @var{fpp0} and heat flux @var{theta0}. Good guesses can
## be improved with
##
## @example
## @code{[@var{fpp0}, @var{theta0}] =  sparrowgregg_shooting (max (@var{x}), @var{fpp0}, @var{theta0}, @var{Pr}))}.
## @end example
##
## The similarity variables are nondimensionalized as by Sparrow & Gregg
## (1956).  The governing equations are (Sparrow & Gregg 1956, eqs 2a--3a)
##
## @example
## @group
## f''' - 3 (f')^2 + 4 f f'' - t = 0
##      t'' + Pr (4 t' f - t f') = 0
##
## f = f' = 0, t' = 1    (x = 0) 
## f', t ~ 0             (x -> Inf).
## @end group
## @end example
##
## The same equations are used by Hieber & Gebhart (1971) but with H =
## -t.
##
## C. A. Hieber & B. Gebhart 1971 Stability of vertical natural
## convection boundary layer: some numerical solutions.  Journal of
## Fluid Mechanics 48:625--646.
##
## E. M. Sparrow and J. L. Gregg 1956 Laminar free convection from a
## vertical plate with a uniform surface heat flux.  @cite{Transactions
## of the ASME} @strong{78}:435--440.
## @seealso{sg_shooting}
## @end deftypefn

## ### BACKGROUND THEORY
##
## ### ## Equivalent first order system
##
## The vector y = [f; f'; f''; q; q'] has the derivative 
##
##   y' = [f'   = y(2); 
##         f''  = y(3); 
##         f''' = 3*y(2)^2 - 4*y(1)*y(3) + y(4); 
##         q'   = y(5); 
##         q''  = Pr*(y(4)*y(2) - 4*y(5)*y(1))];
##
## This is what is computed here by sg_evolution

## Author: G. D. McBain <geordie.mcbain@aeromech.usyd.edu.au>
## Created: 2006-12-05
## Keywords: natural convection, boundary layer, similarity, shooting method

function [f, h] = sparrowgregg (x, Pr, fpp0, h0)

  global sg_Pr
  sg_Pr = Pr;
  assert (x(1), 0);

  F = lsode ({@sg_evolution, @sg_jac}, [0; 0; fpp0; h0; 1], x);

  f = [F(:,1:3), 3*F(:,2).^2 - 4.*F(:,1).*F(:,3) + F(:,4)];
  h = F(:,4:5);

endfunction

function yp = sg_evolution (y)
  global sg_Pr
  yp = [y(2);			
	y(3);
	3*y(2)^2 - 4*y(1)*y(3) + y(4);
	y(5);
	sg_Pr*(y(4)*y(2)-4*y(5)*y(1)) ];
endfunction

function j = sg_jac (y)
  global sg_Pr
  j = [0, 1, 0, 0, 0;
       0, 0, 1, 0, 0;
       -4*y(3), 6*y(2), -4*y(1), 1, 0;
       0, 0, 0, 0, 1;
       sg_Pr*[-4*y(5), y(4), 0, y(2), -4*y(1)] ];
endfunction

%!demo
%!
%! ## Sparrow & Gregg (1956, figure 5)
%!
%! data = [0.1, -2.7507, 1.6434; 1, -1.3574, 0.72196; 10, -0.76746, 0.30639; 100, -0.46566, 0.12620];
%! x = linspace (0, 7, 100);
%! for i = 1:(rows (data))
%!   [f, h] = sparrowgregg (x, Pr = data(i,1), data(i,3), data(i,2));
%!   v(:,i) = f(:,2); theta(:,i) = h(:,1);
%! endfor
%! plot (x, v(:,1), sprintf (";Pr = %g;", data(1,1)), x, v(:,2), sprintf (";%g;", data(2,1)), x, v(:,3), sprintf (";%g;", data(3,1)), x, v(:,4), sprintf (";%g;", data(4,1)));

%!demo
%!
%! ## Sparrow & Gregg (1956, figure 6)
%!
%! data = [0.1, -2.7507, 1.6434; 1, -1.3574, 0.72196; 10, -0.76746, 0.30639; 100, -0.46566, 0.12620];
%! x = linspace (0, 7, 100)';
%! printf ("%5s\t%8s\n\n", "Pr", "theta (0)");
%! for i = 1:(rows (data))
%!   [f, h] = sparrowgregg (x, Pr = data(i,1), data(i,3), data(i,2));
%!   v(:,i) = f(:,2); theta(:,i) = h(:,1);
%!   printf ("%5.0e\t%- 8.5f\n", Pr, h(1,1));
%! endfor
%! theta ./= repmat (theta(1,:), size (x));
%! plot (x, theta(:,1), sprintf (";Pr = %g;", data(1,1)), x, theta(:,2), sprintf (";%g;", data(2,1)), x, theta(:,3), sprintf (";%g;", data(3,1)), x, theta(:,4), sprintf (";%g;", data(4,1)));

%!demo
%!
%! ## the far-field for Pr = 0.1
%!
%! Pr = 0.1;
%! x = 0:0.1:20;
%! [f, h] = sparrowgregg (x, Pr, 1.6434, -2.7507);
%! plot (x, f(:,2), "b;V;", x, h(:,1), "r;T;")
