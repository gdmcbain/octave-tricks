## Copyright (C) 2006, 2012, 2014 G. D. McBain -*- octave -*-
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

## -*- texinfo -*-
## @deftypefn {Function File} {} [@var{v}, @var{vpp}, @var{t}, @var{tp}] = equalizing (@var{x}, @var{Pr})
## Return the one-dimensional transient natural convection solution of
## the Oberbeck--Boussinesq equations for an initially stagnant
## isothermal body of fluid of Prandtl number @var{Pr} against an
## infinite vertical wall that suddenly jumps in temperature;
## specifically, return the vertical velocity @var{v},
##
## @example
## @group
## -x/2 .* ierfc (abs (x)), Pr == 1;
##
## Pr * sign (x) / (Pr - 1) .* (i2erfc (abs (x)) - i2erfc (abs (x) / sqrt (Pr))), otherwise;
## @end group
## @end example
##
## its second derivative @var{vpp}, and the temperature @code{@var{t} =
## erf (@var{x})} and its derivative @var{tp}.
##
## If some @var{x} are negative, use the solution for two bodies of
## fluid of equal Prandtl number @var{Pr} thermally equalizing across a
## thin vertical conducting wall; this coincides with the former for
## positive @var{x}.
##
## The grid @var{x} is interpreted using the length scale @code{2 * sqrt
## (kappa * time)}, where kappa is the thermal diffusivity.  The
## temperature scale and reference level is such that the initial
## temperature is @code{sign (x)}.  The velocity scale is @code{4 * g *
## beta * DeltaT * kappa * time / nu}. 
##
## C. R. Illingworth 1950 Unsteady laminar flow of a gas near an
## infinite flat plate.  @cite{Proceedings of the Cambridge Philosophical
## Society} @strong{46}:603--613.
##
## J. A. Schetz & R. Eichhorn 1962 Unsteady natural convection in the
## vicinity of a doubly-infinite vertical plate.  @cite{Journal of Heat
## Transfer} @strong{84}:334--338.
## @seealso{ierfc, i2erfc}
## @end deftypefn

## ### NONDIMENSIONALIZATION
##
## The reference temperature level is average of the initial
## temperatures and the reference temperature difference (DeltaT) is
## half the initial temperature difference.
##
## The length scale is 2 * sqrt (kappa * t), where t is the dimensional
## time.
##
## The velocity scale is 4 * g * beta * DeltaT * kappa * t / nu .
##
## Thus the Reynolds number is 8 * g * beta * DeltaT * (kappa *
## t).^(3/2) / nu^2
##
## The Grashof number is equal to the Reynolds number.
##
## The time scale is nu / (2 * g * beta * DeltaT * sqrt (kappa * t)) .

## Author: G. D. McBain <gdmcbain@protonmail.com>
## Created: 2006-12-06
## Keywords: natural convection

function [v, vpp, t, tp] = equalizing (x, Pr)

  switch Pr

    case 1

      v = -x/2 .* ierfc (abs (x));

      vpp = sign (x) .* erfc (abs (x)) - x .* exp (-x.^2) / sqrt (pi);

    case Inf

      v = sign (x) .* (i2erfc (abs (x)) - i2erfc (0.0));

      vpp = sign (x) .* erfc (abs (x));

    otherwise

      v = (Pr * sign (x) / (Pr - 1) .*
	   (i2erfc (abs (x)) - i2erfc (abs (x) / sqrt (Pr))));

      vpp = (sign (x) / (Pr - 1) .*
	     (Pr * erfc (abs (x)) - erfc (abs (x) / sqrt (Pr))));

  endswitch

   t = erf (x);
  tp = 2 * exp (-x.^2) / sqrt (pi);

endfunction

%!demo
%!
%! ## The dimensionless base solution profiles for various Prandtl numbers.
%! ## On the half-space x > 0, this coincices with the solution for a fluid
%! ## bounded by a plane vertical wall which suddenly changes temperature
%! ## (Illingworth 1950; Schetz & Eichhorn 1962).
%!
%! x = linspace (-3, 3, 601)';
%! xlabel ("similarity coordinate, xi = 2 sqrt (kappa t)")
%! [va, vapp, theta] = equalizing (x, 0.7);
%! vw = equalizing (x, 7);
%! vinf = equalizing (x, Inf);
%! plot (x, theta, "-;Theta;", x, va, "--;V (Pr=0.7);", x, vw, ".-;V (Pr=7.0);", x, vinf, "+-;V (Pr->Inf);")
%! [x, theta, va, vw, vinf]

%!demo
%!
%! ## Pr = 7 (temperature & its gradient, velocity & its curvature)
%!
%! x = linspace (-5, 5, 100);
%! [v, vpp, theta, thetap] = equalizing (x, 1);
%! plot (x, theta, ";theta;", x, thetap, ";theta';", x, v, ";v;", x, vpp, ";vpp;");

%!demo
%!
%! ## velocity profile for various Prandtl numbers
%!
%! x = linspace (-5, 5, 100);
%! [v1, v1pp, theta] = equalizing (x, 1);
%! [v10, v10pp] = equalizing (x, 10);
%! [v100, v100pp] = equalizing (x, 100);
%! [vinf, vinfpp] = equalizing (x, Inf);
%! plot (x, theta, ";theta;", x, v1, ";v (Pr=1);", x, v10, ";v (Pr=10);", x, v100, ";v (Pr=100);", x, vinf, ";v (Pr->Inf);");

%!demo
%!
%! ## velocity profile curvature for various Prandtl numbers
%!
%! x = linspace (-5, 5, 100);
%! [v1, v1pp] = equalizing (x, 1);
%! [v10, v10pp] = equalizing (x, 10);
%! [v100, v100pp] = equalizing (x, 100);
%! [vinf, vinfpp] = equalizing (x, Inf);
%! plot (x, v1pp, ";v (Pr=1);", x, v10pp, ";v (Pr=10);", x, v100pp, ";v (Pr=100);", x, vinfpp, "+-;v (Pr->Inf);");

%!demo
%!
%! ## velocity profile for Prandtl numbers around one
%!
%! x = linspace (-5, 5, 100);
%! [v9, v9pp, theta] = equalizing (x, 0.9);
%! [v1, v1pp] = equalizing (x, 1.0);
%! [v11, v11pp] = equalizing (x, 1.1);
%! plot (x, v9, ";v (Pr=0.9);", x, v1, ";v (Pr=1.0);", x, v11, ";v (Pr=1.1);");

%!demo
%!
%! ## velocity profile curvature for Prandtl numbers around one
%!
%! x = linspace (-5, 5, 100);
%! [v9, v9pp, theta] = equalizing (x, 0.9);
%! [v1, v1pp] = equalizing (x, 1.0);
%! [v11, v11pp] = equalizing (x, 1.1);
%! plot (x, v9pp, ";v'' (Pr=0.9);", x, v1pp, ";v'' (Pr=1.0);", x, v11pp, ";v'' (Pr=1.1);");
