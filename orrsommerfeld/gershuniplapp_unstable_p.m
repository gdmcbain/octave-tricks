## Copyright (C) 2007 G. D. McBain <gdmcbain@protonmail.com>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

## -*- texinfo -*-
## @deftypefn {Function File} {} gershuniplapp_unstable_p (@var{Rk}, @var{Pr}, @var{D}, @var{D2}, @var{D4}, @var{D2T}, @var{V}, @var{Vpp}, @var{Tp})
## Return 1 if the vertical natural convection boundary layer in an
## unstratified fluid is linearly unstable and zero otherwise.
## @seealso{gershuniplapp}
## @end deftypefn

## Author: G. D. McBain <gdmcbain@protonmail.com>
## Created: 2007-01-17
## Keywords: hydrodynamic stability, natural convection

function p = gershuniplapp_unstable_p (Rk, Pr, D, D2, D4, D2T, V, Vpp, Tp)
  
  if ((p = all (Rk>0)))		# first quadrant
    [L, M] = gershuniplapp (Rk(1), Pr, Rk(2), D, D2, D4, D2T, V, Vpp, Tp);
    p = any (imag (Rk(2) * eig (M\L)) > 0);
  endif

endfunction
