## Copyright (C) 2006, 2007 G. D. McBain <gdmcbain@protonmail.com>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

## -*- texinfo -*-
## @deftypefn {Function File} {} [@var{fpp0}, @var{q0}] = sparrowgregg_shooting (@var{l}, @var{fpp0}, @var{q0}, @var{Pr})
## Compute the missing initial conditions for the natural convection
## boundary layer on an evenly heated vertical wall in an isothermal
## fluid of Prandtl number @var{Pr} using the shooting method from the
## guessed (dimensionless) skin friction @var{fpp0} and heat flux
## @var{q0}. The boundary layer similarity equations are given by
## Sparrow & Gregg (1956).
##
## E. M. Sparrow and J. L. Gregg 1956 Laminar free convection from a
## vertical plate with a uniform surface heat flux.  @cite{Transactions
## of the ASME} @strong{78}:435--440.
## @seealso{sparrowgregg} 
## @end deftypefn

## Author: G. D. McBain <geordie.mcbain@aeromech.usyd.edu.au>
## Created: 2006-12-05
## Keywords: natural convection, boundary layer, similarity, shooting method

function [fpp0, q0] = sparrowgregg_shooting (l, Pr, fpp00, q00)
  h = fsolve (@ (h) sparrowgregg_shooting_r (h, [0; l], Pr), [fpp00, q00]);
  fpp0 = h(1); q0 = h(2);
endfunction

function r = sparrowgregg_shooting_r (h, x, Pr)
  [f, theta] = sparrowgregg (x, Pr, h(1), h(2));
  r = [f(end,2), theta(end,1)];
endfunction

%!test
%!
%! ## Pr = 0.733; fpp0 = 0.80893; h0 = -1.47981; 
%!
%! Pr = 0.733; fpp0 = 0.80893; h0 = -1.47981; 
%! l = 20;
%! [fpp01, h01] = sparrowgregg_shooting (l, Pr, fpp0, h0);
%! assert ([fpp01, h01], [fpp0, h0], 5e-6)

%!test
%!
%! ## Pr = 6.7; fpp0 = 0.35633; theta0 = -0.84170;
%!
%! Pr = 6.7; fpp0 = 0.35633; h0 = -0.84170;
%! l = 20;
%! [fpp01, h01] = sparrowgregg_shooting (l, Pr, fpp0, h0);
%! assert ([fpp01, h01], [fpp0, h0], 5e-6)

%!demo
%!
%! ## Pr = 10.^(-1:2) (Sparrow & Gregg 1956, app. B)
%!
%! data = [0.1, -2.7507, 1.6434; 1, -1.3574, 0.72196; 10, -0.76746, 0.30639; 100, -0.46566, 0.12620];
%! printf ("%20s%5s|  %20s\n\n", "present", "", "Sparrow & Gregg (1956)");
%! printf ("%6s %7s  %7s|  %7s  %7s\n\n", "Pr", "theta (0)", "f'' (0)", "theta (0)", "f'' (0)");
%! for i = 1:(rows (data))
%!   [fpp0, h0] = sparrowgregg_shooting (12, data(i,1), data(i,3), data(i,2));
%!   printf ("%6.2f  %7.4f  %7.4f |   %7.4f  %7.4f\n", data(i,1), h0, fpp0, data(i,2:3));
%! endfor

%!demo
%!
%! ## numerical continuation to get to Pr = 0.733
%! ## fpp0 = 0.80893; theta0 = -1.47981;
%!
%! printf ("%6s  %7s  %7s\n\n", "Pr", "theta (0)", "f'' (0)");
%! fpp0 = 0.72196; h0 = -1.3574; 
%! l = 10;
%! for Pr = linspace (1, 0.733, 4)
%!   [fpp0, h0] = sparrowgregg_shooting (l, Pr, fpp0, h0);
%!   printf ("%6.3f  %7.5f  %7.5f\n", Pr, h0, fpp0);
%! endfor
%! [fpp0, h0] = sparrowgregg_shooting (l, Pr = 0.733, fpp0 = 0.80893, h0 = -1.47981);
%! printf ("\n%6.3f  %7.5f  %7.5f\n", Pr, h0, fpp0);
%! x = linspace (0, l, 99);
%! [f, h] = sparrowgregg (x, Pr, fpp0, h0);
%! plot (x, f(:,2), ";V;", x, h(:,1), ";Theta;");

%!demo
%!
%! ## ...and to Pr = 6.7
%! ## fpp0 = 0.35633; theta0 = -0.84170;
%!
%! printf ("%6s  %7s  %7s\n\n", "Pr", "theta (0)", "f'' (0)");
%! fpp0 = 0.72196; h0 = -1.3574; 
%! l = 10;
%! for Pr = linspace (1, 6.7, 3)
%!   [fpp0, h0] = sparrowgregg_shooting (l, Pr, fpp0, h0);
%!   printf ("%6.3f  %7.5f  %7.5f\n", Pr, h0, fpp0);
%! endfor
%! 
%! fpp0 = 0.35633; theta0 = -0.84170;
%! [fpp0, h0] = sparrowgregg_shooting (l, Pr, fpp0, h0);
%! printf ("\n%6.3f  %7.5f  %7.5f\n", Pr, h0, fpp0);
%! x = linspace (0, l, 99);
%! [f, h] = sparrowgregg (x, Pr, fpp0, h0);
%! plot (x, f(:,2), ";V;", x, h(:,1), ";Theta;");
