## Copyright (C) 2006, 2007 G. D. McBain <gdmcbain@protonmail.com>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

## -*- texinfo -*-
## @deftypefn {Function File} {} [@var{V}, @var{Vpp}, @var{Tp}, @var{D}] = gershuniplapp_equalizing_single_init_fd (@var{x}, @var{Pr})
## Set up the linear stability problem for the natural convection
## boundary layer in an initially stagnant isothermal fluid  of Prandtl
## number @var{Pr} against an infinite vertical wall subject to a sudden
## change in temperature.  The grid @var{x} should not have a point at
## the wall @code{x=0}.
##
## Compute the base solution profiles @var{V}, @var{Vpp}, and @var{Tp}
## using @code{equalizing (@var{x}, @var{Pr})}.
##
## Compute the first-, second-, and fourth-order differentiation
## matrices, storing them as elements of the cell array @var{D}.  Uses
## standard finite differences.  The far-field boundary conditions are
## handled by domain-truncation.  (@strong{TO DO:} implement asymptotic
## boundary conditions.)
## @seealso{finidiff_matrix_dirichlet}
## @seealso{gershuniplapp, gershuniplapp_unstable_p}
## @seealso{gershuniplapp_equalizing_single_init_fd}
## @end deftypefn

## Author: G. D. McBain <gdmcbain@protonmail.com>
## Created: 2006-12-08
## Keywords: natural convection, linear stability

function [V, Vpp, Tp, D] = gershuniplapp_equalizing_single_init_fd (x, Pr)

  n = length (x);
  [V, Vpp, T, Tp] = equalizing (x, Pr);

  ## TODO: replace the far-field Dirichlet approximation with correct
  ## asymptotic boundary conditions
  
  for q = [1, 2, 4]
    D{q} = finidiff_matrix_dirichlet (x, q, 0, x(n) + (x(n)-x(n-1)));
  endfor  

endfunction
