## Copyright (C) 2007 G. D. McBain <gdmcbain@protonmail.com>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

## -*- texinfo -*-
## @deftypefn {Function File} {} [@var{EM}, @var{DM}, @var{B}, @var{M}] = gershuniplapp_energetics (@var{k}, @var{psi}, @var{theta}, @var{w}, @var{D}, @var{D2}, @var{D4}, @var{V})
## Compute the four energy transfer rate integrals for vertical natural
## convection boundary layer disturbances discussed by Nachtsheim
## (1963), Gill & Davey (1969), and Iyer (1973): the rate of change of
## disturbance kinetic energy @var{EM}; the rate of viscous dissipation
## @var{DM}; the rate of gain of disturbance kinetic energy through the
## action of buoyancy; and the rate of transfer of kinetic energy from
## the mean flow @var{M}.
##
## Required inputs are: the wave number @var{k}; the disturbance shape
## functions for streamfunction @var{psi} and temperature @var{theta};
## the quadrature weights @var{w} (which may assume that the integrand
## vanishes at either end of the domain), the first, second, and
## fourth-order differentiation matrices, @var{D}, @var{D2}, and
## @var{D4}; and the base velocity profile @var{V}.
##
## @strong{Background theory}
##
## The word @dfn{energetics} is defined by the Concise Oxford English
## Dictionary (10th edn) as `the properties or behaviour of something in
## terms of energy'.  I believe the first to apply these ideas to the
## linear stability of vertical natural convection was Nachtsheim
## (1963). Later Gill & Davey (1969) and Iyer (1973) concentrated on the
## integrals of the power transfers over the domain and a wavelength.
## This is what we compute here, using essentially their terminology.
##
## The momentum perturbation equation we use in @command{gershuniplapp}
## is
## @example
##
## @group
##   (F'''' - 2k^2 F'' + k^4 F - q') / i k Gr
##
##   +  (V (k^2-D^2) + V'') F = c (k^2 - D^2) F .
## @end group
## @end example
##
## First multiply by i k Gr and take the inner product with F
## @example
##
## @group
##
##   DM + <F', q>
##
##   + i k Gr (<|F|^2, k^2 V + V''> + <|F'|^2, V> + <F, V' F'>)
##
##   = i k Gr c EM
## @end group
## @end example
##
## where
## @example
##
## @group
##   EM = <F, (k^2-D^2) F>
##
##   DM = <F, F'''' - k^2*F'' + k^4*F>
## @end group
## @end example
## which are real since they can be written
## @example
##
## @group
##   EM = || k F ||^2 + || F' ||^2
##
##   DM = || k^2 F - F'' ||^2 .
## @end group
## @end example
## This motivates taking the real part (noting real (1i*z) == - imag (z))
## @example
##
##   DM - B - Gr M = - Gr EM imag (k c)
## @end example
## where
## @example
##
## @group
##   B = - real <F', q>
##
##   M = imag (k <F, V' F'>)
## @end group
## @end example
## This form for @var{B} (as opposed to the equivalent real <F, q'>) doesn't
## require a differentiation matrix for the temperature, which is
## convenient for problems where the heat flux is specified.
##
## @strong{Mechanically versus buoyancy-driven modes}
##
## Iyer (1973, at p. 64) quantified the idea of mechanically and
## buoyancy-driven modes simply by the criterion @var{B} < Gr * @var{M}. 
##
## A. E. Gill & A. Davey 1969 Instabilities of a buoyancy driven system.
## @cite{Journal of Fluid Mechanics}.  @strong{35}:775--798.
##
## P. A. Iyer 1973 Instabilities in buoyancy driven buoyancy flows in a
## stably stratified medium.  @cite{Boundary Layer Meteorology}
## @strong{5}:53--66.
##
## P. R. Nachtsheim 1963 Stability of free-convection boundary-layer
## flows. NASA Technical Note D-2089.
##
## @end deftypefn

## Author: G. D. McBain <gdmcbain@protonmail.com>
## Created: 2007-03-14
## Keywords: natural convection, linear stability, energetics

function [EM, DM, B, M] = gershuniplapp_energetics (k, psi, theta, w, \
						    D, D2, D4, V)
  ip = @ (x, y) (x' .* w) * y;	# inner product
  EM = ip (psi, k^2 * psi - D2 * psi);
  DM = ip (psi, D4*psi - k^2*(D2*psi) + k^4*psi);
  B = - real (ip (D*psi, theta));
  M = imag (k * ip (psi, (D*V) .* (D*psi)));
endfunction			
