## Copyright (C) 2006, 2007 G. D. McBain -*- octave -*-
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

## -*- texinfo -*-
## @deftypefn {Function File} {}  [@var{V}, @var{Vpp}, @var{Tp}, @var{D1}, @var{D2M}, @var{D2T}, @var{D4}] = gershuniplapp_equalizing_init_fd (@var{x}, @var{Pr})
## Set up the linear stability problem for the natural convection
## boundary layer on a thin conducting vertical wall dividing two bodies
## of thermally equalizing fluid of Prandtl number @var{Pr} using a
## (possibly uneven) grid @var{x} which doesn't have a point at the wall
## @var{x=0}.  
##
## Compute the base solution profiles @var{V}, @var{Vpp}, and @var{Tp}, 
## using @code{equalizing}.
##
## Compute the differentiation matrices: @var{D1}, first order for
## temperature; @var{D2M} and @var{D2T}, second order for
## stream-function and temperature, respectively, and @var{D4}, fourth
## order for stream-function.  Uses standard finite differences. The
## far-field boundary conditions are handled by domain-truncation.
##
## The boundary conditions are impermeability and no-slip and continuity
## of temperature and heat flux at the wall
##
## @example 
## @group
## f (0) = f' (0) = 0
##
## q (0-) = q (0+)
## q' (0-) = q' (0+)
## @end group
## @end example
##
## and that the disturbance decay in the far-field
##
## @example
## f, q ~ 0  (x -> Inf).
## @end example
##
## (@strong{TO DO:} implement asymptotic boundary conditions.)
## @seealso{equalizing}
## @seealso{finidiff_matrix_dirichlet, finidiff_matrix2_mod}
## @seealso{gershuniplapp, gershuniplapp_unstable_p}
## @end deftypefn

## Author: G. D. McBain <gdmcbain@protonmail.com>
## Created: 2006-12-08
## Keywords: natural convection, linear stability

function [V, Vpp, Tp, D1, D2M, D2T, D4] = gershuniplapp_equalizing_init_fd (x, Pr)
  
  n = length (x);
  [V, Vpp, T, Tp] = equalizing (x, Pr);

  ## TODO: replace the far-field Dirichlet approximation with correct
  ## asymptotic boundary conditions

  xminf = x(1) - (x(2) - x(1));	# far left boundary
  xinf = x(n) + (x(n) - x(n-1)); # far right boundary

  left = x < 0;

  ## For the second-order derivative for the stream-function
  ## perturbation, impose impermeability in the far field and on either
  ## side of the wall.
  D2l = finidiff_matrix_dirichlet (x(left), 2, xminf, 0);
  D2r = finidiff_matrix_dirichlet (x(~left), 2, 0, xinf);
  D2M = [D2l,                     (zeros (rows (D2l),
					  columns (D2r) ));
	 (zeros (rows (D2r),
		 columns (D2l) )), D2r ];

  ## For the fourth-order derivative for the stream-function
  ## perturbation, impose impermeability and no-slip in the far field
  ## and on either side of the wall.
  D4l = finidiff_matrix_dirichlet (x(left), 4, xminf, 0);
  D4r = finidiff_matrix_dirichlet (x(~left), 4, 0, xinf);
  D4 = [D4l,                     (zeros (rows (D4l),
					 columns (D4r)));
	(zeros (rows (D4r),
		columns (D4l))), D4r ];

  ## For the temperature perturbation, impose: zero at far left and
  ## right, and continuous function and derivative across the wall
  ## (x=0).

  D1 = finidiff_matrix_dirichlet (x, 1, xminf, xinf);
  D1 = finidiff_interface (D1, x, 0, 1, 1, 1);

  D2T = finidiff_matrix_dirichlet (x, 2, xminf, xinf);
  D2T = finidiff_interface (D2T, x, 0, 2, 1, 1);

endfunction


%!test
%!
%! ## stable and unstable points for Pr = 7
%!
%! Pr = 7;
%!
%! x = grid_cellcentred (0.08, 1.1, 47);
%! x = vertcat (flipud (-x), x);
%!
%! [V, Vpp, Tp, D, D2, D2T, D4] = gershuniplapp_equalizing_init_fd (x, Pr);
%! Ra = 2e3;
%!
%! ps = gershuniplapp_unstable_p ([Ra; 0.0], Pr, D, D2, D4, D2T, V, Vpp, Tp);
%! pu = gershuniplapp_unstable_p ([Ra; 0.4], Pr, D, D2, D4, D2T, V, Vpp, Tp);
%! assert (~ps, pu);

%!demo
%!
%! ## Convergence of the amplification rate (takes about 20 s)
%! 
%! ## for Pr = 7, Ra1 = 240, alpha = 0.35.
%!
%! assert (exist ("richardson_extrapolate", "file"));
%! Pr = 7
%! alpha = 0.35
%! Ra = 240
%! amp_exact = 0.0014923;
%! h = 0.2
%! factor = 0.5
%! printf ("% 11s\t%4s\t% 11s\t%4s\t% 11s\t\n", "", "xn>16", "", "xn>32", "");
%! printf ("% 11s\t%4s\t% 11s\t%4s\t% 11s\t%s\n\n", "h", "n", "amp", "n", "amp", "error");
%! hh = aa = [];
%! do 
%!   hh = [hh; h];
%!   r = 1 + 1.25 * h;
%!   printf ("% 11.4e\t", h);
%!   for xinf = [16, 32]
%!     n = ceil (grid_cellcentred_n (h, r, xinf));
%!     x = grid_cellcentred (h, r, n);
%!     x = vertcat (flipud (-x), x);
%!     [V, Vpp, Tp, D, D2, D2T, D4] = gershuniplapp_equalizing_init_fd (x, Pr);
%!     [L, M] = gershuniplapp (Ra/Pr, Pr, alpha, D, D2, D4, D2T, V, Vpp, Tp);
%!     amp = max (imag (alpha * eig (M\L)));
%!     aa = [aa; amp];
%!     printf ("%4d\t% 11.4e\t", n, amp);
%!   endfor
%!   h *= factor;
%!   printf ("%e\n", amp - amp_exact);
%! until h < 1.5e-2
%! amp_ext16 = richardson_extrapolate (aa(1:2:end), hh, 2);
%! amp_ext = richardson_extrapolate (aa(2:2:end), hh, 2);
%! amp_exact = amp_ext(end)
%! amp_uncertainty = abs (diff (amp_ext(end-1:end)))
%! amp_err = abs (aa - amp_exact);
%! title ("Convergence of the amplification rate for Pr = 7, Ra1 = 240, and alpha = 0.35")
%! xlabel ("inner cell width, h")
%! ylabel ("absolute error in imag (alpha c)")
%! loglog (hh, amp_err(1:2:end), "*;n=16;", hh, amp_err(2:2:end), "+;n=32;");

%!demo
%!
%! ## Grid-independence of amplification (takes about five seconds)
%!
%! ## For the particular case Ra1 = 240, Pr = 7, alpha = 0.35,
%! ## with a grid h = 0.16 and r = 1 + 1.25*h, 
%! ## showing that n > 17, 20 gives x(n) > 16, 32
%! ## and that either of these gives an error in the amplification
%! ## than is less than 1e-3.
%!
%! Pr = 7
%! alpha = 0.35
%! Ra = 240
%! amp_exact = 0.001491;
%! h = 0.16
%! r = 1 + 1.25*h
%! printf ("\n%2s\t%4s\t%8s\t%9s\n\n", "n", "x(n)", "amp", "error");
%! for n = 9:21
%!   x = grid_cellcentred (h, r, n);
%!   x = vertcat (flipud (-x), x);
%!   [V, Vpp, Tp, D, D2, D2T, D4] = gershuniplapp_equalizing_init_fd (x, Pr);
%!   [L, M] = gershuniplapp (Ra/Pr, Pr, alpha, D, D2, D4, D2T, V, Vpp, Tp);
%!   amp = max (imag (alpha * eig (M\L)));
%!   amp_error = amp - amp_exact;
%!   printf ("%2d\t%4.1f\t% 8.6f\t% 9.6f\n", n, x(end), amp, amp_error);
%! endfor

%!demo
%!
%! ## stability margin for Pr = 7 (takes about a minute)
%!
%! assert (exist ("askirt", "file"));
%! Pr = 7
%! x = grid_cellcentred (0.08, 1.1, 24);
%! x = vertcat (flipud (-x), x);
%! [V, Vpp, Tp, D, D2, D2T, D4] = gershuniplapp_equalizing_init_fd (x, Pr);
%! Ra = 2e3;
%! printf ("\n%10s   %10s\n\n", "Ra", "alpha");
%! m = askirt ([Ra; 0], [Ra; 0.4], @ (R) gershuniplapp_unstable_p ([R(1)/Pr; R(2)], Pr, D, D2, D4, D2T, V, Vpp, Tp), [1; 1e-3], 4, 1.2, -[Inf; Inf], [Ra; Inf], 1e4, true )';
%! plot (m(:,1), m(:,2)) 
%! title ("linear stabilty margin for Pr = 7")
%! xlabel ("Rayleigh number, Ra1")
%! ylabel ("reduced wave number, alpha")
%! axis ([0, 2000, 0, 1.5])

%!demo
%!
%! ## finding marginal Rayleigh numbers (takes about a minute)
%! ## The critical one seems to be near alpha = 0.33.
%!
%! assert (exist ("gershuniplapp_marginal", "file"))
%! Pr = 7;
%! x = grid_cellcentred (0.08, 1.1, 47); 
%! x = vertcat (flipud (-x), x);
%! [V, V2, Tp, D, D2, D2T, D4] = gershuniplapp_equalizing_init_fd (x, Pr);
%! printf ("%5s\t%7s\n\n", "alpha", "Ra");
%! for a = 0.27:0.01:0.37
%!   Ra = Pr * gershuniplapp_marginal (a, 170/Pr, 190/Pr, 1e-2/Pr, Pr, D, D2, D4, D2T, V, V2, Tp);
%!   printf ("%5.3f\t%7.3f\n", a, Ra);
%! endfor

%!demo
%!
%! ## the critical Rayleigh number at Pr = 7 (takes about a minute)
%! ## Turns out to be Ra = 183.06 at alpha = 0.33090
%!
%! assert (exist ("gershuniplapp_marginal", "file"))
%! assert (exist ("golden", "file"))
%! Pr = 7;
%! x = grid_cellcentred (0.08, 1.1, 47); 
%! x = vertcat (flipud (-x), x);
%! [V, Vpp, Tp, D1, D2M, D2T, D4] = gershuniplapp_equalizing_init_fd (x, Pr);
%! [alpha, Ra] = golden (@ (a) Pr * gershuniplapp_marginal (a, 180/Pr, 185/Pr, 1e-2/Pr, Pr, D1, D2M, D4, D2T, V, Vpp, Tp), 0.3, 0.4, 0.001)

%!demo
%!
%! ## the wave speed spectrum (takes a couple of seconds)
%!
%! Pr = 7; 
%! Ra = 240; 
%! alpha = 0.35;
%!
%! x = grid_cellcentred (0.08, 1.1, 47);
%! x = vertcat (flipud(-x), x);
%!
%! [V, V2, Tp, D, D2, D2T, D4] = gershuniplapp_equalizing_init_fd (x, Pr);
%! [L, M] = gershuniplapp (240/Pr, Pr, 0.35, D, D2, D4, D2T, V, V2, Tp);
%! c = eig (M\L);
%!
%! plot (real (c), imag (c), "*");
%! title ("wave speed spectrum")
%! xlabel ("real (c)")
%! ylabel ("imag (c)")
%! axis ([-0.15, 0.15, -50, 10])
%! grid ("on");

%!demo
%! 
%! ## the disturbance isotherms (takes a couple of seconds)
%!
%! Pr = 7;
%! Ra = 240;
%! alpha = 0.35;
%!
%! n = 47;
%! x = grid_cellcentred (0.08, 1.1, n);
%! x = vertcat (flipud(-x), x);
%!
%! [V, V2, Tp, D, D2, D2T, D4] = gershuniplapp_equalizing_init_fd (x, Pr);
%! [L, M] = gershuniplapp (Ra/Pr, Pr, alpha, D, D2, D4, D2T, V, V2, Tp);
%! [psitheta, c] = eig (M\L);
%! [sorted, cind] = sort (-imag (diag (c)));
%! y = linspace (0, 2*pi/alpha, 256)';
%! theta = psitheta((1:2*n)+2*n,cind(1));
%! contour (x, y, real (exp (1i * alpha * y) * theta.'))
%! grid ("on")
%! axis ([-10, 10, 0, 2*pi/alpha], "equal")
%! xlabel ("x")
%! ylabel ("y")
%! title ("isotherms of least stable mode at Pr = 7, Ra = 240, alpha = 0.35")

%!demo
%! 
%! ## the disturbance stream-lines (takes a couple of seconds)
%!
%! Pr = 7;
%! Ra = 240;
%! alpha = 0.35;
%!
%! n = 47;
%! x = grid_cellcentred (0.08, 1.1, n);
%! x = vertcat (flipud(-x), x);
%!
%! [V, V2, Tp, D, D2, D2T, D4] = gershuniplapp_equalizing_init_fd (x, Pr);
%! [L, M] = gershuniplapp (Ra/Pr, Pr, alpha, D, D2, D4, D2T, V, V2, Tp);
%! [psitheta, c] = eig (M\L);
%! [sorted, cind] = sort (-imag (diag (c)));
%! y = linspace (0, 2*pi/alpha, 256)';
%! psi = psitheta(1:2*n,cind(1));
%! contour (x, y, real (exp (1i * alpha * y) * psi'));
%! grid ("on");
%! axis ([-10, 10, 0, 2*pi/alpha], "equal");
%! xlabel ("x")
%! ylabel ("y")
%! title ("stream-lines of least stable mode at Pr = 7, Ra = 240, alpha = 0.35")

%!demo
%!
%! ## stability margins double- and single-sided cases (about 8 min)
%!
%! assert (exist ("askirt", "file"));
%! assert (exist ("gershuniplapp_equalizing_single_init_fd", "file"));
%! Pr = 7
%! x = grid_cellcentred (0.08, 1.1, 47);
%! [V, Vpp, Tp, D] = gershuniplapp_equalizing_single_init_fd (x, Pr);
%! Ra = 2e3;
%! printf ("\n%10s   %10s\n\n", "Ra", "alpha");
%! m1 = askirt ([Ra; 0], [Ra; 0.4], @ (R) gershuniplapp_unstable_p ([R(1)/Pr; R(2)], Pr, D{1}, D{2}, D{4}, D{2}, V, Vpp, Tp), [1; 1e-3], 4, 1.2, -[Inf; Inf], [Ra; Inf], 1e4, true )';
%!
%! x = vertcat (flipud (-x), x);
%! [V, Vpp, Tp, D, D2, D2T, D4] = gershuniplapp_equalizing_init_fd (x, Pr);
%! Ra = 2e3;
%! m2 = askirt ([Ra; 0], [Ra; 0.4], @ (R) gershuniplapp_unstable_p ([R(1)/Pr; R(2)], Pr, D, D2, D4, D2T, V, Vpp, Tp), [1; 1e-3], 4, 1.2, -[Inf; Inf], [Ra; Inf], 1e4, true )';
%! plot (m2(:,1), m2(:,2), ";double;", m1(:,1), m1(:,2), ";single;")
%! axis ([0, 2000, 0. 1.5]);
%! xlabel ("Rayleigh number, Ra1")
%! ylabel ("reduced wave number, alpha")
%! title ("linear stability margins for double- and single-sided cases (Pr = 7)")

%!demo
%!
%! ## relative growth rate (takes about 8 s)
%!
%! Pr = 7; 
%! alpha = 0.35;
%! x = grid_cellcentred (0.08, 1.1, 47); 
%! x = vertcat (flipud (-x), x);
%! [V, Vpp, Tp, D, D2, D2T, D4] = gershuniplapp_equalizing_init_fd (x, Pr);
%! printf ("%4s\t%11s\t%11s\n\n", "Ra", "amplification", "ratio");
%! Ra = 150:50:1000;
%! ratio = zeros (size (Ra));
%! for i = 1:(length (Ra))
%!   [L, M] = gershuniplapp (Ra(i)/Pr, Pr, alpha, D, D2, D4, D2T, V, Vpp, Tp);
%!   amp = alpha * max (imag (eig (M\L)));
%!   ratio(i) = 0.5 * Ra(i) * amp;
%!   printf ("%4d\t% 11.4e\t% 11.4e\n", Ra(i), amp, ratio(i));
%! endfor
%! plot (Ra, ratio, "+")
%! axis ([0, 1000, 0, 3])
%! xlabel ("Rayleigh number, Ra1")
%! ylabel ("timescale ratio, Ra1 imag (alpha c) / 2")
%! title ("growth-time ratio of base flow to disturbance (Pr = 7, alpha = 0.35)")
