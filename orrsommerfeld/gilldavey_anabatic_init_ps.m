## Copyright (C) 2007 G. D. McBain <gdmcbain@protonmail.com>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

## -*- texinfo -*- @deftypefn {Function File} {} [@var{V}, @var{Vpp}, @var{Tp}, @var{D}, @var{D2T}, @var{x}] = gilldavey_anabatic_init_ps (@var{n}, @var{Pr}, @var{thetabc}) 

## Set up the problem for the the linear stability of the natural
## convection boundary layer on an heated vertical wall in a stratified
## fluid of Prandtl number @var{Pr}; either for fixed heat flux
## "Neumann" or excess temperature "Dirichlet" thermal boundary
## condition @var{thetabc}.  Only the first character @var{thetabc} is
## inspected and that without regard to case. (The "Neumann" case is not
## implemented yet.)  Use a grid of @var{x} of @var{n} points, obtained
## by algebraically mapping the Gauss points from the segment @var{-1 <
## t < 1} to the ray @var{0 < @var{x} < Inf}, using
## @code{map_ray_algebraic}.
##
## Obtain the base solution profiles @var{V}, @var{Vpp}, and @var{Tp}
## from @command{anabatic} (see which for their interpretation).
##
## Return the first-, second-, and fourth-order differentiation matrices
## with Dirichlet boundary conditions in the cell array @var{D}, and a
## second-order differentiation matrix @var{D2T} with appropriate
## thermal boundary conditions.
##
## The boundary conditions at infinity are treated implicitly by the
## algebraic mapping from the segment.  The Dirichlet conditions at the
## wall are treated by multiplying the polynomial basis functions on the
## segment by a factor to coerce the required number of zeros: one for f
## (0) = 0 and two for f (0) = f' (0) = 0.  Note that
##
## Accurate solutions of the linear stability equations have been
## computed by Gill & Davey (1969) for the Dirichlet case and McBain &
## Armfield (2004) and McBain, Armfield, & Desrayaud (in press) for the
## Neumann case.
##
## A. E. Gill & A. Davey 1969 Instabilities of a buoyancy driven system.
## @cite{Journal of Fluid Mechanics} @strong{35}:775--798.
##
## G. D. McBain & S. W. Armfield 2004 Linear stability of natural
## convection on an evenly heated vertical wall. In M. Behnia, W. Lin, &
## G. D. McBain (eds) @cite{Proceedings of the Fifteenth Australasian
## Fluid Mechanics Conference}, The University of Sydney, Paper
## AFMC00196.
##
## G. D. McBain, S. W. Armfield, &  G. Desrayaud (in press) Instability
## of the buoyancy layer on an evenly heated vertical wall.
## @cite{Journal of Fluid Mechanics}.
## @seealso{map_ray_algebraic, coerce_differentiation_matrices, map_differentiation_matrices}
## @end deftypefn

## Author: G. D. McBain <gdmcbain@protonmail.com>
## Created: 2007-05-28
## Keywords: natural convection, linear stability

function [V, Vpp, Tp, D, D2T, x] = gilldavey_anabatic_init_ps (n, l, Pr, thetabc)
  t = orthopolyquad ("legendre", n);
  Dt = differentiation_matrices (t, 4);
  [x, tau] = map_ray_algebraic (t, l, 4);

  [V, Vpp, T, Tp] = anabatic (x);

  D1 = map_differentiation_matrices (Dt, tau);

  alpha = 1 + t;
  beta = postpad (1 ./(1+t), 4, 0, 2);
  Dt2 = coerce_differentiation_matrices (Dt, alpha, beta);
  D = map_differentiation_matrices (Dt2, tau);

  alpha = (1 + t).^2;
  beta = postpad ([2 ./ (1+t), 2 ./ (1+t).^2], 4, 0, 2);
  Dt4 = coerce_differentiation_matrices (Dt, alpha, beta);
  D4 = map_differentiation_matrices (Dt4, tau);

  D{1} = D1{1};			# no wall condition
  D{4} = D4{4};			# double-zero at wall

  switch (toupper (thetabc(1)))	# fix temperature or its gradient?
    case ("D")			# Dirichlet
      D2T = D{2};
    case ("N")			# Neumann
      error ("Neumann case not implemented yet")
    otherwise
      print_usage ();		
  endswitch

endfunction

%!demo
%!
%! ## the base solution
%!
%! [V, Vpp, Tp, D, D2T, x] = gilldavey_anabatic_init_ps (64, 1.0, NaN, "D");
%! plot (x, V, ";V;", x, Vpp, ";V'';", x, Tp, ";T';");
%! axis ([0, 5, -2, 0.5])
%! xlabel ("x")
%! ylabel ("V, V'', T'")
%! title ("the base solution anabatic wind of Gill & Davey (1969)")

%!demo
%!
%! ## the critical mode at Pr = 0.72, Dirichlet thermal b.c.
%!
%! Pr = 0.72; 
%! Re = 109; 
%! kappa = 0.502; 
%!
%! c1GD = 0.200; # (Gill & Davey 1969, table 1)
%! printf ("%3s\t%13s    %s\n\n", "n", "uppermost c", "error"); 
%! for n = 2 .^ (2:7)
%!   [V, Vpp, Tp, D, D2T] = gilldavey_anabatic_init_ps (n, 1.0, Pr, "D");
%!   [L, M] = gilldavey (Re, Pr, kappa, D{1}, D{2}, D{4}, D2T, V, Vpp, Tp);
%!   c = eig (M\L);
%!   [rc1, ic1] = max (imag (c));
%!   c1 = c(ic1);
%!   err(n) = c1 - c1GD;
%!   printf ("%3d\t%- 6.3f %+6.3fi % .1e\n", n, real (c1), imag (c1), err(n)); 
%! endfor

%!demo
%!
%! ## stability margin for Pr = 2
%! ## cf. figure 2 of Gill & Davey (1969)
%!
%! Pr = 2;
%! Re = 400;
%!
%! n = 64; l = 1.0;
%! [V, Vpp, Tp, D, D2T] = gilldavey_anabatic_init_ps (n, l, Pr, "D");
%!
%! printf (" %9s\t%9s\n\n", "R", "alpha");
%! m = askirt ([Re; 0.01], [Re; 0.4], @ (R) gilldavey_unstable_p (R, Pr, D{1}, D{2}, D{4}, D2T, V, Vpp, Tp), [1, 1e-3], 4, 1.2, -[Inf; Inf], [Re; 1], 1e4, true);
%! title ("Neutral stabilty curve for Pr = 2 (after Gill & Davey 1969, figure 2)")
%! xlabel ("Reynolds number, R")
%! ylabel ("reduced wave number, alpha")
%! axis ([0, 400, 0, 1])
%! plot (m(:,1), m(:,2)) 
