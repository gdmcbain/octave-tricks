## Copyright (C) 2007 G. D. McBain <gdmcbain@protonmail.com>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or (at
## your option) any later version.
##
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301USA

## -*- texinfo -*-
## @deftypefn {Function File} {} [@var{V}, @var{Vpp}, @var{Tp}, @var{D}] = gershuniplapp_gershuni_init_ps (@var{x})
## Set up for the linear stability of the steady one-dimensional natural
## convection in a slot between infinite parallel vertical walls at
## different temperatures; i.e. for the abscissae @var{x} on the segment
## @code{-0.5 < @var{x} < 0.5}, compute the vertical velocity @var{V},
## its curvature @var{Vpp}, the horizontal temperature gradient
## @var{Tp}, and a cell array @var{D} of first-, second-, and
## fourth-order pseudospectral differentiation matrices with Dirichlet
## conditions; i.e.
##
## @example
## f (-1/2) = f (1/2) = 0 
## @end example
##
## for orders one and two and
##
## @example
## f (-1/2) = f (1/2) = f' (-1/2) = f' (1/2) = 0
## @end example
##
## for order four.
##
## The return values can be used to assemble and solve the linear stability
## equations using
##
## @example
## @group
## [L, M] = gershuniplapp (Gr, Pr, alpha, D@{1@}, D@{2@}, D@{4@}, D@{2@}, @var{V}, @var{Vpp}, @var{Tp});
## c = eig (M\L);
## @end group
## @end example
##
## The dimensionless base solution is (Waldmann 1939; Ruth 1979)
##
## @example
## @group
## V = (x^3 - x / 4) / 6
## T = -x
## @end group
## @end example
##
## Accurate solutions to these stability equations have been obtained by
## Ruth (1979) for @var{Pr} up to 10 and for all @var{Pr} by McBain &
## Armfield (2004).
##
## G. D. McBain & S. W. Armfield 2004 Natural convection in a vertical
## slot: accurate solution of the linear stability equations.  @cite{The
## ANZIAM Journal} @strong{45}(E):C92--C105.
##
## D. W. Ruth 1979 On the Transition to Transverse Rolls in an Infinite
## Vertical Fluid Layer---A Power Series Solution.  @cite{International
## Journal of Heat and Mass Transfer} @strong{22}:1199--1208.
##
## L. Waldmann 1939 Zur Theorie des Gastrennungsverfahrens von Clusius
## und Dickel (On the theory of the gas separation procedure of Clusius
## and Dickel, in German).  @cite{Die Naturwissenschaften}
## @strong{27}(14):230--231.
## @seealso{differentiation_matrices, coerce_differentiation_matrices}
## @end deftypefn

## Author: G. D. McBain <gdmcbain@protonmail.com>
## Created: 2007-05-29
## Keywords: natural convection, linear stability

function [V, Vpp, Tp, D] = gershuniplapp_gershuni_init_ps (x)

  n = length (x);

  V = (x.^3 - x/4)/6;	       # (Ruth 1979)
  Vpp = x;
  Tp = - ones (n, 1);

  Q = 4;
  D0 = differentiation_matrices (x, Q);

  alpha = 1 - 4*x.^2;		# f (+/- 1/2) = 0
  beta = -8 * postpad (horzcat (x, ones (n, 1)) ./ repmat (alpha, [1, 2]), 
		       Q, 0, 2 );      
  D = coerce_differentiation_matrices (D0, alpha, beta);
  Dv = coerce_differentiation_matrices (D, alpha, beta);
  D{4} = Dv{4};

endfunction

%!demo  
%!
%! ## the marginal Grashof number for Pr = 1.0
%! ## at the (critical) wave number alpha = 2.808
%!
%! ## the answer is Gr = 7940.235 (Ruth 1979)
%!
%! x = 0.5 * orthopolyquad ("legendre", 64);
%! [V, Vpp, Tp, D] = gershuniplapp_gershuni_init_ps (x);
%!
%! Pr = 1.0;
%! alpha = 2.808;
%!  
%! printf ("%5s\t%s\t%s\n\n", "Gr", "c", "")
%! for Gr = 7936:7945
%!   [L, M] = gershuniplapp (Gr, Pr, alpha, D{1}, D{2}, D{4}, D{2}, V, Vpp, Tp);
%!   c = eig (M\L);
%!   [icm, ici] = max (imag (c));
%!   printf ("%5.1f\t%+ e\t%+ ei\n", Gr, real (c(ici)), imag (c(ici)));
%! endfor


