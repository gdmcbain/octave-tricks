## Copyright (C) 2006, 2007, 2017 G. D. McBain -*- octave -*-
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

## -*- texinfo -*-
## @deftypefn {Function File} {} [@var{L}, @var{M}] = orrsommerfeld (@var{Re}, @var{k}, @var{D2}, @var{D4}, @var{U}, @var{Upp})
## Compute the stiffness @var{L} and mass @var{M} matrices for the
## two-dimensional temporal Orr--Sommerfeld equation at Reynolds number
## @var{Re} and stream-wise wavenumber @var{k} using second- and
## fourth-order differentiation matrices @var{D2} and @var{D4} for a
## velocity profile @var{U} with curvature profile @var{Upp}.
## @end deftypefn

## Author: G. D. McBain <gdmcbain@protonmail.com>
## Created: 2006-03-21
## Keywords: hydrodynamic stability

function [L, M] = orrsommerfeld (Re, k, D2, D4, U, Upp)
  n = length (U);
  I = speye (n);
  M = k^2*I - D2;
  Lv = k^4*I - 2*k^2*D2 + D4; # like M*M, but with no-slip conditions
  Li = diag (U) * M + spdiag (Upp);
  L = Lv / (1i * k * Re) + Li;
endfunction

## For demonstrations, see orrsommerfeld_poiseuille_init_ps.m and
## orrsommerfeld_poiseuille_init_fd.m
