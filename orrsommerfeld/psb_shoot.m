## Copyright (C) 2006, 2007 G. D. McBain <gdmcbain@protonmail.com>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

## -*- texinfo -*-
## @deftypefn {Function File} {} [@var{f}, @var{theta}] = psb_shoot (@var{x}, @var{fpp0}, @var{q0}, @var{Pr})
## Compute the natural convection boundary layer on an isothermal hot
## vertical wall in an isothermal cold fluid of Prandtl number @var{Pr},
## reporting the results at @var{x}, a vector of abscissae beginning
## from zero, using a shooting method from the guessed (dimensionless)
## skin friction @var{fpp0} and heat flux @var{q0}. Good guesses can be
## improved by the shooting method using @code{[@var{fpp0}, @var{q0}] =
## psb_shooting (max (@var{x}), @var{fpp0}, @var{q0}, @var{Pr})}.
##
## The similarity variables are nondimensionalized as by Schmidt &
## Beckmann (1930), Nachtsheim (1963), and Gebhart et al. (1988, p. 50),
## not by the different scheme of Ostrach (1953).  The equations are
## (Schmidt & Beckmann 1930, equations 26--29)
##
## @example
## @group
## f'''  + 3 f f'' - 2 f' ^2 + q = 0
##               f'' + 3 Pr f q' = 0
##
## f = f' = 0, q = 1   (x = 0)
## f' = f'' = q ~ 0    (x -> Inf)
## @end group
## @end example
##
## (actually the condition on the second derivative of the
## stream-function at infinity is redundant).
##
## B. Gebhart, Y. Jaluria, R. L. Mahajan, & B. Sammakia 1988
## @cite{Buoyancy-Induced Flows and Transport}, reference edition,
## Hemisphere, New York.  (See especially table 3.4.1, p. 53.)
##
## P. R. Nachtsheim 1963 Stability of free-convection boundary-layer
## flows. NASA Technical Note D-2089.
##
## S. Ostrach 1953 An analysis of laminar free-convection flow and heat
## transfer about a flat plate parallel to the direction of the
## generating body force. NACA Report 1111.
##
## E. Schmidt & W. Beckmann 1930 Das Temperatur- und
## Geschwindigkeitsfeld vor einer W@"arme abgebenden senkrechten Platte
## bei nat@"urlicher Konvektion.  II Die Versuche und ihre Ergebnisse.
## @cite{Techn. Mechan. u. Thermodynamik} @strong{1}(11):391--406.
## @end deftypefn

## Author: G. D. McBain
## Created: 2006-12-05
## Keywords: natural convection, boundary layer, shooting method

function [f, theta] = psb_shoot (x, fpp0, q0, Pr)

  assert (x(1), 0);

  ff = {(@ (f, x) psb_evolution (f, x, Pr)), (@ (f, x) psb_jac (f, x, Pr))};
  F = lsode (ff, [0; 0; fpp0; 1; q0], x);
  
  f = [F(:,1:3), 2*F(:,2).^2 - 3.*F(:,1).*F(:,3) - F(:,4)];
  theta = F(:,4:5);

endfunction

function fp = psb_evolution (f, x, Pr)
  fp = [f(2);			
	f(3);
	2*f(2)^2 - 3*f(1)*f(3) - f(4);
	f(5);
	-3*Pr*f(1)*f(5) ];
endfunction

function j = psb_jac (f, x, Pr)
  j = [0, 1, 0, 0, 0;			
	0, 0, 1, 0, 0;
	-3*f(3), 2*f(2), -3*f(1), -1, 0;
	0, 0, 0, 0, 1;
	-3*Pr*f(5), 0, 0, 0, -3*Pr*f(1) ];
endfunction

%!demo
%!
%! ## Schmidt & Beckmann (1930, table 4 & figure 24)
%!
%! [F, theta] = psb_shoot (x = (0:0.1:6)', 0.675, -0.508, Pr = 2.2/3);
%! printf ("% 4.1f % 7.4f % 7.4f % 7.4f % 7.4f % 6.3f\n", [x, F(:,1:3), theta]');
%! plot (x, F(:,1:3), ";F;", x, theta, "-.;theta;");


%!demo
%!
%! ## Nachtsheim (1963, table 1, p. 28)
%!
%! [F, H] = psb_shoot (x = (0:0.125:8.5)', 0.4547, -1.0408, 6.7);
%! printf ("%6.4f % 7.4f % 7.4f % 7.4f % 7.4f % 7.4f % 7.4f\n", [x, F, H]');

%!demo
%!
%! ## Nachtsheim (1963, figure 1, p. 29)
%!
%! f67 = psb_shoot (x = 0:0.125:8.5, 0.4547, -1.0408, 6.7);
%! f733 = psb_shoot (x, 0.675, -0.508, 0.733);
%! plot (x, f733(:,2), "-b;v (Pr = 0.733);", x, f67(:,2), "-.r;v (Pr = 6.7);")

%!demo
%!
%! ## Gebhart et al. (1988, figure 3.4.1 a, p. 52)
%!
%! data = [0.01, 0.9873, -0.0807; 0.1, 0.8591, -0.2301; 0.72, 0.6760, -0.5046; 1.0, 0.6422, -0.5671; 2.0, 0.5713, -0.7165; 5.0, 0.4818, -0.9540; 6.7, 0.4548, -1.0408; 10.0, 0.4192, -1.1693; 100.0, 0.2517, -2.1913];
%! x = linspace (0, 5, 100);
%! for i = 1:(rows (data))
%!   [f, h] = psb_shoot (x, data(i,2), data(i,3), data(i,1));
%!   v(:,i) = f(:,2); theta(:,i) = h(:,1);
%! endfor
%! plot (x, theta, ";;");

%!demo
%!
%! ## Gebhart et al. (1988, figure 3.4.1 b, p. 52)
%!
%! data = [0.01, 0.9873, -0.0807; 0.1, 0.8591, -0.2301; 0.72, 0.6760, -0.5046; 1.0, 0.6422, -0.5671; 2.0, 0.5713, -0.7165; 5.0, 0.4818, -0.9540; 6.7, 0.4548, -1.0408; 10.0, 0.4192, -1.1693; 100.0, 0.2517, -2.1913];
%! x = linspace (0, 5, 100);
%! for i = 1:(rows (data))
%!   [f, h] = psb_shoot (x, data(i,2), data(i,3), data(i,1));
%!   v(:,i) = f(:,2); theta(:,i) = h(:,1);
%! endfor
%! plot (x, v, ";;");
