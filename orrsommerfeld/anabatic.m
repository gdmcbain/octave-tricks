## Copyright (C) 2002, 2005, 2007, 2018 G. D. McBain -*- octave -*-
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

## -*- texinfo -*-`
## @deftypefn {Function File} {} [@var{V}, @var{Vpp}, @var{T}, @var{Tp}] = anabatic (@var{x})
## Compute the base solution for the stationary laminar anabatic
## boundary layer (Prandtl 1952, pp. 422--425; Gill & Davey 1969);
## specifically, the vertical velocity profile @var{V} and its second
## derivative @var{Vpp} and the excess temperature @var{T} and its
## gradient @var{Tp}. 
##
## @example
##
## @group
## V (x) = exp (-x) * sin (x)
##
##      T (x) = exp (-x) * cos (x)
##
##      0 <= x < Inf
## @end group
## @end example
##
## A. E. Gill & A. Davey 1969 Instabilities of a buoyancy driven system.
## @cite{Journal of Fluid Mechanics} @strong{35}:775--798.
##
## L. Prandtl 1952 @cite{Essentials of Fluid Dynamics}.  Blackie.
##
## @end deftypefn

## ### NONDIMENSIONALIZATION
##
## Our general nondimensionalization scheme sets the stationary
## Oberbeck--Boussinesq momentum and temperature deviation equations as:
##
##         Re (u . NABLA u) = - Re Eu NABLA p + LAP u + (Gr/Re) T j
##
##      Re Pr (u . NABLA T) = lap T - S Re Pr v
##
## To obtain the parameter-free expression of Gill & Davey (1969) for
## the anabatic solution of Prandtl, we need to choose
##
##      Gr = 2 Re
##
## and
##
##      S = 2 / (Re Pr).
##
## The subsidiary parameter RaS (== S Gr Pr) becomes identically 4; that
## this entails no loss in generality follows from the lack of a
## geometric length scale in the definition of the problem.  The length
## scale in this problem is
##
##      L = {(4 nu kappa)/(g beta s)}^{1/4},
##
## where nu, kappa, and beta are the kinematic viscosity, thermometric
## conductivity, and thermal coefficient of cubical expansion; g is the
## gravitational field strength and s is the background vertical
## temperature gradient.  The speed scale is
##
##      V = (Delta T) sqrt (g beta kappa / s nu).
##
## ### IMPLEMENTATION NOTES
##
## If V(i,j) is the j-th derivative of V at x(i), then V(i,j) = A(i,k) *
## B(k,j), summed over k, where A(i,j) = exp (-x(i)) * delta (i,j),
## summed over i where delta is Kronecker's delta, and B(i,1) = sin
## (x(i)), B(i,2) = cos (x(i)) - sin (x(i)), and B(i,3) = -2*cos (x(i)).

## Author: G. D. McBain <gdmcbain@protonmail.com>
## Created: 2002-11-08
## Keywords: natural convection

function [V, Vpp, T, Tp] = anabatic (x)
  
  x = x(:);			# ensure x is a column

  f = diag (exp (-x)) * [sin(x), -2*cos(x), cos(x), -sqrt(2)*sin(x+0.25*pi)]);
  V = f(:,1);
  Vpp = f(:,2);
  T = f(:,3);
  Tp = f(:,4);
	       
endfunction

%!demo
%! [V, Vpp, T] = anabatic (x = 0:0.1:10);
%! plot (x, V, "*-g;V;", x, T, "+-r;Theta;")
