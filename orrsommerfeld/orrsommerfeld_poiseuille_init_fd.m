## Copyright (C) 2006, 2007, 2017 G. D. McBain -*- octave -*-
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

## -*- texinfo -*-
## @deftypefn {Function File} {} [@var{V}, @var{Vpp}, @var{D}] =
## orrsommerfeld_poiseuille_init_fd (@var{x})
##
## Compute the velocity @var{V} and its second derivative @var{Vpp}
## along with the finite difference matrices @var{D} for the
## Orr--Sommerfeld equation for plane Poiseuille flow on a grid
## @var{x}.
##
## @end deftypefn

## Author: G. D. McBain <gdmcbain@protonmail.com>
## Created: 2006-11-28
## Keywords: hydrodynamic stability, finite differences

function [V, Vpp, D] = orrsommerfeld_poiseuille_init_fd (x)

  V = 1 - x.^2;
  Vpp = -2 * ones (length (x), 1);

  for q = [2, 4]
    D{q} = finidiff_matrix_dirichlet (x, q, -1, 1); 
  endfor

endfunction

%!demo
%!
%! ## most unstable mode for Poiseuille flow at Re = 1e4, k = 1
%!
%! c1O = 0.23752649 +0.00373967i; # (Orszag 1971, at p. 695)
%! c1C = 0.237526488821 + 0.003739670623i; # (Chubb 2006, eq. 7.1)
%!
%! Re = 1e4; 
%! k = 1;
%!
%! printf ("%3s\t%- 11.8s %+11.8s error\n\n", "n", "imag (c1)", "");
%! for n = 2 .^ (2:10)
%!   x = (linspace (-1, 1, n+2) ((1:n)+1))';
%!   [V, Vpp, D] = orrsommerfeld_poiseuille_init_fd (x);
%!   [L, M] = orrsommerfeld (Re, k, D{2}, D{4}, V, Vpp);
%!   ic1 = max (imag (eig (M\L)));
%!   err = abs (ic1 - imag (c1C));
%!   printf ("%3d\t%+11.8fi %e\n", n, ic1, err); 
%! endfor

%!demo
%!
%! ## ditto, using Richardson extrapolation
%!
%! assert (exist ("richardson_extrapolate", "file"));
%! c1C = 0.237526488821 + 0.003739670623i; # (Chubb 2006, eq. 7.1)
%!
%! Re = 1e4; 
%! k = 1;
%!
%! printf ("%3s\t%- 11.8s %+11.8s error\n\n", "n", "c1", "");
%! n = 8:8:128; nn = length (n);
%! c1 = zeros (1, nn);
%! for i = 1:nn
%!   x = (linspace (-1, 1, n(i)+2) ((1:n(i))+1))';
%!   [V, Vpp, D] = orrsommerfeld_poiseuille_init_fd (x);
%!   [L, M] = orrsommerfeld (Re, k, D{2}, D{4}, V, Vpp);
%!   c = eig (M\L);
%!   [rc1, ic1] = max (imag (c));
%!   c1(i) = c(ic1);
%!   printf ("%3d\t%- 11.8f %+11.8fi %e\n", n(i), real (c1(i)), imag (c1(i)), abs (c1(i) - c1C)); 
%! endfor
%! printf ("\n");
%! r = richardson_extrapolate (c1, n, -2);
%! for i = 1:nn-1
%!   printf ("%3d\t%- 11.8f %+11.8fi %e\n", n(i+1), real (r(i)), imag (r(i)), abs (r(i) - c1C)); 
%! endfor
