## Copyright (C) 2006, 2007 G. D. McBain -*- octave -*-
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or (at
## your option) any later version.
##
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301USA

## -*- texinfo -*-
## @deftypefn {Function File} {} [@var{V}, @var{Vpp}, @var{Tp}, @var{D}, @var{D2T}] = gershuniplapp_nachtsheim_init_fd (@var{x}, @var{Pr}, @var{fpp00}, @var{Tp00})

## Set up for the linear stability of the natural convection boundary
## layer on an isothermal hot vertical wall in a cold isothermal fluid;
## i.e. for the abscissae @var{x} and Prandtl number @var{Pr}, compute
## the vertical velocity @var{V}, its curvature @var{Vpp}, the
## horizontal temperature gradient @var{Tp}, and a cell array @var{D} of
## first, second, and fourth-order differentiation matrices with
## homogeneous Dirichlet boundary conditions at the wall; i.e. f (0) = 0
## for @var{D}@{1@} and @var{D}@{2@} and f (0) = f' (0) = 0 for
## @var{D}@{4@}.
##
## The caller must provide the missing initial conditions @var{fpp00}
## and @var{Tp00} of the Pohlhausen--Schmidt--Beckmann similarity
## equations for the base solution (Schmidt & Beckmann 1930); these should be obtained from @code{psb_shooting (@var{x}, @var{fpp0}, @var{Tp0}, @var{Pr}.
##
## P. R. Nachtsheim 1963 Stability of free-convection boundary-layer
## flows. NASA Tech. Note D-2089.
##
## E. Schmidt & W. Beckmann 1930 Das Temperatur- und
## Geschwindigkeitsfeld vor einer W@"arme abgebenden senkrechten Platte
## bei nat@"urlicher Konvektion.  II Die Versuche und ihre Ergebnisse.
## @cite{Techn. Mechan. u. Thermodynamik} @strong{1}(11):391--406.

## @seealso{psb_shoot}
## @seealso{finidiff_matrix_dirichlet}
## @end deftypefn

## Author: G. D. McBain <gdmcbain@protonmail.com>
## Created: 2006-12-04
## Keywords: natural convection, linear stability

function [V, Vpp, Tp, D] = gershuniplapp_nachtsheim_init_fd (x, Pr, fpp00, Tp00)

  n = length (x);
  xinf = x(n) + (x(n)-x(n-1));
  [F, H] = psb_shoot ([0; x; xinf], fpp00, Tp00, Pr);
  V = F((1:n)+1,2); Vpp = F((1:n)+1,4); Tp = H((1:n)+1,2);
  
  ## TODO: replace the far-field Dirichlet approximation with correct
  ## asymptotic boundary conditions
  
  for q = [1, 2, 4]
    D{q} = finidiff_matrix_dirichlet (x, q, 0, xinf);
  endfor  

endfunction

%!test
%!
%! ## finding a transverse segment near the critical point (Pr = 0.733)
%!
%! Pr = 0.733; alpha = 0.15;
%!
%! xinf = 15; 
%! h = 0.1; 
%! r = 1 + 1.25 * h;
%! n = ceil (grid_cellcentred_n (h, r, 25));
%! x = grid_cellcentred (h, r, n);
%!
%! [V, Vpp, Tp, D] = gershuniplapp_nachtsheim_init_fd (x, Pr, 0.67418, -0.50791);
%! p50 = gershuniplapp_unstable_p ([50; alpha], Pr, D{1}, D{2}, D{4}, D{2}, V, Vpp, Tp);
%! p70 = gershuniplapp_unstable_p ([70; alpha], Pr, D{1}, D{2}, D{4}, D{2}, V, Vpp, Tp);
%! assert (p50, ~p70);

%!test
%!
%! ## ... Finding a transverse segment near the bottom-right
%! Pr = 0.733; 
%! Re = 200;
%!
%! xinf = 25; 
%! h = 0.1; 
%! r = 1 + 1.25 * h;
%! n = ceil (grid_cellcentred_n (h, r, 25));
%! x = grid_cellcentred (h, r, n);
%!
%! [V, Vpp, Tp, D] = gershuniplapp_nachtsheim_init_fd (x, Pr, 0.67418, -0.50791);
%! ps = gershuniplapp_unstable_p ([Re; 0.02], Pr, D{1}, D{2}, D{4}, D{2}, V, Vpp, Tp);
%! pu = gershuniplapp_unstable_p ([Re; 0.04], Pr, D{1}, D{2}, D{4}, D{2}, V, Vpp, Tp);
%! assert (~ps, pu);


%!test
%!
%! ## transverse segment for Pr = 6.7
%!
%! Pr = 6.7;
%!
%! xinf = 25; 
%! h = 0.1; 
%! r = 1 + 1.25 * h;
%! n = ceil (grid_cellcentred_n (h, r, 25));
%! x = grid_cellcentred (h, r, n);
%!
%! [V, Vpp, Tp, D] = gershuniplapp_nachtsheim_init_fd (x, Pr, 0.45475, -1.0408);
%! R = 400;
%! ps = gershuniplapp_unstable_p ([R; 0], Pr, D{1}, D{2}, D{4}, D{2}, V, Vpp, Tp);
%! pu = gershuniplapp_unstable_p ([R; 0.05], Pr, D{1}, D{2}, D{4}, D{2}, V, Vpp, Tp);
%! assert (~ps, pu);

%!demo
%!
%! ## velocity profiles for Pr = [0.733, 6.7] (Nachtsheim 1963, figure 1)
%! ## (takes less than a second)
%! 
%! xinf = 25; 
%! h = 0.1; 
%! r = 1 + 1.25 * h;
%! n = ceil (grid_cellcentred_n (h, r, 25));
%! x = grid_cellcentred (h, r, n);
%!
%! Vair = gershuniplapp_nachtsheim_init_fd (x, 0.733, 0.67418, -0.50791);
%! VH20 = gershuniplapp_nachtsheim_init_fd (x, 6.7, 0.45475, -1.0408);
%!
%! plot (x, Vair, ";Pr = 0.733;", x, VH20, ";6.7;");
%! grid ("on")
%! axis ([0, 7, 0, 0.28]);
%! title ("velocity profiles for Pr = [0.733, 6.7] (Nachtsheim 1963, figure 1)")%! xlabel ("normal distance from plate, x")
%! ylabel ("vertical velocity, V")

%!demo
%!
%! ## stability margin for Pr = 0.733 (Nachtsheim 1963, figure 6)
%! ## (takes about 20 s)
%!
%! assert (exist ("askirt", "file"));
%!
%! Pr = 0.733;
%!
%! xinf = 15; 
%! h = 0.1; 
%! r = 1 + 1.25 * h;
%! n = ceil (grid_cellcentred_n (h, r, 25));
%! x = grid_cellcentred (h, r, n);
%!
%! [V, Vpp, Tp, D] = gershuniplapp_nachtsheim_init_fd (x, Pr, 0.67418, -0.50791);
%! R = 200;
%! printf ("\n%10s   %10s\n\n", "Re", "alpha");
%! m = askirt ([R; 0], [R; 0.04], @ (R) gershuniplapp_unstable_p (R, Pr, D{1}, D{2}, D{4}, D{2}, V, Vpp, Tp), [1; 1e-3], 4, 1.2, -[Inf; Inf], [R; 0.6], 1e4, true )';
%! plot (m(:,1), m(:,2)) 
%! title ("neutral curve for air (after Nachtsheim 1963, figure 6)")
%! xlabel ("Reynolds number, Re")
%! ylabel ("reduced wave number, alpha")
%! axis ([60, 200, 0, 0.7])

%!demo
%!
%! ## stability margin for Pr = 6.7 (Nachtsheim 1963, figure 10)
%! ## (takes about 30 s)
%!
%! assert (exist ("askirt", "file"));
%!
%! Pr = 6.7;
%!
%! xinf = 25; 
%! h = 0.1; 
%! r = 1 + 1.25 * h;
%! n = ceil (grid_cellcentred_n (h, r, 25));
%! x = grid_cellcentred (h, r, n);
%!
%! [V, Vpp, Tp, D] = gershuniplapp_nachtsheim_init_fd (x, Pr, 0.45475, -1.0408);
%! R = 400;
%! printf ("\n%10s   %10s\n\n", "Re", "alpha");
%! m = askirt ([R; 0], [R; 0.05], @ (R) gershuniplapp_unstable_p (R, Pr, D{1}, D{2}, D{4}, D{2}, V, Vpp, Tp), [1; 1e-3], 4, 1.2, -[Inf; Inf], [R; 0.75], 1e4, true )';
%! plot (m(:,1), m(:,2)) 
%! axis ([0, 600, 0, 0.8])
%! title ("neutral curve for water (after Nachtsheim 1963, figure 10)")
%! xlabel ("Reynolds number, Re")
%! ylabel ("reduced wave number, alpha")
