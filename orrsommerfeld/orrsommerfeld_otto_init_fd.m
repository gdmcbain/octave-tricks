## Copyright (C) 2018 G. D. McBain -*- octave -*-

## -*- texinfo -*-
## @deftypefn {Function File} {} [@var{V}, @var{Vpp}, @var{D}] =
## orrsommerfeld_rayleigh_init_fd (@var{x})
## Compute the velocity @var{V} and its second derivative @var{Vpp}
## along with the finite difference matrices @var{D} for the
## Orr--Sommerfeld equation for the frozen unsteady Rayleigh flow on a
## grid @var{x}.
##
## Reference: Otto (1993)
##
## Otto, S. R. (1993). On the stability of a time dependent boundary
## layer. ICASE Report No. 93-73.  Institute for Computer Applications
## in Science and Engineering, Hampton, Virginia.
## @end deftypefn


function [V, Vpp, D] = orrsommerfeld_otto_init_fd (x)

  ## cf. gershuniplapp_nachtsheim_init_fd.m.
  [V, Vpp] = otto(x);
  
  n = length(x);
  xinf = x(n) + (x(n) - x(n-1));
  
  ## TODO: replace the far-field Dirichlet approximation with correct
  ## asymptotic boundary conditions

  for q = [1, 2, 4]
    D{q} = finidiff_matrix_dirichlet(x, q, 0, xinf);
  endfor
	
endfunction

%!demo
%!
%! ## most unstable mode for Otto flow at Re = 3e3, k = 0.4255
%!
%! y = grid_cellcentred (0.08, 1.1, 47);  # ../finidiff
%! [V, Vpp, D] = orrsommerfeld_otto_init_fd (y);
%! Re = 3e3;
%! k = 0.4255;
%! [L, M] = orrsommerfeld (Re, k, D{2}, D{4}, V, Vpp);
%! ic1 = max (imag (eig (M\L)))
