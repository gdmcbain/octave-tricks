## Copyright (C) 2007 G. D. McBain <geordie_mcbain@yahoo.com.au>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

## -*- texinfo -*-
## @deftypefn {Function File} {} bisect_expanding (@var{p}, @var{a}, @var{b}, @var{tol}, @var{varargin})
## Locate a marginal point of the predicate @var{p} using
## @command{bisect}.
##
## Begin using the segment from @var{a} to @var{b}, but expanding them
## until @var{a} is false and @var{b} is true; that is, if @code{@var{p}
## (@var{a})}, then set @var{b} to @var{a} and shift a further out.
## Similarly at the other end unless @code{@var{p} (@var{b})}.
## @end deftypefn

## Author: G. D. McBain <geordie.mcbain@aeromech.usyd.edu.au>
## Created: 2007-01-31
## Keywords: nonlinear equations, root-finding

function x = bisect_expanding (p, a, b, tol)
  
  try
    x = bisect (p, a, b, tol);
  catch
    ##warning ("bisect_expanding failed on [%s, %s]", disp (a), disp (b));
    while p (a)
      d = b - a;
      b = a;			# p (b), for sure
      a -= d;			# ~p (a), maybe
    endwhile
    while ~p (b)
      d = b - a;
      a = b;			# ~p (a), for sure
      b += d;			# p (b), maybe
    endwhile
    ##warning ("bisect_expanding segment to [%s, %s]", disp (a), disp (b));
    x = bisect_expanding (p, a, b, tol);
  end_try_catch

endfunction

%!test
%!
%! tol = 1e-3;
%! assert (abs (bisect_expanding (@ (x) x > 0, -1, 2, tol)) < tol);

%!test
%!
%! tol = 1e-3;
%! assert (abs (bisect_expanding (@ (x) x > 0, 1, 2, tol)) < tol);


%!demo
%!
%! ## p (x) = x.^3 > 0  (Kahaner, Moler, & Nash 1989, p. 240, example 7.1)
%!
%! tol = 1e-3;
%! bisect_expanding (@ (x) x.^3 > 0, -1, 2, tol)
%! bisect_expanding (@ (x) x.^3 > 0, 21, 22, tol)

