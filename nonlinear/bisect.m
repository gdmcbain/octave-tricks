## Copyright (C) 2007 G. D. McBain <geordie_mcbain@yahoo.com.au>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

## -*- texinfo -*-
## @deftypefn {Function File} {} bisect (@var{p}, @var{a}, @var{b}, @var{tol})
## Locate a marginal point of the predicate @var{p} on the segment from
## @var{a} to @var{b} to within @var{tol} using bisection (Kahaner,
## Moler, & Nash 1989).  It is an error if @code{@var{p} (@var{a}) ==
## @var{p} (@var{b})}.
##
## D. Kahaner, C. Moler, & S. Nash 1989 @cite{Numerical Methods and
## Software}. Prentice-Hall, Englewood Cliffs, New Jersey. 
## @end deftypefn

## Author: G. D. McBain <geordie.mcbain@aeromech.usyd.edu.au>
## Created: 2007-01-31
## Keywords: nonlinear equations, root-finding

function x = bisect (p, a, b, tol)

  pa = p (a);
  assert (pa != p (b));

  do 
    x = 0.5 * (a + b);
    if (p (x) == pa)
      a = x;
    else
      b = x;
    endif
  until (all (abs (b-a) < tol))

endfunction

%!test
%!
%! tol = 1e-3;
%! assert (abs (bisect (@ (x) x > 0, -1, 2, tol)) < tol);

%!test
%!
%! fail ("bisect (@ (x) x.^3 > 0, 1, 2, 1e-3)", "!=");

%!demo
%!
%! ## f (x) = x.^3 (Kahaner, Moler, & Nash 1989, p. 240, example 7.1)
%!
%! printf ("%5s\t %7s\n\n", "tol", "x (=error)");
%! for tol = logspace (-1, -9, 9)
%!   x = bisect (@ (x) x.^3 > 0, -1, 2, tol);
%!   printf ("%5.0e\t% 7.1e\t\n", tol, x);
%! endfor
