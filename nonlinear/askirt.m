## Copyright (C) 2003, 2006, 2007, 2012 G. D. McBain -*- octave -*-
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2, or (at your option)
## any later version.
##
## Askirt is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
## for more details.
##
## You should have received a copy of the GNU General Public License
## along with Askirt; see the file COPYING.  If not, write to the Free
## Software Foundation, 51 Franklin Street, Fifth Floor, Boston, MA
## 02110-1301, USA.

## -*- texinfo -*-
##
## @deftypefn {Function File} {} askirt (@var{u0}, @var{v0}, @var{P}, @var{tol}, @var{btol}, @var{gmax}, @var{ll}, @var{ur}, @var{nsteps}, @var{verbose})
##
## Return a two-rowed matrix whose columns are marginal points of the
## plane point predicate @var{P}.  @var{u0} and @var{v0} are points such
## that @code{@var{P} (@var{u0}) != @var{P} (@var{v0})}. @var{tol} is
## the (2*1) tolerance in each coordinates.  @var{btol} is real (and
## typically >=1) and @code{@var{btol}*@var{tol}} is the tolerance used
## to assess departure of the marginal curve from a polygon.
## @var{max_growth} is a positive real factor (ineffectual unless >1)
## limiting the growth per step of the transverse segment length.
##
## @var{ll} and @var{ur} are the lower-left and upper-right corners of
## the rectangular domain; the function returns when the margin leaves
## the rectangle---set to [-Inf,-Inf] or [Inf,Inf] to deactivate this
## feature. @var{nsteps} is the maximum number of steps to take before
## returning (set to Inf to deactivate).
##
## If the optional @var{verbose} is set to true, report marginal points
## as they're found.
##
## @end deftypefn

## ### BACKGROUND
##
## ## Basics
##
## A segment is said to be TRANSVERSE with respect to a predicate if the
## predicate takes different values at either end.
##
## A point is said to be MARGINAL with respect to a predicate if every
## neighbourhood contains true and false points.
##
## Every transverse segment contains at least one marginal point, which
## can be found by BISECTION.
##
## Given a transverse segment (u,v), another can be found as follows.
## Rotate the segment through one-sixth of a revolution about its left
## end. Test the new end point, w.  If P(w)==P(u), (w,v), and otherwise
## (u,w), is a new transverse segment.
##
## See McBain (2004) for further details.

## ### REFERENCES
##
## G. D. McBain 2004 Skirting subsets of the plane, with application to
## marginal stability curves.  The ANZIAM Journal 45(E), C78--C91.

## Author: G. D. McBain <geordie.mcbain@aeromech.usyd.edu.au>
## Created: 2003-06-23
## Keywords: continuation; adaptive; path following; nonlinear equations

function margin = askirt (u0, v0, P, tol, btol, gmax, ll, ur, nsteps, verbose)

  if (nargin < 10) verbose = false; endif

  assert (Pu = P (u0), ~ P (v0));
  if (nsteps <= 0) return endif

  g = tol .** (-2);		# diagonal of metric tensor
  R = diag (tol) * givens (0.5, -0.5*sqrt (3)) / diag (tol); # rotator

  while (true)			# initialize prior to main loop

    m0 = predicate_bisect (u0, v0, P, Pu, g);

    if (nsteps < 2) margin = m0; return endif

    if (P (us = m0+0.5*(u0-m0)) != Pu)
      v0 = us; continue; 	# additional marginal point
    endif	# left end pulled in

    if (P (vs = m0+0.5*(v0-m0)) == Pu)
      u0 = vs; continue; 	# additional marginal point
    endif	# right end pulled in
    
    [m1, u1, v1] = step_and_bisect (u0, v0, P, Pu, g, R);

     if (perp (m1, m0, 
	       ms = step_and_bisect (us, vs, P, Pu, g, R), 
	       g)
	 < btol )		# m1 follows from m0 and m1s
       break			# so initialization is complete
     endif

    u0 = us; v0 = vs;		# shorten segment and try again

  endwhile			# end of initialization loop

  margin = [m0, m1]; 
  if verbose, disp (margin.'); endif

  if ((nsteps -= 2) < 1) return endif

  while (true)			# main loop

    ## By now we know m0 and u1, v1, and m1.  We use u1 and v1 to
    ## construct u2 and v2 and then m2, and compare m2 with m0 and m1
    ## for deviation from linearity.

    [m2, u2, v2] = step_and_bisect (u1, v1, P, Pu, g, R);

    if ((dogleg = perp (m2, m0, m1, g)) > btol)	# too crooked
      if (P (u1new =u1+0.5*(m1-u1)) != Pu)
	m1 = predicate_bisect (u1, v1=u1new, P, Pu, g);
      elseif (P (v1new =v1+0.5*(m1-v1)) == Pu)
	m1 = predicate_bisect (u1=v1new, v1, P, Pu, g);
      else
	u1 = u1new; v1 = v1new;	# & retain old m1
      endif

    else			# running smoothly

      margin = [margin, m2];	# output new marginal point
      if verbose, disp (m2.'); endif
      if (any (m2<ll) || any (m2>ur) || --nsteps<1) 
	break endif		# exited domain or exceeded step limit

      if (btol > dogleg*gmax) # cap growth rate?
	growth = gmax; else growth = btol / dogleg; endif

      if (P (u2new =m2+growth*(u2-m2)) == Pu)
	u1 = u2new; 		# left end extended
      else u1 = u2; endif	# crossed margin, don't extend
      if (P (v2new =m2+growth*(v2-m2)) == Pu)
	v1 = v2; 		# crossed margin, don't extend
      else v1 = v2new; endif	# right end extended
      
      m0 = m1; m1 = m2;

    endif

  endwhile			# end of main loop

endfunction

function [m, u, v] = step_and_bisect (a, b, P, Pa, g, R)
  [u, v] = generic_step (a, b, P, Pa, R);
  m = predicate_bisect (u, v, P, Pa, g);
endfunction

function [u, v] = generic_step (u, v, P, P_u, A)
  if (P (w = u+A*(v-u)) == P_u)
    u = w; else v = w; endif
endfunction

function a = predicate_bisect (a, b, pred, p_a, g)
  do
    [a, b] = generic_step (a, b, pred, p_a, 0.5);
  until ((b-a)' * diag (g) * (b-a) <= 4) # segment length <2
  a += 0.5 * (b-a);		# midpoint of final segment
endfunction

function d = perp (a, b, c, g)
  
  u = a - b; v = c - b;		# use b as origin

  ## return distance from a to its projection on span (b).
  perpendicular = u - (v * v' * diag (g))/(v' * diag (g) * v) * u;
  d = sqrt (perpendicular' * diag (g) * perpendicular);

endfunction

%!demo# skirt the unit circle
%! m = askirt ([0; 0], [2; 0], @ (x) (norm (x) < 1), 0.1*ones (2, 1), 0.1, 1.1, -[Inf; Inf], [Inf; Inf], 600);
%! theta = linspace (0, 2*pi, 1e3);
%! plot (m(1,:), m(2,:), "r*-;;", cos (theta), sin (theta), "g-;;")
